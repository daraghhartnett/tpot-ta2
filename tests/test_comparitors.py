import unittest
import math
import os
import sys

from autoflow.util import comparitors

class TestComparitor(unittest.TestCase):

    def test_soft_max_method(self):
        # Test large values descending
        values = [0.9, 0.7, 0.5, 0.3]
        probs = comparitors.softmax(values)
        for index in range(len(probs) - 1):
            self.assertTrue(probs[index] > probs[index + 1])
        # Values should always sum to 1.0
        self.assertTrue(sum(probs) == 1.0)

        # Test small values ascending
        values = [0.3, 0.5, 0.7, 0.9]
        probs = comparitors.softmax(values)
        for index in range(len(probs) - 1):
            self.assertTrue(probs[index] < probs[index + 1])
        # Values should always sum to 1.0
        self.assertTrue(sum(probs) == 1.0)

        # Test mixed values
        values = [0.001, 0.99, 0.0001, 0.8]
        probs = comparitors.softmax(values)
        self.assertTrue(probs[0] < probs[1] and probs[0] > probs[2] and probs[0] < probs[3])
        self.assertTrue(probs[1] > probs[0] and probs[1] > probs[2] and probs[1] > probs[3])
        self.assertTrue(probs[2] < probs[0] and probs[2] < probs[1] and probs[2] < probs[3])
        self.assertTrue(probs[3] > probs[0] and probs[3] < probs[1] and probs[3] > probs[2])
        # Values should always sum to 1.0
        self.assertTrue(sum(probs) == 1.0)