# Run this script from the tpot-ta2 home directory so that the following dependencies will be installed as peers
# modules

# If the environment is already in place run the following to purge it before recreating it.
# conda remove -n 20200518 --all
# First create the environment
conda env create -f scripts/20200518-env.yml
# Switch to the environment so that we install the dependencies into the target environment
source activate 20200518

if [ $? -eq 0 ]; then
    echo Environment activated. Start install to env.
else
    echo Failed to activate environment. ABORT
    exit $?
fi


# Install common primitives:
cd ..
git clone --recursive git@gitlab.com:datadrivendiscovery/common-primitives.git
cd common-primitives/
git checkout tags/v0.8.0
# tensorflow-gpu is not currently available for mac so we will remove it before installing common-primitives
sed '/.*tensorflow\-gpu.*/d' setup.py > tmp.py
mv setup.py original-setup.py
mv tmp.py setup.py
pip install -e ./

# Install DSBox:
cd ..
#TODO: Update this reference when ISI makes a release for 20200518
pip install -e git+https://github.com/usc-isi-i2/dsbox-primitives@ca0a450d1686e373a3692a6b496c504dfe79f065#egg=dsbox-primitives

# Install the Dummy TA3:
git clone https://gitlab.com/datadrivendiscovery/dummy-ta3.git
cd dummy-ta3
pip3 install .

# Install Gama:
cd ..
git clone https://github.com/PGijsbers/gama.git
cd gama
git checkout d3m
# python setup.py install
# or the following to allow you to debug into the source
pip install -e ./

# Install sklearn-wrap
cd ..
pip install -e git+https://gitlab.com/datadrivendiscovery/sklearn-wrap.git@dist#egg=sklearn_wrap

# Install the ta3-ta2api
git clone https://gitlab.com/datadrivendiscovery/ta3ta2-api.git
cd ta3ta2-api
#TODO: Update this if the 23 api gets updates for 20200518
git checkout tags/v2020.6.2

# Install datamart-api
cd ..
git clone https://github.com/usc-isi-i2/datamart.git
cd datamart
git checkout jun_test
pip install -e ./

# Depends on torch version that is incompatible with common-primitives
# Install Autonbox for semi supervised
#cd ..
#pip install -U -e git+https://github.com/autonlab/autonbox.git#egg=autonbox




