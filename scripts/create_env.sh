conda remove -n test-package --all
# First create the environment
conda create -n test-package 
# Switch to the environment so that we install the dependencies into the target environment
source activate test-package 
