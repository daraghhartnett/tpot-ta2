# Before running this script be sure you have the primitives repo (https://gitlab.datadrivendiscovery.org/jpl/primitives_repo.git)
# checked out as a peer of this repo in a primitives folder. Adjust the path below if necessary.

export PATH_TO_PRIMITIVES_REPO="../primitives/primitives_repo/v2019.4.4/SRI"
export PRIMITIVES_VERSION="1.2.5"

mkdir -p $PATH_TO_PRIMITIVES_REPO/d3m.primitives.data_preprocessing.dataset_text_reader.DatasetTextReader/$PRIMITIVES_VERSION/
mkdir -p $PATH_TO_PRIMITIVES_REPO/d3m.primitives.data_transformation.graph_transformer.GraphTransformer/$PRIMITIVES_VERSION/
mkdir -p $PATH_TO_PRIMITIVES_REPO/d3m.primitives.data_transformation.conditioner.StaticEnsembler/$PRIMITIVES_VERSION/
mkdir -p $PATH_TO_PRIMITIVES_REPO/d3m.primitives.data_transformation.graph_to_edge_list.GraphToEdgeList/$PRIMITIVES_VERSION/
mkdir -p $PATH_TO_PRIMITIVES_REPO/d3m.primitives.data_transformation.collaborative_filtering_parser.CollaborativeFilteringParser/$PRIMITIVES_VERSION/
mkdir -p $PATH_TO_PRIMITIVES_REPO/d3m.primitives.classification.community_detection.CommunityDetection/$PRIMITIVES_VERSION/
mkdir -p $PATH_TO_PRIMITIVES_REPO/d3m.primitives.data_transformation.graph_node_splitter.GraphNodeSplitter/$PRIMITIVES_VERSION/
mkdir -p $PATH_TO_PRIMITIVES_REPO/d3m.primitives.classification.gaussian_classification.MeanBaseline/$PRIMITIVES_VERSION/
mkdir -p $PATH_TO_PRIMITIVES_REPO/d3m.primitives.data_transformation.community_detection_parser.CommunityDetectionParser/$PRIMITIVES_VERSION/
mkdir -p $PATH_TO_PRIMITIVES_REPO/d3m.primitives.data_transformation.edge_list_to_graph.EdgeListToGraph/$PRIMITIVES_VERSION/
mkdir -p $PATH_TO_PRIMITIVES_REPO/d3m.primitives.classification.vertex_nomination.VertexNomination/$PRIMITIVES_VERSION/
mkdir -p $PATH_TO_PRIMITIVES_REPO/d3m.primitives.data_transformation.vertex_nomination_parser.VertexNominationParser/$PRIMITIVES_VERSION/
mkdir -p $PATH_TO_PRIMITIVES_REPO/d3m.primitives.data_transformation.graph_matching_parser.GraphMatchingParser/$PRIMITIVES_VERSION/
mkdir -p $PATH_TO_PRIMITIVES_REPO/d3m.primitives.link_prediction.link_prediction.LinkPrediction/$PRIMITIVES_VERSION/
mkdir -p $PATH_TO_PRIMITIVES_REPO/d3m.primitives.link_prediction.graph_matching_link_prediction.GraphMatchingLinkPrediction/$PRIMITIVES_VERSION/
mkdir -p $PATH_TO_PRIMITIVES_REPO/d3m.primitives.link_prediction.collaborative_filtering_link_prediction.CollaborativeFilteringLinkPrediction/$PRIMITIVES_VERSION/
mkdir -p $PATH_TO_PRIMITIVES_REPO/d3m.primitives.classification.general_relational_dataset.GeneralRelationalDataset/$PRIMITIVES_VERSION/
mkdir -p $PATH_TO_PRIMITIVES_REPO/d3m.primitives.time_series_forecasting.time_series_to_list.RelationalTimeseries/$PRIMITIVES_VERSION/
mkdir -p $PATH_TO_PRIMITIVES_REPO/d3m.primitives.data_transformation.zero_count.ZeroCount/$PRIMITIVES_VERSION/
mkdir -p $PATH_TO_PRIMITIVES_REPO/d3m.primitives.data_transformation.conditioner.Conditioner/$PRIMITIVES_VERSION/
mkdir -p $PATH_TO_PRIMITIVES_REPO/d3m.primitives.data_transformation.stacking_operator.StackingOperator/$PRIMITIVES_VERSION/

mkdir -p $PATH_TO_PRIMITIVES_REPO/d3m.primitives.data_preprocessing.dataset_text_reader.DatasetTextReader/$PRIMITIVES_VERSION/pipelines
mkdir -p $PATH_TO_PRIMITIVES_REPO/d3m.primitives.data_transformation.graph_transformer.GraphTransformer/$PRIMITIVES_VERSION/pipelines
mkdir -p $PATH_TO_PRIMITIVES_REPO/d3m.primitives.data_transformation.conditioner.StaticEnsembler/$PRIMITIVES_VERSION/pipelines
mkdir -p $PATH_TO_PRIMITIVES_REPO/d3m.primitives.data_transformation.graph_to_edge_list.GraphToEdgeList/$PRIMITIVES_VERSION/pipelines
mkdir -p $PATH_TO_PRIMITIVES_REPO/d3m.primitives.data_transformation.collaborative_filtering_parser.CollaborativeFilteringParser/$PRIMITIVES_VERSION/pipelines
mkdir -p $PATH_TO_PRIMITIVES_REPO/d3m.primitives.classification.community_detection.CommunityDetection/$PRIMITIVES_VERSION/pipelines
mkdir -p $PATH_TO_PRIMITIVES_REPO/d3m.primitives.data_transformation.graph_node_splitter.GraphNodeSplitter/$PRIMITIVES_VERSION/pipelines
mkdir -p $PATH_TO_PRIMITIVES_REPO/d3m.primitives.classification.gaussian_classification.MeanBaseline/$PRIMITIVES_VERSION/pipelines
mkdir -p $PATH_TO_PRIMITIVES_REPO/d3m.primitives.data_transformation.community_detection_parser.CommunityDetectionParser/$PRIMITIVES_VERSION/pipelines
mkdir -p $PATH_TO_PRIMITIVES_REPO/d3m.primitives.data_transformation.edge_list_to_graph.EdgeListToGraph/$PRIMITIVES_VERSION/pipelines
mkdir -p $PATH_TO_PRIMITIVES_REPO/d3m.primitives.classification.vertex_nomination.VertexNomination/$PRIMITIVES_VERSION/pipelines
mkdir -p $PATH_TO_PRIMITIVES_REPO/d3m.primitives.data_transformation.vertex_nomination_parser.VertexNominationParser/$PRIMITIVES_VERSION/pipelines
mkdir -p $PATH_TO_PRIMITIVES_REPO/d3m.primitives.data_transformation.graph_matching_parser.GraphMatchingParser/$PRIMITIVES_VERSION/pipelines
mkdir -p $PATH_TO_PRIMITIVES_REPO/d3m.primitives.link_prediction.link_prediction.LinkPrediction/$PRIMITIVES_VERSION/pipelines
mkdir -p $PATH_TO_PRIMITIVES_REPO/d3m.primitives.link_prediction.graph_matching_link_prediction.GraphMatchingLinkPrediction/$PRIMITIVES_VERSION/pipelines
mkdir -p $PATH_TO_PRIMITIVES_REPO/d3m.primitives.link_prediction.collaborative_filtering_link_prediction.CollaborativeFilteringLinkPrediction/$PRIMITIVES_VERSION/pipelines
mkdir -p $PATH_TO_PRIMITIVES_REPO/d3m.primitives.classification.general_relational_dataset.GeneralRelationalDataset/$PRIMITIVES_VERSION/pipelines
mkdir -p $PATH_TO_PRIMITIVES_REPO/d3m.primitives.time_series_forecasting.time_series_to_list.RelationalTimeseries/$PRIMITIVES_VERSION/pipelines
mkdir -p $PATH_TO_PRIMITIVES_REPO/d3m.primitives.data_transformation.zero_count.ZeroCount/$PRIMITIVES_VERSION/pipelines
mkdir -p $PATH_TO_PRIMITIVES_REPO/d3m.primitives.data_transformation.conditioner.Conditioner/$PRIMITIVES_VERSION/pipelines
mkdir -p $PATH_TO_PRIMITIVES_REPO/d3m.primitives.data_transformation.stacking_operator.StackingOperator/$PRIMITIVES_VERSION/pipelines

python -m d3m.index describe -i 4 d3m.primitives.data_preprocessing.dataset_text_reader.DatasetTextReader > $PATH_TO_PRIMITIVES_REPO/d3m.primitives.data_preprocessing.dataset_text_reader.DatasetTextReader/$PRIMITIVES_VERSION/primitive.json
python -m d3m.index describe -i 4 d3m.primitives.data_transformation.graph_transformer.GraphTransformer > $PATH_TO_PRIMITIVES_REPO/d3m.primitives.data_transformation.graph_transformer.GraphTransformer/$PRIMITIVES_VERSION/primitive.json
python -m d3m.index describe -i 4 d3m.primitives.data_transformation.conditioner.StaticEnsembler > $PATH_TO_PRIMITIVES_REPO/d3m.primitives.data_transformation.conditioner.StaticEnsembler/$PRIMITIVES_VERSION/primitive.json
python -m d3m.index describe -i 4 d3m.primitives.data_transformation.graph_to_edge_list.GraphToEdgeList > $PATH_TO_PRIMITIVES_REPO/d3m.primitives.data_transformation.graph_to_edge_list.GraphToEdgeList/$PRIMITIVES_VERSION/primitive.json
python -m d3m.index describe -i 4 d3m.primitives.data_transformation.collaborative_filtering_parser.CollaborativeFilteringParser > $PATH_TO_PRIMITIVES_REPO/d3m.primitives.data_transformation.collaborative_filtering_parser.CollaborativeFilteringParser/$PRIMITIVES_VERSION/primitive.json
python -m d3m.index describe -i 4 d3m.primitives.classification.community_detection.CommunityDetection > $PATH_TO_PRIMITIVES_REPO/d3m.primitives.classification.community_detection.CommunityDetection/$PRIMITIVES_VERSION/primitive.json
python -m d3m.index describe -i 4 d3m.primitives.data_transformation.graph_node_splitter.GraphNodeSplitter > $PATH_TO_PRIMITIVES_REPO/d3m.primitives.data_transformation.graph_node_splitter.GraphNodeSplitter/$PRIMITIVES_VERSION/primitive.json
python -m d3m.index describe -i 4 d3m.primitives.classification.gaussian_classification.MeanBaseline > $PATH_TO_PRIMITIVES_REPO/d3m.primitives.classification.gaussian_classification.MeanBaseline/$PRIMITIVES_VERSION/primitive.json
python -m d3m.index describe -i 4 d3m.primitives.data_transformation.community_detection_parser.CommunityDetectionParser > $PATH_TO_PRIMITIVES_REPO/d3m.primitives.data_transformation.community_detection_parser.CommunityDetectionParser/$PRIMITIVES_VERSION/primitive.json
python -m d3m.index describe -i 4 d3m.primitives.data_transformation.edge_list_to_graph.EdgeListToGraph > $PATH_TO_PRIMITIVES_REPO/d3m.primitives.data_transformation.edge_list_to_graph.EdgeListToGraph/$PRIMITIVES_VERSION/primitive.json
python -m d3m.index describe -i 4 d3m.primitives.classification.vertex_nomination.VertexNomination > $PATH_TO_PRIMITIVES_REPO/d3m.primitives.classification.vertex_nomination.VertexNomination/$PRIMITIVES_VERSION/primitive.json
python -m d3m.index describe -i 4 d3m.primitives.data_transformation.vertex_nomination_parser.VertexNominationParser > $PATH_TO_PRIMITIVES_REPO/d3m.primitives.data_transformation.vertex_nomination_parser.VertexNominationParser/$PRIMITIVES_VERSION/primitive.json
python -m d3m.index describe -i 4 d3m.primitives.data_transformation.graph_matching_parser.GraphMatchingParser > $PATH_TO_PRIMITIVES_REPO/d3m.primitives.data_transformation.graph_matching_parser.GraphMatchingParser/$PRIMITIVES_VERSION/primitive.json
python -m d3m.index describe -i 4 d3m.primitives.link_prediction.link_prediction.LinkPrediction > $PATH_TO_PRIMITIVES_REPO/d3m.primitives.link_prediction.link_prediction.LinkPrediction/$PRIMITIVES_VERSION/primitive.json
python -m d3m.index describe -i 4 d3m.primitives.link_prediction.graph_matching_link_prediction.GraphMatchingLinkPrediction > $PATH_TO_PRIMITIVES_REPO/d3m.primitives.link_prediction.graph_matching_link_prediction.GraphMatchingLinkPrediction/$PRIMITIVES_VERSION/primitive.json
python -m d3m.index describe -i 4 d3m.primitives.link_prediction.collaborative_filtering_link_prediction.CollaborativeFilteringLinkPrediction > $PATH_TO_PRIMITIVES_REPO/d3m.primitives.link_prediction.collaborative_filtering_link_prediction.CollaborativeFilteringLinkPrediction/$PRIMITIVES_VERSION/primitive.json
python -m d3m.index describe -i 4 d3m.primitives.classification.general_relational_dataset.GeneralRelationalDataset > $PATH_TO_PRIMITIVES_REPO/d3m.primitives.classification.general_relational_dataset.GeneralRelationalDataset/$PRIMITIVES_VERSION/primitive.json
python -m d3m.index describe -i 4 d3m.primitives.time_series_forecasting.time_series_to_list.RelationalTimeseries > $PATH_TO_PRIMITIVES_REPO/d3m.primitives.time_series_forecasting.time_series_to_list.RelationalTimeseries/$PRIMITIVES_VERSION/primitive.json
python -m d3m.index describe -i 4 d3m.primitives.data_transformation.zero_count.ZeroCount > $PATH_TO_PRIMITIVES_REPO/d3m.primitives.data_transformation.zero_count.ZeroCount/$PRIMITIVES_VERSION/primitive.json
python -m d3m.index describe -i 4 d3m.primitives.data_transformation.conditioner.Conditioner > $PATH_TO_PRIMITIVES_REPO/d3m.primitives.data_transformation.conditioner.Conditioner/$PRIMITIVES_VERSION/primitive.json
python -m d3m.index describe -i 4 d3m.primitives.data_transformation.stacking_operator.StackingOperator > $PATH_TO_PRIMITIVES_REPO/d3m.primitives.data_transformation.stacking_operator.StackingOperator/$PRIMITIVES_VERSION/primitive.json