import os
import time
import datetime
from dateutil import parser
from pathlib import Path


def td_format(td_object):
    seconds = int(td_object.total_seconds())
    periods = [
        ('year',        60*60*24*365),
        ('month',       60*60*24*30),
        ('day',         60*60*24),
        ('hr',        60*60),
        ('min',      60),
        ('sec',      1)
    ]

    strings=[]
    for period_name, period_seconds in periods:
        if seconds > period_seconds:
            period_value , seconds = divmod(seconds, period_seconds)
            has_s = 's' if period_value > 1 else ''
            strings.append("%s %s%s" % (period_value, period_name, has_s))

    return ", ".join(strings)


def run_fast_scandir(dir, ext):    # dir: str, ext: str
    subfolders, files, timestamps = [], [], []

    for f in os.scandir(dir):
        if f.is_dir():
            subfolders.append(f.path)
        if f.is_file():
            # file extensions are longer than 1 part
            # if os.path.splitext(f.name)[1].lower() in ext:
            if f.name.endswith(ext):
                files.append(f.path)
                timestamp = time.ctime(os.path.getmtime(f))
                parsed_timestamp = parser.parse(timestamp)
                age = datetime.datetime.now() - parsed_timestamp
                timestamps.append(age)

    for dir in list(subfolders):
        sf, f, t = run_fast_scandir(dir, ext)
        # We only want the immediate sub folders
        # subfolders.extend(sf)
        files.extend(f)
        timestamps.extend(t)
    return subfolders, files, timestamps


output_data_path = '/home/ta2-sri/output/sri'
subfolders, score_files, timestamps = run_fast_scandir(output_data_path, "score.csv")

GREEN = "{color:#00875A}"
RED = "{color:#DE350B}"

print("||Problem|Age|Scores||")
# For each seed problem found
total_count = len(subfolders)
failed = 0
success = 0
for problem in subfolders:
    newest = None
    newest_index = -1
    # Check the available score files
    for i in range(len(score_files)):
        file = score_files[i]
        # Is this a score file for this problem?
        if problem in file:
            # Is this the most recent score file of > 0 size?
            size = Path(score_files[i]).stat().st_size
            if (newest is None or timestamps[i] < newest) and size > 0:
                newest = timestamps[i]
                newest_index = i
    age = "N/A"
    score = "N/A"
    color = RED
    if newest is not None:
        success += 1
        color = GREEN
        age = td_format(newest)
        with open(score_files[newest_index]) as f:
            f.readline()
            score = f.readline().rstrip()
    else:
        failed += 1
    parts = problem.split("/")
    problem_name = parts[len(parts) - 1]
    print("|%s%s{color}|%s|%s|" % (color, problem_name, age, score))

print("\nSummary:")
print("\t%s failures" % failed)
print("\t%s successes" % success)
print("\t%s total" % total_count)

