# Run this script from the tpot-ta2 home directory so that the following dependencies will be installed as peers
# modules

# If the environment is already in place run the following to purge it before recreating it.
# conda remove -n 20201103 --all
# First create the environment
conda env create -f scripts/20201103-env.yml
# Switch to the environment so that we install the dependencies into the target environment
source activate 20201103

if [ $? -eq 0 ]; then
    echo Environment activated. Start install to env.
else
    echo Failed to activate environment. ABORT
    exit $?
fi


# Install common primitives:
cd ..
git clone --recursive git@gitlab.com:datadrivendiscovery/common-primitives.git
cd common-primitives/
git checkout tags/v0.8.0
# tensorflow-gpu is not currently available for mac so we will remove it before installing common-primitives
sed '/.*tensorflow\-gpu.*/d' setup.py > tmp.py
mv setup.py original-setup.py
mv tmp.py setup.py
pip install -e ./

# Install DSBox:
cd ..
#TODO: Update this reference if ISI updates the release for 20201103
pip install -e git+https://github.com/usc-isi-i2/dsbox-primitives@dd210f0b0ce8d569e12f8dbe9ac5373e09e9346c#egg=dsbox-primitives

# Install the Dummy TA3:
git clone https://gitlab.com/datadrivendiscovery/dummy-ta3.git
cd dummy-ta3
pip3 install .

# Install Gama:
cd ..
git clone https://github.com/PGijsbers/gama.git
cd gama
git checkout d3m
# python setup.py install
# or the following to allow you to debug into the source
pip install -e ./

# Install sklearn-wrap
cd ..
pip install -e git+https://gitlab.com/datadrivendiscovery/sklearn-wrap.git@dist#egg=sklearn_wrap

# Install the ta3-ta2api
git clone git@gitlab.com:datadrivendiscovery/automl-rpc.git
cd automl-rpc
git checkout tags/v1.0.0

# We are no longer using the datamart
# Install datamart-api
#cd ..
#git clone https://github.com/usc-isi-i2/datamart.git
#cd datamart
#git checkout jun_test
#pip install -e ./

# Depends on torch version that is incompatible with common-primitives
# Install Autonbox for semi supervised
#cd ..
#pip install -U -e git+https://github.com/autonlab/autonbox.git#egg=autonbox




