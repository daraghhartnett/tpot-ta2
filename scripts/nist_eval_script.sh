#
# This script is used to run a series of tests on one of the seed datasets. It first runs search and then runs test on
# the best scoring pipeline. The predictions made by the test phase are then scored using the provided metric.
#
# It takes two parameters:
#
#     $1 = seed dataset name - this is the actual directory name of the input problem, for instance "38_sick"
#     $2 = the metric that should be used to score the performance of the best pipeline. This is passed directly to
#          the code/evaluate.py script so have a look in there to see the options. An example is "f1Macro"
#     $3 = the absolute path to this directory - this is necessary for the docker container mounts to work properly
#
# An example command line to run this script is:
#
#      sh scripts/nist_eval_script.sh 38_sick f1Macro /home/daragh/d3m/code/sri_ta2
#
# Each iterations search/test output is preserved in its own log file (for example 38_sick-output-1.txt). The scores
# for each iteration is appended to a different file (38_sick_results.log)
#
# The script is currenly configured to run for 10 iterations. For some reason ubuntu did not like the for i in [1..10]
# notation and would exit after one iteration.
#
#  TODO: Fix the for loop issue this and make the iterations desired a parameter.
#  TODO: Make the grep command work accross linux flavors (works on ubuntu but not on mac)
#

for i in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40
do
    echo ""
    echo "=================================="
    echo "=================================="
    echo "Working on iteration $i of 40"
    echo "=================================="
    echo "=================================="
    echo ""
    echo "Working on iteration $i of 40" >> $1_results.log

    # Search
    docker run -i --entrypoint /bin/bash -v $3/config/search_config.json:/home/code/config.json -v $3/data/input/$1:/inputs/ -v $3/data/outputs/$1:/outputs/ --entrypoint=ta2_search registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:latest /home/code/config.json 2>&1 | tee $1-output-$i.txt

    # Get Best Pipeline
    BESTPIPELINE=`grep -E "Created pipeline (.*) \(1," $1-output-$i.txt | grep -Po 'pipeline \K[^ (]*'`

    echo "Best pipeline is: " $BESTPIPELINE
    echo "Best pipeline is: " $BESTPIPELINE >> $1_results.log

    # Test
    docker run -i --entrypoint /bin/bash -v $3/config/test_config.json:/home/code/config.json -v $3/data/input/$1:/inputs/ -v $3/data/outputs/$1:/outputs/ registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:latest -c "/outputs/executables/$BESTPIPELINE.py /home/code/config.json"

    # Validate
    echo "NIST Baseline Score:" >> $1_results.log
    python nist_eval_output_validation_scoring/validate_runner.py $3/data/input/$1/mitll_predictions.csv $3/data/input/$1/SCORE/ $3/data/input/$1/SCORE/targets.csv >> $1_results.log
    echo "SRI Score:" >> $1_results.log
    python nist_eval_output_validation_scoring/validate_runner.py $3/data/outputs/$1/results/predictions.csv $3/data/input/$1/SCORE/ $3/data/input/$1/SCORE/targets.csv >> $1_results.log

    # Evaluate
    python  code/evaluate.py data/outputs/$1/results/predictions.csv data/input/$1/SCORE/targets.csv $2 >> $1_results.log

    echo " " >> $1_results.log
done