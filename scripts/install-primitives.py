#!/usr/bin/env python3

import argparse
import json
import os.path
import subprocess
import sys
from distutils import version
from urllib import parse as url_parse

import frozendict

# To have both stderr and stdout interleaved together.
sys.stderr = sys.stdout

parser = argparse.ArgumentParser(description="Install primitives from primitive annotations.")
parser.add_argument(
    '-a', '--annotations',
    required='D3M_INTERFACE_VERSION' not in os.environ,
    help="directory with primitive annotations to install"
)
parser.add_argument(
    '-u', '--username',
    help="username to use for accessing gitlab.datadrivendiscovery.org"
)
parser.add_argument(
    '-p', '--password',
    help="password to use for accessing gitlab.datadrivendiscovery.org"
)
arguments = parser.parse_args()

if (arguments.username and not arguments.password) or (not arguments.username and arguments.password):
    parser.error("both --username and --password are required, or none")

if arguments.annotations:
    annotations_directory = arguments.annotations
else:
    # "D3M_INTERFACE_VERSION" environment variables comes from the core image.
    if os.environ['D3M_INTERFACE_VERSION'] == 'devel':
        # Use latest stable release for primitive annotations for devel image.
        known_versions = [version.LooseVersion(directory.name[1:]) for directory in os.scandir('primitives_repo') if directory.is_dir() and directory.name.startswith('v')]

        max_version = max(known_versions)

        annotations_directory = os.path.join('primitives_repo', 'v{max_version}'.format(max_version=max_version))

    else:
        annotations_directory = os.path.join('primitives_repo', os.environ['D3M_INTERFACE_VERSION'])


def make_hashable(obj):
    if isinstance(obj, dict):
        return frozendict.frozendict({make_hashable(key): make_hashable(value) for key, value in obj.items()})
    elif isinstance(obj, list):
        return tuple(make_hashable(value) for value in obj)
    else:
        return obj


def get_annotations():
    for dirpath, dirnames, filenames in os.walk(annotations_directory):
        for filename in filenames:
            if filename != 'primitive.json':
                continue

            annotation_path = os.path.join(dirpath, filename)

            print(">>> Installing '{annotation_path}'.".format(annotation_path=annotation_path), flush=True)

            with open(annotation_path, 'r') as annotation_file:
                yield json.load(annotation_file)


def run(args):
    subprocess.run(args, stdout=sys.stdout, stderr=sys.stdout, check=True, encoding='utf8')
    sys.stdout.flush()


def private_pip_uri_access(uri):
    """
    Convert a pip installation URI to something which can be accessed.
    """

    # "git+git@git.myproject.org:MyProject" format cannot be parsed with urlparse.
    if uri.startswith('git+git@'):
        return uri

    parsed_uri = url_parse.urlparse(uri)

    if parsed_uri.hostname != 'gitlab.datadrivendiscovery.org':
        # Not a private URI.
        return uri

    # Not a good practice, but this is primitive author's problem. They could just make a repository public.
    if parsed_uri.username and parsed_uri.password:
        # Private URI already has username and password.
        return uri

    # Remove anything before "@", if it exists.
    _, _, netloc = parsed_uri.netloc.rpartition('@')

    if 'CI_JOB_TOKEN' in os.environ:
        # If we are running on GitLab CI.
        parsed_uri = parsed_uri._replace(netloc='gitlab-ci-token:{CI_JOB_TOKEN}@{netloc}'.format(
            CI_JOB_TOKEN=os.environ['CI_JOB_TOKEN'],
            netloc=netloc,
        ))
    if 'D3M_GITLAB_USERNAME' in os.environ and 'D3M_GITLAB_PASSWORD' in os.environ:
        # If username and password were provided through environment, also inside GitLab CI.
        parsed_uri = parsed_uri._replace(netloc='{username}:{password}@{netloc}'.format(
            username=os.environ['D3M_GITLAB_USERNAME'],
            password=os.environ['D3M_GITLAB_PASSWORD'],
            netloc=netloc,
        ))
    elif arguments.username and arguments.password:
        # If username and password were provided.
        parsed_uri = parsed_uri._replace(netloc='{username}:{password}@{netloc}'.format(
            username=arguments.username,
            password=arguments.password,
            netloc=netloc,
        ))
    else:
        raise Exception("A gitlab.datadrivendiscovery.org URI encountered, but no username and password provided.")

    return url_parse.urlunparse(parsed_uri)


already_installed_entries = set()
already_updated_apt = False
for annotation in get_annotations():
    try:
        for installation_entry in annotation['installation']:
            # Optimization. If something is already installed, we can skip it.
            # Many primitives share same dependencies.
            hashable_installation_entry = make_hashable(installation_entry)
            if hashable_installation_entry in already_installed_entries:
                continue
            # We add it immediately and if it fails, we also do not retry it in the devel image.
            already_installed_entries.add(hashable_installation_entry)

            if installation_entry['type'] == 'PIP' and 'package' in installation_entry:
                registry = installation_entry.get('registry', 'https://pypi.python.org/simple')
                print(">>> Installing Python package '{package}=={version}' from '{registry}'.".format(package=installation_entry['package'], version=installation_entry['version'], registry=registry), flush=True)
                run(['pip3', 'install', '--process-dependency-links', '--upgrade', '--upgrade-strategy', 'only-if-needed', '--index-url', registry, '{package}=={version}'.format(package=installation_entry['package'], version=installation_entry['version'])])

            elif installation_entry['type'] == 'PIP' and 'package_uri' in installation_entry:
                print(">>> Installing Python package '{package_uri}'.".format(package_uri=installation_entry['package_uri']), flush=True)
                # git+git scheme is not supported, and other URIs can be parsed with urlparse.
                parsed_uri = url_parse.urlparse(installation_entry['package_uri'])

                # Not a git URI. Just directly install, without "--editable" argument.
                if not parsed_uri.scheme.startswith('git'):
                    package_uri = url_parse.urlunparse(parsed_uri)

                    # Add username and password for private URIs.
                    package_uri = private_pip_uri_access(package_uri)

                    run(['pip3', 'install', '--process-dependency-links', '--upgrade', '--upgrade-strategy', 'only-if-needed', package_uri])

                else:
                    # To install with "--editable" argument an "egg" fragment argument has to be provided.
                    # We parse it here and set it to Python path to assure it is unique, if it does not yet
                    # exist. We used to not require it in v2017.12.27 and v2018.1.5 interface versions.
                    if parsed_uri.fragment:
                        parsed_fragment = url_parse.parse_qs(parsed_uri.fragment, strict_parsing=True)
                    else:
                        parsed_fragment = {}

                    if 'egg' not in parsed_fragment:
                        parsed_fragment['egg'] = [annotation['python_path']]

                    parsed_uri = parsed_uri._replace(fragment=url_parse.urlencode(parsed_fragment, doseq=True, safe='/', quote_via=url_parse.quote))

                    package_uri = url_parse.urlunparse(parsed_uri)

                    # Add username and password for private URIs.
                    package_uri = private_pip_uri_access(package_uri)

                    # We install with "--editable" so that packages can have access to their git repositories.
                    # For example, they might need it to compute installation git commit hash for their metadata.
                    run(['pip3', 'install', '--process-dependency-links', '--upgrade', '--upgrade-strategy', 'only-if-needed', '--editable', package_uri])

            elif installation_entry['type'] == 'UBUNTU':
                print(">>> Installing Ubuntu package '{package}'.".format(package=installation_entry['package']), flush=True)
                if not already_updated_apt:
                    run(['apt-get', 'update', '-q', '-q'])
                    already_updated_apt = True
                run(['apt-get', 'install', '--yes', '--force-yes', '--no-install-recommends', installation_entry['package']])

    except Exception as error:
        if arguments.annotations:
            raise
        elif os.environ['D3M_INTERFACE_VERSION'] == 'devel':
            print(">>> Exception during installation but ignoring because it is a devel image: {error}".format(error=error), flush=True)
        else:
            raise


print(">>> All available primitives:", flush=True)
run(['python3', '-m', 'd3m.index', 'search'])
