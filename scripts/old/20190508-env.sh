# Run this script from the tpot-ta2 home directory so that the following dependencies will be installed as peers
# modules

# If the environment is already in place run the following to purge it before recreating it.
# conda remove -n 20190508 --all
# First create the environment
conda env create -f scripts/20190508-env.yml
# Switch to the environment so that we install the dependencies into the target environment
source activate 20190508

# Install common primitives:
cd ..
git clone --recursive git@gitlab.com:datadrivendiscovery/common-primitives.git
cd common-primitives/
git checkout devel
# tensorflow-gpu is not currently available for mac so we will remove it before installing common-primitives
sed '/.*tensorflow\-gpu.*/d' setup.py > tmp.py
mv setup.py original-setup.py
mv tmp.py setup.py
pip install -e ./

# Install DSBox:
cd ..
pip install -e git+https://github.com/usc-isi-i2/dsbox-primitives@9ffd48258d3f3d7d8d116162b9ed1a01222a014e#egg=dsbox-primitives

# Install the Dummy TA3:
git clone https://gitlab.com/datadrivendiscovery/dummy-ta3.git
cd dummy-ta3
pip3 install .

# Install Gama:
cd ..
git clone https://github.com/PGijsbers/gama.git
cd gama
git checkout d3m
python setup.py install

# Install sklearn-wrap
cd ..
pip install -e git+https://gitlab.com/datadrivendiscovery/sklearn-wrap.git@dist#egg=sklearn_wrap

# Install the ta3-ta2api
git clone https://gitlab.com/datadrivendiscovery/ta3ta2-api.git
cd ta3ta2-api
git checkout v2019.4.11

# Install datamart-api
cd ..
git clone https://github.com/usc-isi-i2/datamart.git
cd datamart
git checkout jun_test
pip install -e ./




