# Run this script from the autoflow home directory

# If the environment is already in place run the following to purge it before recreating it.
# conda remove -n jido --all

# First create the environment
conda env create -f scripts/jido_env.yml

# Switch to the environment so that we install the dependencies into the target environment
source activate jido

if [ $? -eq 0 ]; then
    echo Environment activated. Start install to env.
else
    echo Failed to activate environment. ABORT
    exit $?
fi