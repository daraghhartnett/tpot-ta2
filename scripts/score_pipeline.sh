#!/bin/bash

INPUT="/Users/pieter/data/seed_datasets_current/"
OUTPUT="/Users/pieter/201907011/data/output/"

DATASET="LL1_736_population_spawn_simpler"
DATASET="LL1_VTXC_1369_synthetic"
INPUT_DATA=$INPUT$DATASET"/"

for PIPELINE in "$*"
do 
 PIPELINE_PATH=$OUTPUT$DATASET"/pipelines_ranked/"$PIPELINE".json"
 PROBLEM_PATH=$INPUT_DATA$DATASET"_problem/problemDoc.json"
 TRAIN_PATH=$INPUT_DATA"TRAIN/dataset_TRAIN/datasetDoc.json"
 TEST_PATH=$INPUT_DATA"TEST/dataset_TEST/datasetDoc.json"
 SCORE_PATH=$INPUT_DATA"SCORE/dataset_SCORE/datasetDoc.json"
 PREDICTIONS_PATH=$OUTPUT"predictions/"$PIPELINE"/predictions.csv"
 RUN_PATH=$OUTPUT"pipeline_runs/"$PIPELINE"/pipeline_run.yml"
 SCORE_OUTPUT=$OUTPUT"scores/"$PIPELINE"/scores.csvi"
 echo python3 -m d3m runtime fit-score -p $PIPELINE_PATH -r $PROBLEM_PATH -i $TRAIN_PATH -t $TEST_PATH -a $SCORE_PATH
 ~/miniconda3/envs/20190711/bin/python3 -m d3m runtime fit-score -p $PIPELINE_PATH -r $PROBLEM_PATH -i $TRAIN_PATH -t $TEST_PATH -a $SCORE_PATH
 echo ----
 echo 
done


