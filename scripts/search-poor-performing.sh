echo "Radon:"
docker run -i --entrypoint /bin/bash -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/config/search_config.json:/home/code/config.json -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/input/26_radon_seed:/inputs/ -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/outputs/26_radon_seed:/outputs/ --entrypoint=ta2_search registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:stable /home/code/config.json

echo "Spectrometer"
docker run -i --entrypoint /bin/bash -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/config/search_config.json:/home/code/config.json -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/input/313_spectrometer:/inputs/ -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/outputs/313_spectrometer:/outputs/ --entrypoint=ta2_search registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:stable /home/code/config.json

echo "MiceProtein"
docker run -i --entrypoint /bin/bash -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/config/search_config.json:/home/code/config.json -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/input/4550_MiceProtein:/inputs/ -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/outputs/4550_MiceProtein:/outputs/ --entrypoint=ta2_search registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:stable /home/code/config.json
