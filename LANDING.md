## SRI Autoflow ML System

Welcome to SRI International's ML System landing page. This page links to all the resources you will need to use 
Autoflow. There are three main sets of documentation:

### [Autoflow](https://gitlab.com/daraghhartnett/tpot-ta2/-/blob/master/README.md)
Outlines how to install and use Autoflow ML system. The readme takes the user through the process of installing,
configuring and running the Autoflow system on a classification and regression problem. The readme is located in the 
gitlab repository that houses the Autoflow code base. This code is open source.

*Who should read this?*  Casual users of Autoflow are advised to start here by following the 
[pip install instructions](https://gitlab.com/daraghhartnett/tpot-ta2#pip). 

### [D3M Documentation](https://datadrivendiscovery.org/)
Documentation for the underlying D3M architecture that Autoflow uses to find primitives to solve problems. Provides
resources for both the user's and developer's. Autoflow is considered a user of the D3M system as it relies on the
interfaces, schema and primitives included in D3M.  

*Who should read this?* Users that want a better understanding of the underlying schemas, API's and capabilities
offered by the D3M ecosystem. This insight will enable the user to enhance Autoflow to use more elaborate 
combinations of primitives to solve problems. Also suitable for developers intending to add new primitives to the 
D3M environment to add functionality not already present in the system. 

### [GAMA](https://github.com/PGijsbers/gama/blob/d3m/README.md)
GAMA (General Automated Machine learning Assistant) is the Genetic Algorithm library developed by Pieter Gijsbers and Joaquin Vanschoren at the Eindhoven University of Technology that 
is used to construct, evaluate and improve the pipelines that solves problems for Autoflow. This is considered the
engine of the Autoflow ML system. GAMA is modular which allows the genetic algorithm to be replaced by others such as
ASHA (hyperband).

*Who should read this?* Developers that want a deeper understanding of how the pipeline search is being executed.

### System Overview

<img src="docs/images/system_overview.png" alt="System Overview" style="float: left; margin-right: 10px;" />
     
