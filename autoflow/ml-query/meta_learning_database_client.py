import logging
import plac
import sys
from d3m_metalearning_database import D3MMetalearningDatabase

# init logger
logging.basicConfig(level=logging.INFO, format='%(asctime)s [%(levelname)s] %(name)s -- %(message)s')
_logger = logging.getLogger(__name__)


'''
Sample usage of the MetalearningDatabaseAPI class to drive development of the API
'''
def main(number_of_top_scoring_pipelines, minimum_number_of_primitive_occurrences):

    print("Running with top_K (Number of top scoring pipelines to consider as in the \"top\") = %s" % number_of_top_scoring_pipelines)
    print("Running with Y (Number of instances a primitive must be used to be included in PMI) = %s" % minimum_number_of_primitive_occurrences)

    classification_datasets = ["1491_one_hundred_plants_margin_MIN_METADATA", "1567_poker_hand_MIN_METADATA", "185_baseball_MIN_METADATA", "27_wordLevels_MIN_METADATA", "299_libras_move_MIN_METADATA", "30_personae_MIN_METADATA", "313_spectrometer_MIN_METADATA", "32_wikiqa_MIN_METADATA", "38_sick_MIN_METADATA", "4550_MiceProtein_MIN_METADATA", "57_hypothyroid_MIN_METADATA", "LL0_1100_popularkids_MIN_METADATA", "LL0_186_braziltourism_MIN_METADATA", "LL1_GS_process_classification_tabular_MIN_METADATA", "LL1_GS_process_classification_text_MIN_METADATA", "LL1_TXT_CLS_3746_newsgroup_MIN_METADATA", "LL1_TXT_CLS_airline_opinion_MIN_METADATA", "LL1_TXT_CLS_apple_products_sentiment_MIN_METADATA", "loan_status_MIN_METADATA", "uu10_posts_3_MIN_METADATA", "uu4_SPECT_MIN_METADATA", "uu5_heartstatlog_MIN_METADATA", "uu6_hepatitis_MIN_METADATA", "uu7_pima_diabetes_MIN_METADATA"]
    regression_datasets = ["196_autoMpg_MIN_METADATA", "26_radon_seed_MIN_METADATA", "534_cps_85_wages_MIN_METADATA", "LL0_207_autoPrice_MIN_METADATA", "LL1_retail_sales_total_MIN_METADATA", "kaggle_music_hackathon_MIN_METADATA", "uu2_gp_hyperparameter_estimation_MIN_METADATA", "uu3_world_development_indicators_MIN_METADATA", "uu3_world_development_indicators_raw", "uu8_posts_1_MIN_METADATA", "uu9_posts_2_MIN_METADATA"]

    # Produce some usage instance statistics about the classification  and regression primitives
    # generate_top_primitive_statistics(datasets=classification_datasets, name="classification")
    # generate_top_primitive_statistics(datasets=regression_datasets, name="regression")

    # Generate point-wise mutual information about the relative usefulness of classification and regression primitives
    # generate_pmi_for_primitives(datasets=classification_datasets, name="classification",
    #                             top_K=int(number_of_top_scoring_pipelines), Y=int(minimum_number_of_primitive_occurrences))
    # generate_pmi_for_primitives(datasets=regression_datasets, name="regression",
    #                             top_K=int(number_of_top_scoring_pipelines), Y=int(minimum_number_of_primitive_occurrences))

    get_top_pipeline_runs_by_size(datasets=classification_datasets)


def get_top_pipeline_runs_by_size(datasets):
    # gary = sys.getsizeof(response.hits.hits._l_[3])

    # 01: Initial setup
    mlDatabase = D3MMetalearningDatabase()
    from pympler import asizeof

    # 02: Harvest 100 largest pipeline runs
    pipeline_runs_with_size = {}
    for dataset in datasets:
        pipeline_runs = mlDatabase.get_pipeline_runs(dataset_name=dataset)
        for pipeline_run in pipeline_runs:
            # print("size for %s is: %s" % (pipeline_run.id, asizeof.asizeof(pipeline_run)))
            pipeline_runs_with_size[pipeline_run.id] = pipeline_run
        # Just do one dataset for now.
        # break

    sorted_pipeline_runs_by_size = sorted(pipeline_runs_with_size.items(), key=lambda kv: asizeof.asizeof(kv[1]), reverse=True)

    # 03: Print the % portion of the pipeline_run the predicted labels takes up:
    for pipeline_run in sorted_pipeline_runs_by_size:
        id = pipeline_run[0]
        total_size = float(asizeof.asizeof(pipeline_run[1]))
        predictions_size = float(asizeof.asizeof(pipeline_run[1].run.results.predictions))
        steps_size = float(asizeof.asizeof(pipeline_run[1].steps))

        print("\nPipeline_run: %s. Total Size: %s. Size of predictions: %s. Percentage of total: %.2f" %
              (id, total_size, predictions_size, (predictions_size / total_size) * 100))
        print("\tSize of steps: %s. Percentage of total: %.2f" %
              (steps_size, (steps_size / total_size) * 100))


'''         
Suppose you have N pipelines in your sample (e.g., results for all classification problems), 
M of which are determined to be top-10 (or top-K -- you can make this a parameter of the metric).  
For a given primitive, let P (for "positive") be the number of top-K pipelines in which it occurs 
and T (for "total") the total number of pipelines in which it occurs.  Try calculating the following metric:

    Version 1.0:
    PMI = (P/M) / (T/N)

    Version 1.1:
    - Suppress any primitive that doesn't occur in at least Y (10) pipelines.
    - Calculate PMI=((1 + P) / (2 + M)) / ((1 + T) / (2 + N)).  This should smooth the statistics somewhat.

'''
def generate_pmi_for_primitives(datasets, name, top_K = 10, Y = 10):

    # 01: Initial setup
    mlDatabase = D3MMetalearningDatabase()

    # 02: Get N - the total number of pipelines for the datasets (of a specific problem type) supplied
    N = 0
    print("\nCalculate N...")
    for dataset in datasets:
        count = mlDatabase.get_pipeline_runs_count(dataset_name=dataset)
        print("\tDataset: %s. Pipeline_run instances: %s" % (dataset, count))
        N += count
    print("N is %s" % N)

    # 03: Get M - the number of pipelines that are in the top k scoring pipeline_runs for the datasets (of a specific
    # problem type) supplied. Note: M will always be 'k' * 'number of datasets' unless it is larger then the number
    # of available pipeline_runs for certain problems
    M = 0
    print("\nCalculate M...")
    for dataset in datasets:
        count = mlDatabase.get_pipeline_runs_count(dataset_name=dataset, top_K=top_K)
        print("\tDataset: %s. Pipeline_run instances: %s" % (dataset, count))
        M += count
    print("M is %s" % M)

    # 04: Get the P's - For a given primitive, let P (for "positive") be the number of top-K pipelines in which it
    # occurs
    primitives_P = {}
    print("\nGet P (TOP K primitive instances)...")
    for dataset in datasets:
        primitive_instances = mlDatabase.get_primitive_instance_counts_in_pipeline_runs_for_dataset(
                                    dataset_name=dataset,
                                    limit=top_K)
        print("\tDataset: %s. Primitive count: %s. Total instances: %s" % (dataset, len(primitive_instances),
                                                                          sum(primitive_instances.values())))
        primitives_P.update(primitive_instances)
    print("primitives_P's calculated")

    # 05: Get the T's - For a given primitive, let T be the total number of pipelines in which it occurs
    primitives_T = {}
    print("\nGet T (ALL primitive instances)...")
    for dataset in datasets:
        primitive_instances = mlDatabase.get_primitive_instance_counts_in_pipeline_runs_for_dataset(
                                    dataset_name=dataset,
                                    limit=None)
        print("\tDataset: %s. Primitive count: %s. Total instances: %s" % (dataset, len(primitive_instances),
                                                                          sum(primitive_instances.values())))
        primitives_T.update(primitive_instances)
    print("primitives_T's calculated")

    print("\nPrimitive instance histogram:")
    sorted_primitives_T = sorted(primitives_T.items(), key=lambda kv: kv[1], reverse=True)
    for primitive in sorted_primitives_T:
        print("\tPrimitive: %s\t Occurrences: %s" % (primitive[0], primitive[1]))

    # 06: Version 1: Calculate PMI = (P/M) / (T/N) for each primitive
    # 06: Version 2: Calculate PMI = ((1 + P)/(2 + M)) / ((1 + T)/(2 + N))
    primitives_PMI = {}
    # We will cycle over the T's since this should be a full list of the primitives used on the datasets
    for primitive in primitives_T:
        P = 0
        if primitive in primitives_P:
            P = primitives_P[primitive]
        T = primitives_T[primitive]
        # Only consider primitives that occur in at least Y pipelines
        if T >= Y:
            # primitives_PMI[primitive] = (P / M) / (T / N)
            primitives_PMI[primitive] = ((1 + P) / (2 + M)) / ((1 + T) / (2 + N))

    # 07 Order the results
    sorted_primitives_PMI = sorted(primitives_PMI.items(), key=lambda kv: kv[1], reverse=True)

    print("\nPMI = ((1 + P)/(2 + M)) / ((1 + T)(2 + N)) for primitives (%s total) used to solve %s problems:" % (len(sorted_primitives_PMI), name))
    for prim in sorted_primitives_PMI:
        print("\tPrimitive: \"%s\", PMI: %s" % (prim[0], prim[1]))


'''
Query the Meta Learning Database for the top 10 scoring pipeline_runs for classification and regression. For each of the
pipeline_runs, retrieve the pipeline, the memory allocation, predictions and hyperparameter settings. Generate a list of
primitive occurrences accross all of those pipelines and sort them by the number of times they show up in the 
classification and regression pipelines.
'''
def generate_top_primitive_statistics(datasets, name):
    # 01: Initial setup
    mlDatabase = D3MMetalearningDatabase()

    # 02: Get the top 10 pipeline_run objects for the supplied datasets
    top_pipeline_runs = []
    for dataset in datasets:
        top_pipeline_runs.extend(mlDatabase.get_pipeline_runs_sorted_by_score(dataset_name=dataset))
    _logger.info("Got %s %s pipeline_runs" % (len(top_pipeline_runs), name))

    # 03: Get the pipeline for each of the pipeline runs
    pipelines = list()
    for pipeline_run in top_pipeline_runs:
        pipeline = mlDatabase.get_pipeline(pipeline_run['pipeline']['id'])
        pipelines.append(pipeline)
        # Memory used during the run:
        # memory = pipeline_run['environment']['resources']['memory']['total_memory']
        # Predictions produced during the run:
        # predictions = pipeline_run['run']['results']['predictions']
        # Hyper-parameter settings:
        # pipeline['steps'][3]['hyperparams']

    # 04: Get the list of primitives that appear in each pipeline and count their occurrences
    primitives = {}
    for pipeline in pipelines:
        for primitive in pipeline['steps']:
            name = primitive['primitive']['name']
            if name not in primitives:
                primitives[name] = 0
            primitives[name] += 1

    # 05: Create a sorted list of which primitives occur most frequently in top scoring pipelines
    top_primitives = sorted(primitives.items(), key=lambda kv: kv[1], reverse=True)
    print("Top ranked classification primitives (%s):" % len(top_primitives))
    for prim in top_primitives:
        print("\tPrimitive: \"%s\", Occurrences in top 10 pipelines for all %s problems: %s" % (
        prim[0], name, prim[1]))


'''
Entry point - required to make python happy
'''
if __name__ == "__main__":
    plac.call(main)