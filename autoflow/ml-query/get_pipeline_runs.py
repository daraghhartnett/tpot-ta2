from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search, FacetedSearch, TermsFacet, A, query

client = Elasticsearch(hosts="https://metalearning.datadrivendiscovery.org/es")

## Get a pipeline with a specific id

# s = Search(using=client, index="pipelines") \
#     .query("match", id="9310a9c0-3b9f-4255-b926-c1d869ad5f22")
#
# response = s.execute()
#
# print(response)
#
# for hit in response:
#     print(hit.meta.score, hit.id)
#
# print("\n")


## Get a pipeline_run with a specific id

# The index limits what 'index' should be queried.
# s = Search(using=client, index="pipeline_runs") \
#     .query("match", id="44b7fb44-b842-5866-943d-f1939967c041")
#
# response = s.execute()
#
# print(response)
#
# for hit in response:
#     print(hit.meta.score, hit.id)
#
# print("\n")

## Get pipeline_runs for a specific dataset and report the number of hits

# Search results will be cached. Subsequent calls to execute or trying to iterate over an already executed
# Search object will not trigger additional requests being sent to Elasticsearch. To force a request specify
# ignore_cache=True when calling execute.
# >This is currently not working ad advertised - setting the value seems to have
# no effect. So how can we access subsequent hits? Punt for now
# s = Search(using=client, index="pipeline_runs") \
#     .query("match", datasets__id="LL1_336_MS_Geolife_transport_mode_prediction_separate_lat_lon_dataset_TRAIN")
#
# response = s.execute(ignore_cache=False)
# # This does report the correct number of hits thankfully
# print("Number of hits: %s" % response.hits.total)
# for hit in response:
#     print(hit.meta.score, hit.id)
#
# response = s.execute(ignore_cache=True)
# print("Number of hits: %s" % response.hits.total)
# for hit in response:
#     print(hit.meta.score, hit.id)

# print(response)


## Iterate through pipeline_runs to get scores (since scores are not indexed)
# OR harvest the scores off the leader board and use those to query the ML db (but that will lag quite a bit)
# OR use the downloaded files to pull the data from (files are massive - no point reinventing ES service)

# s = Search(using=client, index="pipeline_runs") \
#     .query("match", datasets__id="LL1_336_MS_Geolife_transport_mode_prediction_separate_lat_lon_dataset_TEST")
#
# response = s.execute(ignore_cache=False)
# # This does report the correct number of hits thankfully
# print("Number of hits: %s" % response.hits.total)
# for hit in response:
#     print(hit.meta.score, hit.id)
#     print("Score is: %s", hit.run.results.scores[0].value)


## Use a faceted search to get top scores first in returned pipeline_runs

# # this next snippet accurately finds the pipeline runs we want to preserve it while we figure out the filtering...
# class TopScoreSearch(FacetedSearch):
#     # doc_types = ['user', 'post']
#     using = client
#     index = "pipeline_runs"
#
#     fields = ('run.results.scores[0].value')
#
#     facets = {
#         'datasetid': TermsFacet(field='datasets.id'),
#     }
#
# s = TopScoreSearch(filters={"datasetid": "LL1_336_MS_Geolife_transport_mode_prediction_separate_lat_lon_dataset_TEST"})
#
# response = s.execute()
# print("Number of hits: %s" % response.hits.total)
# for hit in response:
#     print(hit.meta.score, hit.id)
#     print("Score is: %s", hit.run.results.scores[0].value)
#
# print("\nwith the metric\n")

## Get the date metric filtering to work:
# This will return documents ordered by ingestion date - preserve and try do the same for score
class TopScoreSearch(FacetedSearch):
    using = client
    index = "pipeline_runs"

    facets = {
        'datasetid': TermsFacet(field='datasets.id'),
        # Example of using a facet to sort by date
        # 'id': TermsFacet(field='id', metric=A('max', field='_ingest_timestamp')),
        # NOTE: This now appears to work since Sujen fixed https://gitlab.com/datadrivendiscovery/metalearning/-/issues/137
        # but as noted in the issue ES no longer supports Facets - what? Why does this work? It does seem to sort in the
        # wrong order so for these reasons I do not have a lot of confidence in the features
        'id': TermsFacet(field='id', metric=A('max', field='run.results.scores.value')),
    }

s = TopScoreSearch()

response = s.execute()
print("Number of hits: %s" % response.hits.total)
for hit in response.facets.id:
    print("ID: %s Score: %s" % (hit[0], hit[1]))


## Get the date metric filtering to work:
# class TopScoreSearch(FacetedSearch):
# #
# #     using = client
# #     index = "pipeline_runs"
# #
# #     facets = {
# #         #'id': TermsFacet(field='id', metric=A('max', field='_ingest_timestamp')),
# #         'datasetid': TermsFacet(field='datasets.id'),
# #         'id': TermsFacet(field='id', metric=A('max', field='run')),
# #     }
# #
# # s = TopScoreSearch(filters={'datasetid': "LL1_336_MS_Geolife_transport_mode_prediction_separate_lat_lon_dataset_TEST"})
# #
# # response = s.execute()
# # print("Number of hits: %s" % response.hits.total)
# # for hit in response.facets.id:
# #     # Does not return anything for the run metric
# #     print("ID: %s Score: %s" % (hit[0], hit[1]))


# ## Iterate through pipeline_runs to get scores (since scores are not indexed)
#
s = Search(using=client, index="pipeline_runs") \
    .query("match", datasets__id="LL1_336_MS_Geolife_transport_mode_prediction_separate_lat_lon_dataset_TEST")

# This is a hack to get us the full set of responses since the caching is broken
total = s.count()
s = s[0:total]

response = s.execute(ignore_cache=True)
print("Total number of pipeline runs for LL1_336_MS_Geolife: %s" % response.hits.total)

id_score_list = list()
average = 0.0
metric = response.hits[0].run.results.scores[0].metric.metric
for hit in response:
    id = hit.id
    score = hit.run.results.scores[0].value
    average += float(score)
    id_score_list.append([id, score])
    # print("ID: %s. Score is: %s" % (id, score))

average /= total

# function to return the score of the pair of elements passed as the parameter
def sort_by_score(id_score):
    return id_score[1]

id_score_list.sort(key=sort_by_score, reverse=True)

# Print all values
# for pair in id_score_list:
    # print("ID: %s. Score is: %s" % (pair[0], pair[1]))

print("\tMetric: %s" % metric)
print("\tMinimum Score: %s" % id_score_list[total - 1] [1])
print("\tMaximum Score: %s" % id_score_list[0] [1])
print("\tAverage Score: %s" % average)
