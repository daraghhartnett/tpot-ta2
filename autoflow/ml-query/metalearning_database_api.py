

class MetalearningDatabaseAPI(object):

    '''
    Given a dataset name return the top scoring pipeline_run structures as a dict.
     - 'limit' sets the number of the top pipeline runs to return
     - 'reverse' sorts the pipeline runs from highest score to lowest score if True, lowest to highest otherwise. This
       is useful for getting desired values when the metric means smaller numbers are better. 
    '''
    def get_pipeline_runs(self, dataset_name, limit=10, reverse=True):
        raise NotImplementedError()

    ''' 
    Given an pipeline_id, return the corresponding pipeline structure
    '''
    def get_pipeline(self, pipeline_id):
        raise NotImplementedError()
