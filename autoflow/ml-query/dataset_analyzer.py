import logging
import os
import plac
import json

# init logger
logging.basicConfig(level=logging.INFO, format='%(asctime)s [%(levelname)s] %(name)s -- %(message)s')
_logger = logging.getLogger(__name__)

'''
Convenience module for getting the list of datasets that match certain criteria
'''
def main():
    problem_list = sorted(get_problem_list("/datasets/seed_datasets_current"))

    classification_list = list()
    regression_list = list()

    for problem in problem_list:
        exclude = ["image", "audio", "timeSeries", "geospatial", "video"]
        task_type = havest_task_type(problem=problem, dataset_home="/datasets/seed_datasets_current", exclude=exclude)

        if task_type == "classification":
            classification_list.append(problem)
        elif task_type == "regression":
            regression_list.append(problem)

    classification_string = ""
    for dataset in classification_list:
        classification_string += ", \"%s\"" % dataset
    regression_string = ""
    for dataset in regression_list:
        regression_string += ", \"%s\"" % dataset

    print("Classification: %s" % classification_string[2:])
    print("Regression: %s" % regression_string[2:])



'''
Returns a list of problem names from the seed data set directory indicated in the environment variable
'''
def get_problem_list(dataset_home):
    problem_list = []

    exists = os.path.isdir(dataset_home)
    _logger.info("Dataset directory exists? %s" % exists)
    readable = os.access(dataset_home, os.R_OK)
    _logger.info("Dataset directory readable? %s" % readable)

    directories_list = next(os.walk(dataset_home))[1]
    for directory in directories_list:
        # Ensure this is an actual problem dir
        sub_directories_list = next(os.walk("%s/%s" % (dataset_home, directory)))[1]
        for sub_directory in sub_directories_list:
            if sub_directory == "SCORE":
                # This is a legit problem dir
                problem_list.append(directory)
                break
    return problem_list


'''
Used to retrieve the task type from the problemDoc.json
'''
def havest_task_type(problem, dataset_home, exclude):

    taskKeyword_taskType_set = {
        "classification",
        "regression",
        "linkPrediction",
        "vertexNomination",
        "vertexClassification",
        "graphClustering",
        "graphMatching",
        "collaborativeFiltering",
        "objectDetection",
        "communityDetection",
        "semiSupervised"
    }

    problemDoc = json.load(open('%s/%s/TRAIN/problem_TRAIN/problemDoc.json' % (dataset_home, problem)))
    taskKeywords = problemDoc["about"]["taskKeywords"]

    if exclude is not None:
        if len(set(exclude).intersection(taskKeywords)) > 0:
            return None

    taskType = taskKeyword_taskType_set.intersection(taskKeywords)
    if 'timeSeries' in taskKeywords and 'forecasting' in taskKeywords:
        task_type = 'timeSeriesForecasting'
    elif 'timeSeries' in taskKeywords and 'classification' in taskKeywords:
        task_type = 'timeSeriesClassification'
    elif 'semiSupervised' in taskKeywords and 'classification' in taskKeywords:
        task_type = 'semiSupervisedClassification'
    elif len(taskType) == 1:
        task_type = taskType.pop()
    elif len(taskType) == 0:
        _logger.warning("Supported Task Type is not found in taskKeywords: %s from problemDoc" % taskKeywords)
        return None
    elif len(taskType) > 1:
        _logger.warning("More than one Supported Task Type Task Type found in taskKeywords: %s from problemDoc."
                        " Continuing with the first one detected" % taskKeywords)
        task_type = taskType.pop()
    else:
        return None
    # _logger.info("Task Type selected is: %s" % task_type)
    return task_type

'''
Entry point - required to make python happy
'''
if __name__ == "__main__":
    plac.call(main)