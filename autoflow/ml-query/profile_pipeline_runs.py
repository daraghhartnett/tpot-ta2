import os
import logging
import plac
import json
import time
from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search

# init logger
logging.basicConfig(level=logging.INFO, format='%(asctime)s [%(levelname)s] %(name)s -- %(message)s')
_logger = logging.getLogger(__name__)

'''
For each of the seed datasets - query the ML Library for information about the available pipeline_runs 
'''
def main(datasets_home, ml_library_url):

    # First get the list of all current seed datasets
    _logger.info("Dataset path: %s" % datasets_home)
    problem_list = sorted(get_problem_list(datasets_home))

    # Establish the link to the ES ML Library
    client = Elasticsearch(hosts=ml_library_url)
    search_handle = Search(using=client, index="pipeline_runs").query("wildcard", datasets__id="*dataset_TEST")
    print("Total Number of Pipeline Runs in ML Database: %s" % search_handle.count())

    task_type_stats = {}
    index = 1
    for problem in problem_list:
        problem_id = "%s%s" % (problem, "_dataset_TEST")

        task_type = havest_task_type(problem=problem, dataset_home=datasets_home)

        # Establish the link to the ES ML Library
        client = Elasticsearch(hosts=ml_library_url)
        search_handle = Search(using=client, index="pipeline_runs") \
            .query("match", datasets__id=problem_id)
        search_handle.params(request_timeout=300)
        search_handle.params(retry_on_timeout=True)

        # This is a hack to get us the full set of responses since the caching is broken
        total = search_handle.count()
        search_handle = search_handle[0:total]
        if task_type not in task_type_stats:
            task_type_stats[task_type] = 0
        task_type_stats[task_type] += total

        success = False
        attempts = 5
        for i in range (0, attempts):
            try:
                response = initialte_search(search_handle)
                success = True
            except Exception as e:
                _logger.info("Got a timeout: %s" % e)
                time.sleep(5)
            if success:
                break
        if success == False:
            _logger.warning("Was unable to make connection of ML Library ES after %s attempts" % attempts)

        print("Dataset (%s of %s): %s" % (index, len(problem_list), problem_id))
        print("\tTask Type: %s" % task_type)
        print("\tPipeline_run count: %s" % response.hits.total)
        index += 1

        id_score_list = list()
        average = 0.0
        metric = response.hits[0].run.results.scores[0].metric.metric
        for hit in response:
            id = hit.id

            if hasattr(hit.run, "results"):
                if hasattr(hit.run.results, "scores"):
                    score = hit.run.results.scores[0].value
                else:
                    _logger.info("Encountered pipeline_run with no score")
                    continue
            else:
                _logger.info("Encountered pipeline_run with no results object")
                continue
            average += float(score)
            id_score_list.append([id, score])

        average /= total

        id_score_list.sort(key=sort_by_score, reverse=True)

        print("\tMetric: %s" % metric)
        print("\tMinimum Score: %s" % id_score_list[len(id_score_list) - 1][1])
        print("\tMaximum Score: %s" % id_score_list[0][1])
        print("\tAverage Score: %s" % average)

    print("Task Type Summary")
    for task_type in task_type_stats:
        print("\tTask Type %s has %s pipeline_runs" % (task_type, task_type_stats[task_type]))


def initialte_search(search_handle):
    response = search_handle.execute()
    return response


'''
Used to retrieve the task type from the problemDoc.json
'''
def havest_task_type(problem, dataset_home):

    taskKeyword_taskType_set = {
        "classification",
        "regression",
        "linkPrediction",
        "vertexNomination",
        "vertexClassification",
        "graphClustering",
        "graphMatching",
        "collaborativeFiltering",
        "objectDetection",
        "communityDetection",
        "semiSupervised"
    }

    problemDoc = json.load(open('%s/%s/TRAIN/problem_TRAIN/problemDoc.json' % (dataset_home, problem)))
    taskKeywords = problemDoc["about"]["taskKeywords"]
    taskType = taskKeyword_taskType_set.intersection(taskKeywords)
    if 'timeSeries' in taskKeywords and 'forecasting' in taskKeywords:
        task_type = 'timeSeriesForecasting'
    elif 'timeSeries' in taskKeywords and 'classification' in taskKeywords:
        task_type = 'timeSeriesClassification'
    elif 'semiSupervised' in taskKeywords and 'classification' in taskKeywords:
        task_type = 'semiSupervisedClassification'
    elif len(taskType) == 1:
        task_type = taskType.pop()
    elif len(taskType) == 0:
        _logger.warning("Supported Task Type is not found in taskKeywords: %s from problemDoc" % taskKeywords)
        return None
    elif len(taskType) > 1:
        _logger.warning("More than one Supported Task Type Task Type found in taskKeywords: %s from problemDoc."
                        " Continuing with the first one detected" % taskKeywords)
        task_type = taskType.pop()
    else:
        return None
    # _logger.info("Task Type selected is: %s" % task_type)
    return task_type


'''
Allows us to sort the ids of the pipeline runs based on their corresponding score
'''
def sort_by_score(id_score):
    return id_score[1]

'''
Returns a list of problem names from the seed data set directory indicated in the environment variable
'''
def get_problem_list(dataset_home):
    problem_list = []

    exists = os.path.isdir(dataset_home)
    _logger.info("Dataset directory exists? %s" % exists)
    readable = os.access(dataset_home, os.R_OK)
    _logger.info("Dataset directory readable? %s" % readable)

    directories_list = next(os.walk(dataset_home))[1]
    for directory in directories_list:
        # Ensure this is an actual problem dir
        sub_directories_list = next(os.walk("%s/%s" % (dataset_home, directory)))[1]
        for sub_directory in sub_directories_list:
            if sub_directory == "SCORE":
                # This is a legit problem dir
                problem_list.append(directory)
                break
    return problem_list


'''
Entry point - required to make python happy
'''
if __name__ == "__main__":

    plac.call(main)