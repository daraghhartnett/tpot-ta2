import os
import logging
import plac
import json
import time
from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search
from metalearning_database_api import MetalearningDatabaseAPI

# init logger
logging.basicConfig(level=logging.INFO, format='%(asctime)s [%(levelname)s] %(name)s -- %(message)s')
_logger = logging.getLogger(__name__)
# Silence chatty DSL logs
logging.getLogger("elasticsearch").setLevel(logging.WARNING)

'''
This class is an interface to the Elastic Search DSL Python API: 
https://elasticsearch-dsl.readthedocs.io/en/latest/api.html 
'''
class D3MMetalearningDatabase(MetalearningDatabaseAPI):

    '''
    Create the class and member variables
    '''
    def __init__(self, url="https://metalearning.datadrivendiscovery.org/es"):
        self.url = url


    '''
    Given a dataset name return the top scoring pipeline_run structures as a dict.
     - 'limit' sets the number of the top pipeline runs to return. Default is to return all
     - 'reverse' sorts the pipeline runs from highest score to lowest score if True, lowest to highest otherwise. This
       is useful for getting desired values when the metric means smaller numbers are better. 
     - include_fields is useful for limiting what ES returns to your query. If you are querying a large object, such 
       as pipeline_runs with a bunch of predictions, and dont really need them, only specify the fields you want in this 
       variable, for example: ["id", "dataset.id"]
    '''
    def get_pipeline_runs_sorted_by_score(self, dataset_name, limit=None, reverse=True, include_fields=None):
        response = self.get_pipeline_runs(dataset_name, include_fields)

        # Parse through the list of pipeline_runs so we can return just the best ones
        # This will be easier in the future see https://gitlab.com/datadrivendiscovery/metalearning/-/issues/137
        # Once it is fixed we can use a metric facet - see get_pipeline_runs.py ~line 107
        pipeline_run_score_list = list()
        for hit in response:
            # Filter out the bad pipeline runs
            if hasattr(hit, "run"):
                if hasattr(hit.run, "results"):
                    if hasattr(hit.run.results, "scores"):
                        score = hit.run.results.scores[0].value
                    else:
                        _logger.debug("Encountered pipeline_run with no score")
                        continue
                else:
                    _logger.debug("Encountered pipeline_run with no results object")
                    continue
            else:
                _logger.debug("Encountered pipeline_run with no run object")
                continue
            # Keep track of the scores and ids
            pipeline_run_score_list.append([hit, score])

        # Sort the list of pipeline_run id's using their score, best to worst
        pipeline_run_score_list.sort(key=self._sort_by_score, reverse=reverse)

        best_pipelines_runs = list()
        if limit is None or limit > len(pipeline_run_score_list):
            limit = len(pipeline_run_score_list) - 1
        for i in range(0, limit):
            best_pipelines_runs.append(pipeline_run_score_list[i][0].to_dict())
        return best_pipelines_runs


    '''
    Get the pipeline_runs specified with no sorting applied
    '''
    def get_pipeline_runs(self, dataset_name, include_fields = None):
        # Build the query
        problem_id = "%s%s" % (dataset_name, "_dataset_TEST")
        client = Elasticsearch(hosts=self.url)
        search_handle = Search(using=client, index="pipeline_runs") \
            .query("match", datasets__id=problem_id) # \
            # TODO: Uncomment the following line (and the / above) to sort the pipeline_runs by score. This is now
            #       possible since Sujen fixed https://gitlab.com/datadrivendiscovery/metalearning/-/issues/137
            #       I decided to leave it commented out as I have not tested all the code with this new feature.
            # .sort("run.results.scores.value")
        search_handle.params(request_timeout=300)
        search_handle.params(retry_on_timeout=True)
        if include_fields is not None:
            search_handle = search_handle.source(include_fields)
        # Launch the query
        response = self._launch_query(search_handle)
        return response


    '''
    Given a dataset, get the top limit (default to all) pipeline runs by score (default is higher score is better, use 
    reverse to flip this). For each of these pipeline_runs, get the corresponding pipeline and the list of primitives 
    it uses. Add these counts to a map from primitive name to primitive usage instances.
    '''
    def get_primitive_instance_counts_in_pipeline_runs_for_dataset(self, dataset_name, limit=None, reverse=True):
        primitive_instances = {}

        # For the purposes of calculation PMI only pull the fields we will need.
        desired_fields = ['pipeline', 'run.results.scores']

        # Get the top_k pipeline_run objects for the supplied dataset
        top_pipeline_runs = self.get_pipeline_runs_sorted_by_score(dataset_name=dataset_name, limit=limit, reverse=reverse,
                                                                   include_fields=desired_fields)

        # Get the pipeline for each of the pipeline runs
        pipelines = list()
        # For the purposes of calculation PMI only pull the fields we will need.
        desired_fields = ['steps.primitive.python_path']
        for pipeline_run in top_pipeline_runs:
            pipeline = self.get_pipeline(pipeline_run['pipeline']['id'], include_fields=desired_fields)
            pipelines.append(pipeline)

        # Get the list of primitives that appear in each pipeline and count their occurrences
        for pipeline in pipelines:
            for primitive in pipeline['steps']:
                name = primitive['primitive']['python_path']
                if name not in primitive_instances:
                    primitive_instances[name] = 0
                primitive_instances[name] += 1

        return primitive_instances


    ''' 
    Given an pipeline_id, return the corresponding pipeline structure as a dict
    '''
    def get_pipeline(self, pipeline_id, include_fields=None):
        client = Elasticsearch(hosts=self.url)
        search_handle = Search(using=client, index="pipelines") \
            .query("match", id=pipeline_id)
        search_handle.params(request_timeout=300)
        search_handle.params(retry_on_timeout=True)

        if include_fields is not None:
            search_handle = search_handle.source(include_fields)

        # Launch the query
        response = self._launch_query(search_handle)
        return response.hits[0].to_dict()


    ''' 
    Given a dataset, return the corresponding number of pipeline_run instances.
    If top_K is set, return the corresponding number of pipeline_run instances that are in the top scoring instances as 
    defined by top_K. The response will typically be top_K unless there are insufficient pipeline_run instances 
    available 
    '''
    def get_pipeline_runs_count(self, dataset_name, top_K = None):
        # Build the query
        problem_id = "%s%s" % (dataset_name, "_dataset_TEST")
        client = Elasticsearch(hosts=self.url)
        search_handle = Search(using=client, index="pipeline_runs") \
            .query("match", datasets__id=problem_id) # \
            # TODO: Uncomment the following line (and the \ above) to sort the pipeline_runs by score. This is now
            #       possible since Sujen fixed https://gitlab.com/datadrivendiscovery/metalearning/-/issues/137
            #       I decided to leave it commented out as I have not tested all the code with this new feature.
            # .sort("run.results.scores.value")
        search_handle.params(request_timeout=300)
        search_handle.params(retry_on_timeout=True)

        # Launch the query
        total = search_handle.count()

        if top_K is None:
            return total
        else:
            # If the total number of pipelines is less than the desired top_K just return that, else the top_K
            if total <= top_K:
                return total
            return top_K


    '''
    Allows us to sort the pipeline runs based on their corresponding score
    '''
    def _sort_by_score(self, id_score):
        return id_score[1]


    '''
    Submit the provided query and return the response when it succeeds
    '''
    def _launch_query(self, search_handle):
        # This is a hack to get us the full set of responses since caching is broken
        total = search_handle.count()
        # Use scan here instead
        search_handle = search_handle[0:total]
        # Add some retry logic in case the request fails
        success = False
        attempts = 10
        response = None
        for i in range(0, attempts):
            try:
                response = search_handle.execute()
                success = True
            except Exception as e:
                _logger.info("Got a timeout: %s" % e)
                time.sleep(5)
            if success:
                break
        if success == False:
            _logger.warning("Was unable to make connection of ML Library ES after %s attempts" % attempts)
            return None
        else:
            return response


