from ta2c.pyclient import RegistrationHelper


if __name__ == '__main__':
    ta2c_address = "http://localhost"
    ta2s_port = 8080

    # Register with coordinator
    RegistrationHelper(ta2c_address, ta2s_port)

    print("")
