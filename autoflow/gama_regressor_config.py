import numpy as np
from d3m.primitives.data_cleaning.min_max_scaler import SKlearn as SKMinMaxScaler
# Data cleaning
from d3m.primitives.data_cleaning.standard_scaler import SKlearn as SKStandardScaler
# Data Transformation
from d3m.primitives.data_transformation.fast_ica import SKlearn as SKFastICA
from d3m.primitives.data_transformation.nystroem import SKlearn as SKNystroem
from d3m.primitives.data_transformation.one_hot_encoder import SKlearn as SKOneHotEncoder
from d3m.primitives.data_transformation.polynomial_features import SKlearn as SKPolynomialFeatures
from d3m.primitives.data_transformation.rbf_sampler import SKlearn as SKRBFSampler
# Feature Extraction
from d3m.primitives.feature_extraction.feature_agglomeration import SKlearn as SKFeatureAgglomeration
from d3m.primitives.feature_extraction.pca import SKlearn as SKPCA
# Feature Selection
from d3m.primitives.feature_selection.select_percentile import SKlearn as SKSelectPercentile
# Regression
from d3m.primitives.regression.ada_boost import SKlearn as SKAdaBoost
from d3m.primitives.regression.ard import SKlearn as SKARD
from d3m.primitives.regression.bagging import SKlearn as SKBagging
from d3m.primitives.regression.decision_tree import SKlearn as SKDecisionTreeRegressor
from d3m.primitives.regression.elastic_net import SKlearn as SKElasticNet
from d3m.primitives.regression.extra_trees import SKlearn as SKExtraTreesRegressor
from d3m.primitives.regression.gaussian_process import SKlearn as SKGaussianProcess
from d3m.primitives.regression.gradient_boosting import SKlearn as SKGradientBoostingRegressor
from d3m.primitives.regression.k_neighbors import SKlearn as SKKNeighborsRegressor
from d3m.primitives.regression.kernel_ridge import SKlearn as SKKernelRidge
from d3m.primitives.regression.lars import SKlearn as SKLars
from d3m.primitives.regression.lasso import SKlearn as SKLasso
from d3m.primitives.regression.lasso_cv import SKlearn as SKLassoCV
from d3m.primitives.regression.linear import SKlearn as SKLinear
from d3m.primitives.regression.linear_svr import SKlearn as SKLinearSVR
from d3m.primitives.regression.mlp import SKlearn as SKMLP
from d3m.primitives.regression.passive_aggressive import SKlearn as SKPassiveAggressive
from d3m.primitives.regression.random_forest import SKlearn as SKRandomForestRegressor
from d3m.primitives.regression.ridge import SKlearn as SKRidge
from d3m.primitives.regression.sgd import SKlearn as SKSGD
from d3m.primitives.regression.svr import SKlearn as SKSVR

regressor_config_dict = {

    SKAdaBoost: {
        'add_index_columns': [True],
        'use_semantic_types': [True],
        'n_estimators': [100],
        'learning_rate': [1e-3, 1e-2, 1e-1, 0.5, 1.],
        'loss': ["linear", "square", "exponential"],
        'max_depth': range(1, 11)
    },

    SKARD: {
        'add_index_columns': [True],
        'use_semantic_types': [True]
    },

    SKBagging: {
        'add_index_columns': [True],
        'use_semantic_types': [True]
    },

    SKDecisionTreeRegressor: {
        'add_index_columns': [True],
        'use_semantic_types': [True],
        'max_depth': range(1, 11),
        'min_samples_split': range(2, 21),
        'min_samples_leaf': range(1, 21)
    },

    SKElasticNet: {
        'add_index_columns': [True],
        'use_semantic_types': [True]
    },

    SKExtraTreesRegressor: {
        'add_index_columns': [True],
        'use_semantic_types': [True],
        'n_estimators': [100],
        'max_features': np.arange(0.05, 1.01, 0.05),
        'min_samples_split': range(2, 21),
        'min_samples_leaf': range(1, 21),
        'bootstrap': [True, False]
    },

    SKGaussianProcess: {
        'add_index_columns': [True],
        'use_semantic_types': [True]
    },
    
    SKGradientBoostingRegressor: {
        'add_index_columns': [True],
        'use_semantic_types': [True],
        'n_estimators': [100],
        'loss': ["ls", "lad", "huber", "quantile"],
        'learning_rate': [1e-3, 1e-2, 1e-1, 0.5, 1.],
        'max_depth': range(1, 11),
        'min_samples_split': range(2, 21),
        'min_samples_leaf': range(1, 21),
        'subsample': np.arange(0.05, 1.01, 0.05),
        'max_features': np.arange(0.05, 1.01, 0.05),
        'alpha': [0.75, 0.8, 0.85, 0.9, 0.95, 0.99]
    },

    SKKNeighborsRegressor: {
        'add_index_columns': [True],
        'use_semantic_types': [True],
        'n_neighbors': range(1, 101),
        'weights': ["uniform", "distance"],
        'p': [1, 2]
    },

    SKKernelRidge: {
        'add_index_columns': [True],
        'use_semantic_types': [True]
    },

    SKLars: {
        'add_index_columns': [True],
        'use_semantic_types': [True]
    },

    SKLasso: {
        'add_index_columns': [True],
        'use_semantic_types': [True]
    },

    SKLassoCV: {
        'add_index_columns': [True],
        'use_semantic_types': [True]
    },

    SKLinear: {
        'add_index_columns': [True],
        'use_semantic_types': [True]
    },

    SKLinearSVR: {
        'add_index_columns': [True],
        'use_semantic_types': [True],
        'loss': ["epsilon_insensitive", "squared_epsilon_insensitive"],
        'dual': [True, False],
        'tol': [1e-5, 1e-4, 1e-3, 1e-2, 1e-1],
        'C': [1e-4, 1e-3, 1e-2, 1e-1, 0.5, 1., 5., 10., 15., 20., 25.],
        'epsilon': [1e-4, 1e-3, 1e-2, 1e-1, 1.]
    },

    SKMLP: {
        'add_index_columns': [True],
        'use_semantic_types': [True]
    },

    SKPassiveAggressive: {
        'add_index_columns': [True],
        'use_semantic_types': [True]
    },

    SKRandomForestRegressor: {
        'add_index_columns': [True],
        'use_semantic_types': [True],
        'n_estimators': [100],
        'max_features': np.arange(0.05, 1.01, 0.05),
        'min_samples_split': range(2, 21),
        'min_samples_leaf': range(1, 21),
        'bootstrap': [True, False]
    },

    SKRidge: {
        'add_index_columns': [True],
        'use_semantic_types': [True]
    },

    SKSGD: {
        'add_index_columns': [True],
        'use_semantic_types': [True]
    },

    SKSVR: {
        'add_index_columns': [True],
        'use_semantic_types': [True]
    },


   SKFastICA: {
        'tol': np.arange(0.0, 1.01, 0.05)
    },

    SKFeatureAgglomeration: {
        'linkage': ['ward', 'complete', 'average'],
        'affinity': ['euclidean', 'l1', 'l2', 'manhattan', 'cosine', 'precomputed']
    },

    SKMinMaxScaler: {
    },

    SKNystroem: {
        'kernel': ['rbf', 'cosine', 'chi2', 'laplacian', 'polynomial', 'poly', 'linear', 'additive_chi2', 'sigmoid'],
        'gamma': np.arange(0.0, 1.01, 0.05),
        'n_components': range(1, 11)
    },


    SKPCA: {
        'svd_solver': ['randomized'],
        'iterated_power': range(1, 11)
    },

    SKPolynomialFeatures: {
        'degree': [2],
        'include_bias': [False],
        'interaction_only': [False]
    },

    SKRBFSampler: {
        'gamma': np.arange(0.0, 1.01, 0.05)
    },

    SKStandardScaler: {
    },

    #ZeroCount: {
    #},

    SKOneHotEncoder: {
        'minimum_fraction': [0.05, 0.1, 0.15, 0.2, 0.25],
        'sparse': [False],
        'handle_unknown': ['ignore']
    },

    SKSelectPercentile: {
        'percentile': range(1, 100),
        'score_func': ['f_regression']
    }
}
