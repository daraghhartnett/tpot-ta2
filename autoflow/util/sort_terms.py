
import csv
import os

# Read the Term Labels file
path_to_jminer_output = "/Users/daraghhartnett/Projects/JMiner/d3m/data/output_config_06"
x_labels = "%s/TERM_BY_ADJ_COCL_OLD_FORMAT/xLabels.tsv" % path_to_jminer_output
# Read the Term Labels to frequency counts file
x_margins = "%s/TERM_BY_ADJ_COCL_OLD_FORMAT/xMargs.tsv" % path_to_jminer_output
# Read the MWU's to their PMI
mwus_to_score = "%s/mwus_to_score.tsv" % path_to_jminer_output

labels = list()
frequencies = list()
mwu_to_score_dict = dict()

# Read both files and build up a dictionary of terms to their frequency
with open(x_labels, 'r') as x_labels_file,  \
     open(x_margins, 'r') as x_margins_file, \
     open(mwus_to_score, 'r') as mwus_to_score_file:

    x_labels_reader = x_labels_file.readlines()
    x_margins_reader = csv.reader(x_margins_file)
    mwus_to_score_reader = csv.reader(mwus_to_score_file)

    for label in x_labels_reader:
        labels.append(label.strip())

    for frequency in x_margins_reader:
        frequencies.append(int(frequency[0]))

    for line in mwus_to_score_reader:
        parts = line[0].split('\t')
        mwu_to_score_dict[parts[0]] = float(parts[1])

# Get the label to frequency dictionary
term_to_frequency = dict()
for index in range(len(labels)):
    term_to_frequency[labels[index]] = frequencies[index]

# Sort the dictionary of terms by frequency
terms_sorted_by_freq = sorted(term_to_frequency.items(), key=lambda x: x[1], reverse=True)

# Sort the dictionary of mwu's by score
mwus_sorted_by_freq = sorted(mwu_to_score_dict.items(), key=lambda x: x[1], reverse=True)

with open("%s/mwus_to_score_and_freq.csv" % path_to_jminer_output, "w") as mwus_sorted_by_score_freq_file:
    for index in range(len(mwus_sorted_by_freq)):
        term = mwus_sorted_by_freq[index][0]
        score = mwus_sorted_by_freq[index][1]
        freq = 0
        if term in term_to_frequency:
            freq = term_to_frequency[term]
        mwus_sorted_by_score_freq_file.write("%s\t%s\t%s\n" % (term, score, freq))






