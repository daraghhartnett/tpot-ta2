import unittest
import math
import os
import sys
sys.path.append(os.path.dirname("."))

from term_expansion import TermExpansion


class TestTermExpansion(unittest.TestCase):

    def test_can_load_the_minimum_data_needed_for_term_expansion_x_labels(self):
        input_dir = "./coclustering_data/TERM_BY_DOC_COCL_OLD_FORMAT/"
        term_expansion = TermExpansion(input_directory=input_dir)

        term_expansion.read_term_expansion_data()

        term_indices = term_expansion.get_x_labels()

        expected_number_of_terms = 1735
        expected_first_term_index_0 = 'it'
        expected_random_term_index_1018 = 'victory'
        expected_last_term_index_1734 = 'hoover'

        self.assertEqual(expected_number_of_terms, len(term_indices))
        self.assertEqual(expected_first_term_index_0, term_indices[0])
        self.assertEqual(expected_random_term_index_1018, term_indices[1018])
        self.assertEqual(expected_last_term_index_1734, term_indices[1734])


    def test_can_load_the_minimum_data_needed_for_term_expansion_x_labels_xx_divergence(self):
        input_dir = "./coclustering_data/TERM_BY_DOC_COCL_OLD_FORMAT/"
        term_expansion = TermExpansion(input_directory=input_dir)

        term_expansion.read_term_expansion_data()
        xx_divergences = term_expansion.get_xx_divergence()

        # I was not sure how to calculate this so I just trusted the structure was returning the correct size.
        expected_number_of_entries = 1504725
        expected_distance_0_1 = 1.4299745095447447
        expected_distance_123_1076 = 1.5550049923600149
        expected_distance_1730_1734 = 4.0

        # Note: if you look at the getters that serve up the distances, they order the term indices from smallest to
        # largest since the distances are symmetrical and only need to be stored in 1 direction.
        # Also, the distance from a term to itself is 0 so that is also not stored and is programatically presented.

        self.assertEqual(expected_number_of_entries, len(xx_divergences))
        self.assertEqual(expected_distance_0_1, xx_divergences[(0, 1)])
        self.assertEqual(expected_distance_123_1076, xx_divergences[(123, 1076)])
        self.assertEqual(expected_distance_1730_1734, xx_divergences[(1730, 1734)])


    def test_can_load_the_minimum_data_needed_for_term_expansion_x_labels_xx_divergence_adj(self):
        input_dir = "./coclustering_data/TERM_BY_DOC_COCL_OLD_FORMAT/"
        term_expansion = TermExpansion(input_directory=input_dir)

        term_expansion.read_term_expansion_data()
        xx_divergences = term_expansion.get_xx_divergence_adj()

        # I was not sure how to calculate this so I just trusted the structure was returning the correct size.
        expected_number_of_entries = 1504836
        expected_distance_0_1 = 1.4916520803546267
        expected_distance_123_1076 = 0.7311349605366741
        expected_distance_1730_1734 = 1.9386443082499185

        # Note: if you look at the getters that serve up the distances, they order the term indices from smallest to
        # largest since the distances are symmetrical and only need to be stored in 1 direction.
        # Also, the distance from a term to itself is 0 so that is also not stored and is programatically presented.

        self.assertEqual(expected_number_of_entries, len(xx_divergences))
        self.assertEqual(expected_distance_0_1, xx_divergences[(0, 1)])
        self.assertEqual(expected_distance_123_1076, xx_divergences[(123, 1076)])
        self.assertEqual(expected_distance_1730_1734, xx_divergences[(1730, 1734)])


    def test_can_get_term_expansion_to_return_expected_results(self):
        input_dir = "./coclustering_data/TERM_BY_DOC_COCL_OLD_FORMAT/"
        term_expansion = TermExpansion(input_directory=input_dir)
        term_expansion.read_term_expansion_data()

        # This term:
        #   has term index 519 (on line 520 of xLabels.tsv)
        #   is in term cluster 43 which has 41 members
        term = "explosive device"

        # For the purposes of the test we will get the distance against all other terms
        closest_terms = term_expansion.get_similar_terms(term=term, limit=1735)

        # ensure we get the correct number - that would be the total, 1735 terms - 1 for the term we used as the query
        self.assertEqual(1734, len(closest_terms))

        # ensure the divergences are ranked from closest to furthest
        previous_divergence = 0.0
        count = 0
        for term in closest_terms:
            if count < 10:
                print(f"term: {term[0]}, divergence: {term[1]}")
                count += 1
            divergence = term[1]
            self.assertTrue(divergence >= previous_divergence)
            previous_divergence = divergence

        # check a few of the terms to make sure they are present in the siblings returned.
        # xLabels Index:    520          521          555        607      761
        expected_terms = ["planted", "exploded", "motorcycle", "found", "himself"]
        for term in expected_terms:
            self.assertTrue(len([item for item in closest_terms if item[0] == term]) > 0)


    def test_can_get_term_expansion_from_adj_to_return_expected_results(self):
        input_dir = "./coclustering_data/TERM_BY_DOC_COCL_OLD_FORMAT/"
        term_expansion = TermExpansion(input_directory=input_dir)
        term_expansion.read_term_expansion_data()

        # This term:
        #   has term index 519 (on line 520 of xLabels.tsv)
        #   is in term cluster 43 which has 41 members
        term = "explosive device"

        # For the purposes of the test we will get the distance against all other terms
        closest_terms = term_expansion.get_similar_terms(term=term, limit=1735, adjacency=True)

        # ensure we get the correct number - that would be the total, 1735 terms - 1 for the term we used as the query
        self.assertEqual(1734, len(closest_terms))

        # ensure the divergences are ranked from closest to furthest
        previous_divergence = 0.0
        count = 0
        for term in closest_terms:
            if count < 10:
                print(f"term: {term[0]}, divergence: {term[1]}")
                count += 1
            divergence = term[1]
            self.assertTrue(divergence >= previous_divergence)
            previous_divergence = divergence

        # check a few of the terms to make sure they are present in the siblings returned.
        # xLabels Index:    520          521          555        607      761
        expected_terms = ["planted", "exploded", "thrown", "escorts", "car"]
        for term in expected_terms:
            self.assertTrue(len([item for item in closest_terms if item[0] == term]) > 0)

