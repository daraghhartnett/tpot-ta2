import plac
import sys
import os
sys.path.append(os.path.dirname("."))
from coclustering import Coclustering

class TermExpansion(Coclustering):

    def __init__(self, **args):
        """
        Constructor passes the provided parameters to the _add_attrs method for addition to the instance

        Parameters
        ----------
        **args : list of strings
            list of parameters provided to the main or constructor arguments
            'input_directory' : path to the by-document co-clustering files where artifacts such as xLabels.tsv,
            xxAssoc.tsv and xxAssocAdj.tsv live
        """
        super().__init__(**args)
        # This is a workaround to prevent a logic gate from stopping the adjacency flag from being used. It was
        # originally intended to gracefully disable features when needed data was not available.
        # TODO: Clean up the logic so this is not necessary.
        self.adj_input_directory=self.input_directory


    def read_term_expansion_data(self):
        """
        Calls a read function for the co-occurrence data necessary to support term expansion. Note that this is a much
        reduced set of data compared to all that is available (see the read methods in the Co-clustering base class
        for details). Since we are working with a minimal set of data - some available methods in the underlying class
        will not work.
        """
        # - self.term_labels is dictionary that maps from a term_index to its term label. (x_labels)
        # For example self.term_labels[0] = 'dog'
        self.term_labels = self.read_x_labels()

        # - self.term_to_term_divergence is a dictionary from a tuple of term indices to their relative divergence, or
        # distance, from each other.
        self.term_to_term_divergence = self.read_xx_assoc(file_path="%s/xxAssoc.tsv" % self.input_directory)

        # Read the by adjacency co-clustering association output data.
        # - self.term_to_term_adj_divergence is a dictionary from a tuple of term indices to their relative
        # divergence, or distance, from each other.
        self.term_to_term_adj_divergence = self.read_xx_assoc(file_path="%s/xxAssocAdj.tsv" % self.input_directory)


    def get_similar_terms(self, term, limit=20, adjacency=False):
        """
        Get tuples of term_indices to divergence with the specified term, sorted by closest first. Note this will
        compare the specified term to *all* terms, not only those that are in the same term_cluster.

        Parameters
        ----------
        term : string
            the term that we want to get the closest terms for
        limit : int
            the number of terms to return. Default is 20
        adjacency : boolean
            if true, use the by-adjacency co-clustering output, false, use the by-document co-clustering output

        Returns
        -------
        list of tuples of terms to divergence with the specified term, sorted by closest first. Term count returned is limited
        by the 'limit' parameter.
        """
        term_index = self.get_term_index_from_term(term=term)
        similar_term_indices = self.get_closest_terms_sorted_by_divergence(term_index, limit, adjacency)
        similar_terms = []
        for term in similar_term_indices:
            # Calling code is not going to want to know about term indices and the like so hide that stuff here.
            term_string = self.get_x_labels()[term[0]]
            similar_terms.append((term_string, term[1]))
        return similar_terms


if __name__ == "__main__":
    """
    Entry point - required to make python happy
    """

    def main(input_directory: "location of the by document co-clustering data"):
        """
        Main entry point to the Term Expansion class

        Parameters
        ----------
        input_directory : string
            location of the co_clustering data
        """
        TermExpansion(input_directory=input_directory).main()

    plac.call(main)