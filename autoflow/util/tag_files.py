
import csv
import os

# Read the filename to target data
truth_file = "../../data/inputs/JIDO_SOHR_Articles_1061/TRAIN/dataset_TRAIN/tables/learningData.csv"

with open(truth_file) as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    first = True
    for row in csv_reader:
        # skip header
        if first:
            first = False
            continue
        filename = row[2]
        of_interest = row[1]
        # Iterate over the files and change their names to match their target
        current_file_name = "../../data/inputs/JIDO_SOHR_Articles_1061/TRAIN/dataset_TRAIN/media/%s" % filename
        file_addition = "ignore"
        if of_interest == "1":
            file_addition = "of_interest"
        new_file_name = current_file_name.replace(".txt", "_%s.txt" % file_addition)
        try:
            os.rename(current_file_name, new_file_name)
        except Exception as e:
            # No big deal - this just means the file name has already been changed by a prior run
            pass






