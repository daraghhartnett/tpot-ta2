import numpy

def softmax(values):
    """
    Compute softmax values for each list of candidate pipeline scores
    """
    e_x = numpy.exp(values - numpy.max(values))
    return e_x / e_x.sum(axis=0)