import copy
import logging
import math
import os
import os.path
import pickle
import sys
import shutil
import threading
import time
import traceback
from datetime import datetime
from threading import Lock
import concurrent.futures

import d3m
import numpy.random
import pandas as pd
from d3m.base import utils as base_utils
from d3m.container import Dataset, DataFrame
from autoflow.config.d3m_grid import grid
import random
import copy

from datetime import datetime, timezone
from autoflow.util import comparitors

# from d3m.container.dataset import D3MDatasetLoader
# from common_primitives.datamart_augment import Hyperparams as hyper_augment, DataMartAugmentPrimitive
# from common_primitives.datamart_download import Hyperparams as hyper_download, DataMartDownloadPrimitive
#
# from d3m.container.dataset import Dataset, D3MDatasetLoader
# from common_primitives.denormalize import Hyperparams as hyper_denormalize, DenormalizePrimitive
# import os
# from dsbox.datapreprocessing.cleaner.wikifier import WikifierHyperparams, Wikifier

from d3m.metadata import base as Base
from d3m.metadata.pipeline import PlaceholderStep
from d3m.runtime import Runtime

#TODO: Uncomment this and add the gama code base as a peer to 'autoflow' to get your local version of GAMA overlayed
#      on a Docker image
# CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
# sys.path.append(os.path.dirname(CURRENT_DIR))
# sys.path.append(os.path.abspath('../gama'))
# sys.path.append(os.path.abspath('../sri_tpot'))

from gama import GamaClassifier, GamaRegressor, GamaTimeSeriesForecaster, GamaTimeSeriesClassifier
from google.protobuf.timestamp_pb2 import Timestamp
from pathos.multiprocessing import ProcessPool

if os.getenv('TPOT') is not None:
    from sri_tpot import TPOTClassifier, TPOTRegressor

from sri.d3mglue.d3mwrap import D3MWrapper, TRANSFORMER_FAMILIES

from .config_converter import config_to_d3m
from .gama_classifier_config import classifier_config_dict
from .gama_regressor_config import regressor_config_dict
# There are two possible configurations for TSF, one that mixes in standard regressors, and one that
# uses only time series forecasting algorithms (pure).
import os
# When we are running outside of docker dont try to import problematic (difficult to install) primitives
if os.getenv('AUTOFLOW_WITH_NO_DOCKER') is None:
    from .gama_tsf_config import tsf_config_dict, tsf_config_dict_pure
else:
    from .gama_tsf_reduced_config import tsf_config_dict, tsf_config_dict_pure
from .nbest import NBest
from .pipeline import GraphPipeline, GamaPipeline, GamaEnsemblePipeline
from pipelines import find_pipeline_class
from pipelines.base import KEY_TYPES, DATETIME_TYPE, TARGET_TYPE, TRUE_TARGET_TYPE
from itertools import repeat

#logging.basicConfig(level=logging.DEBUG, format='%(asctime)s [%(levelname)s] %(name)s -- %(message)s')

_logger = logging.getLogger(__name__)

gamalog = logging.getLogger('gama')
gamalog.setLevel(logging.INFO)


class UnfitException(Exception):
    pass


class AutoflowOptimizer(object):
    """
    Abstract base class for several optimization sub-problems, each of
    which is addressed separately through subclassing.
    """
    mutex = Lock()

    # List to manage active solution ids
    solution_ids = []

    # Keep track of which id is at which index
    solution_ids_to_index = {}
    solution_ids_to_predictions = {}

    # Initialize time
    _start_time = datetime.now()
    _logger.info("Start time has been set as initially: %s" %  _start_time)

    # Flag to ensure we dont generate these every single time a pipeline is exported
    considered_pipelines_generated = False

    @staticmethod
    def enable_pipeline_caching(enable):
        D3MWrapper.enable_cache(enable)

    def __init__(self, config=None):
        """
        We'll assume for the moment that constructing an optimizer does
        all the heavy lifting of reading the config file and
        remembering all the important info it contains.  We probably will
        want a class method to return the appropriate subclass based
        on the config contents, so top-level autoflow doesn't need to hold
        that logic.
        """
        self.config = config
        _logger.info("Dataset Schema to be loaded is %s" % config.dataset_schema)
        if "http" not in config.dataset_schema:
            uri = "file://%s" % os.path.abspath(config.dataset_schema)
        else:
            # Open ML datasets are already fully formed.
            uri = config.dataset_schema
        _logger.info("Loading %s" % uri)
        self.dataset = Dataset.load(uri)
        _logger.info("Dataset loaded")

        if self.config.enable_pipeline_caching():
            _logger.info("Pipeline caching enabled")
            self.enable_pipeline_caching(True)


    def _minutes_remaining(self):
        """
        Used to track how long we have been searching so we can stay beneath our allocated budget
        """
        minutes = self.config.timeout - ((datetime.now() - self._start_time).total_seconds() / 60)
        _logger.info("Minutes Remaining: %s" % minutes)
        return minutes


    def _initialize_start_time(self):
        _start_time = datetime.now()
        _logger.info("Initializing start time for ta3 mode %s" % _start_time)


    def get_pipeline_count(self):
        """
        Each optimizer has a different approach so it is asked how many solutions it plans to return. CROptimizer
        generally does 20 but the GraphOptimizer can only generate 1 solution
        """
        raise NotImplementedError()


    def adjust_target_column(self, dataset=None):
        # The learning data is the resource that needs to be altered for the target change, if necessary
        resource_id = 'learningData'
        # Check to see if the target specified by the ta3 matches up with the suggestedTarget in the dataset
        specified_target = self.config.target_name
        specified_target_index = -1
        suggested_target = -1
        suggested_target_index = -1
        # Iterate over the features in the dataset
        for index, feature in enumerate(dataset['learningData'].columns):
            # Keep track of the column index of the target specified by ta3
            if specified_target in feature:
                specified_target_index = index
            # When the suggested target is found, keep its column index and name
            if self.dataset.metadata.has_semantic_type((resource_id, Base.ALL_ELEMENTS, index),
                                                       'https://metadata.datadrivendiscovery.org/types/SuggestedTarget'):
                suggested_target_index = index
                suggested_target = feature
        # If the suggested target and the specified target match, no adjustments are necessary
        if specified_target in suggested_target:
            _logger.info("Suggested target and specified target match")
        # Otherwise we need to adjust the semantic types of the two features so the correct feature is set as the
        # target for training
        else:
            _logger.info("Suggested Target %s is being overriden by TA3 as %s" % (suggested_target, specified_target))

            # Remove Suggested Target attribute from Dataset
            dataset.metadata = dataset.metadata.remove_semantic_type(
                (resource_id, Base.ALL_ELEMENTS, suggested_target_index),
                'https://metadata.datadrivendiscovery.org/types/SuggestedTarget')

            dataset.metadata = dataset.metadata.add_semantic_type(
                (resource_id, Base.ALL_ELEMENTS, suggested_target_index),
                'https://metadata.datadrivendiscovery.org/types/Attributes')

            # Add Suggested Target to At Bat
            dataset.metadata = dataset.metadata.remove_semantic_type(
                (resource_id, Base.ALL_ELEMENTS, specified_target_index),
                'https://metadata.datadrivendiscovery.org/types/Attributes')

            dataset.metadata = dataset.metadata.add_semantic_type(
                (resource_id, Base.ALL_ELEMENTS, specified_target_index),
                'https://metadata.datadrivendiscovery.org/types/SuggestedTarget')
        return dataset


    def remove_attribute_from_target(self, dataset=None):
        suggested_target_type = 'https://metadata.datadrivendiscovery.org/types/SuggestedTarget'
        true_target_type = 'https://metadata.datadrivendiscovery.org/types/TrueTarget'
        attribute_type = 'https://metadata.datadrivendiscovery.org/types/Attribute'
        if dataset is None:
            dataset = self.dataset

        resource_id, resource = base_utils.get_tabular_resource(dataset, None)
        for index, feature in enumerate(dataset[resource_id].columns):
            selector = (resource_id, Base.ALL_ELEMENTS, index)
            if dataset.metadata.has_semantic_type(selector, suggested_target_type):
                dataset.metadata = dataset.metadata.remove_semantic_type(selector, attribute_type)
            if dataset.metadata.has_semantic_type(selector, true_target_type):
                dataset.metadata = dataset.metadata.remove_semantic_type(selector, attribute_type)

        return dataset


    def fit(self):
        """
        Optimize against the training data.
        """
        raise NotImplementedError()


    def pipelines(self, index=None):
        """
        Return an iterator over the best pipelines found, which are
        AutoflowPipeline objects.  Raise an exception if fit has not
        been run yet.
        """
        raise NotImplementedError()


    def running_pipelines(self):
        raise NotImplementedError()


    def considered_pipelines(self):
        """
        Return an iterator over the best pipelines found, which are
        AutoflowPipeline objects.  Raise an exception if fit has not
        been run yet.
        """
        raise NotImplementedError()


    def use_dataset(self, data_uri):
        self.dataset = Dataset.load(data_uri)


    '''
    This is to support the DescribeSolution call for TA3
    '''
    def get_pipeline(self, solution_id):
        raise NotImplementedError()


    def checkpoint(self, break_after_first=False, index=None):
        _logger.info("Checkpointing pipelines... Index: %s" % index)
        count = 0
        for pipeline in self.pipelines(index=index):
            if pipeline is None:
                continue
            pipeline.log()
            if break_after_first:
                return pipeline.name
            count += 1
        _logger.info("Checkpointed %s pipelines" % count)

        # Only perform this step once
        if not self.considered_pipelines_generated:
            self.considered_pipelines_generated = True
            _logger.info("Checkpointing considered pipelines...")
            considered_count = 0
            for pipeline in self.considered_pipelines():
                pipeline.log(True)
                considered_count += 1
                # Only limit the number of considered pipelines we export when testing locally - this is something
                # that DARPA tracks.
                # if considered_count >= 25:
                #     break
            _logger.info("Checkpointed %s considered pipelines" % considered_count)

        return count


    def checkpoint_state(self):
        pid = os.getpid()
        troot = self.config.temp
        state = self.get_checkpoint_state()
        state['pid'] = pid
        path = "%s/%d" % (troot, pid)
        with open(path, "wb") as fh:
            pickle.dump(state, fh)


    def get_checkpoint_state(self):
        """
        Basically, a no-op state.  Ideally, subclasses will provide a version that supports warn restarts.
        """
        return {}

    def get_state(self):
        """
        Return a hash containing all relevant aspects of state to restore about restart.
        """
        raise NotImplementedError()

    def set_state(self, stateobj):
        """
        Set internal state based on previously saved state object, a hash.
        """
        raise NotImplementedError()


    def reload_state(self, statefile):
        with open(statefile, "rb") as fh:
            stateobj = pickle.load(fh)
        self.set_state(stateobj)


def threaded(fn):
    def wrapper(*args, **kwargs):
        thread = threading.Thread(target=fn, args=args, kwargs=kwargs)
        thread.start()
        return thread
    return wrapper


class GraphOptimizer(AutoflowOptimizer):

    pipeline_count = 1
    pipeline_complete = False

    def __init__(self, **kw):
        super().__init__(**kw)
        self.pipeline = self.pipeline_class.generate(self.dataset)
#        self.pipeline = self.pipeline_class.get_instance()


    def get_pipeline_count(self):
        return self.pipeline_count


    @threaded
    def spinoff_fit(self, search_id, solution_ids, ta3_preamble):
        """
        Threaded entrypoint for the TA3TA2api to initiate a fit call on a separate thread.
        """
        # Adjust the output paths to use the search_id as the directory between the dataset and the
        # pipeline files
        self.config.update_paths(search_id)

        _logger.info("GraphOptimizer spinoff_fit")
        self.solution_ids = solution_ids

        for i in range(0, 1):
            self.solution_ids_to_index[solution_ids[i]] = i

        self.fit()


    def fit(self):
        """
        Fit for the Graph Problems
        :return:
        """
        _logger.info("GraphOptimizer fit")
        self.fitted_pipeline = Runtime(self.pipeline, context=Base.Context.TESTING, volumes_dir=self.config.static_volumes)
        dataset = self.remove_attribute_from_target()
        self.fitted_pipeline.fit(inputs=[dataset])
        self.pipeline_obj = GraphPipeline(optimizer=self,
                                          pipeline=self.pipeline,
                                          fitted_pipeline=self.fitted_pipeline,
                                          rank=1)
        _logger.info("Created pipeline %s (%d)" %
                     (self.pipeline_obj.name, self.pipeline_obj.rank))
        self.pipeline_complete = True


    def pipelines(self, index=None):
        if self.pipeline is None:
            raise UnfitException()
        yield self.pipeline_obj


    def get_pipeline(self, solution_id):
        if self.pipeline is None:
            _logger.info("Pipeline is not yet ready to describe for this solution id: %s" % solution_id)
        return self.pipeline_obj


    def considered_pipelines(self):
        for p in self.pipelines():
            yield p


    def running_pipelines(self):
        """
        Depending on the state of the pipelines this method will return a "running" pipeline
        structure or a completed pipeline structure to the TA3TA2-api layer for communication
        to the TA3 client
        """
        timestamp = Timestamp()
        start = timestamp
        end = timestamp
        start.GetCurrentTime()
        end.GetCurrentTime()

        state = "running"
        time_left = self._minutes_remaining()
        if time_left < 2:
            state = "completed"
            _logger.info("%s minutes left, tell TA3 so it can harvest the pipelines..." % time_left)
        else:
            _logger.info("%s minutes left, keep improving..." % time_left)

        while len(self.solution_ids) == 0:
            # Sometimes the TA3 is a little hasty asking for running pipelines before we can complete the id
            # initialization. This could be due to the time it takes to establish a JHU pipeline. Sleep for 1
            # second to allow the initialization to complete
            _logger.info("Sleeping while initialization completes")
            time.sleep(1)


        if not self.pipeline_complete:
            _logger.info("No valid pipelines have been returned yet")
            for index in range(0, self.pipeline_count):
                pipeline_state = dict(
                    state="running",
                    status="running",
                    start=start,
                    end=end,
                    done_ticks=0,
                    all_ticks=0,
                    solution_id=self.solution_ids[index],
                    score=0,
                    rank=index + 1
                )
                yield pipeline_state
        else:
            self.checkpoint()
            _logger.info("Candidate pipelines present...")
            for index in range(0, self.pipeline_count):
                pipeline_score = 0.1
                pipeline_state = dict(
                    state=state,
                    status="successful",
                    start=start,
                    end=end,
                    done_ticks=0,
                    all_ticks=0,
                    solution_id=self.solution_ids[index],
                    score=pipeline_score,
                    rank=index + 1
                )
                yield pipeline_state


    def completed_pipeline(self, solution_id):
        """
        Once processing is complete return the score results to the TA3TA2-api layer
        """
        solution_index = self.solution_ids_to_index[solution_id]

        timestamp = Timestamp()
        start = timestamp
        end = timestamp
        start.GetCurrentTime()
        end.GetCurrentTime()
        pipeline_state = None


        if not self.pipeline_complete:
            _logger.info("No valid pipelines have been returned yet")

            pipeline_state = dict(
                state="running",
                status="running",
                start=start,
                end=end,
                metric=self.config.metric,
                score=0.0,
                rank=1
            )
        else:
            _logger.info("Candidate pipelines present...")

            # TODO: use the actual internal pipeline score, using the reference runtime can take ages
            pipeline_score = 0.1

            pipeline_state = dict(
                state="completed",
                status="successful",
                start=start,
                end=end,
                metric=self.config.metric,
                score=pipeline_score,
                rank=solution_index
            )

        return pipeline_state


    def spinoff_export(self, fitted_solution_id=None, rank=None):
        _logger.info("Exporting pipeline for fitted_solution_id: %s and rank: %s" % (fitted_solution_id, rank))

        # No-op Currently already done for Graphs.

        _logger.info("Exported pipelines for problem: %s", self.config.problem_id)

    def end_search(self, search_id):
        _logger.info("Removing search associated with search_id: %s" % search_id)
        # TODO: Purge the solution ids


    def unpickle_pipeline(self, pipeline_name):
        return GraphPipeline(optimizer=self, pipeline_name=pipeline_name)


    def get_state(self):
        """ We currently don't support restarting long-running optimizations, no this is a no-op """
        return {}


    def set_state(self, stateobj):
        """ We currently don't support restarting long-running optimizations, no this is a no-op """
        pass


class VertexClassificationOptimizer(GraphOptimizer):
    pipeline_class = find_pipeline_class(label="vertex_classification")

class VertexClassificationJHUOptimizer(GraphOptimizer):
    pipeline_class = find_pipeline_class(label="vertex_classification_jhu")

class GraphMatchingJHUOptimizer(GraphOptimizer):
    pipeline_class = find_pipeline_class(label="graph_matching_jhu")

class LinkPredictionJHUOptimizer(GraphOptimizer):
    pipeline_class = find_pipeline_class(label="link_prediction_jhu")

class CommunityDetectionJHUOptimizer(GraphOptimizer):
    pipeline_class = find_pipeline_class(label="community_detection_jhu")

class ObjectDetectionOptimizer(GraphOptimizer):
    # Until we have a better way to do object detection
    pipeline_class = find_pipeline_class(label="baseline")


class CROptimizer(AutoflowOptimizer):
    """
    Base class for all classification and regression optimization.
    """

    solution_fitted = False
    # Stick with 20 pipelines - some problems produce bad pipelines so this increases our chances of have at least
    # one the runs (which counts as a win)
    pipeline_count = 20
    optimizer_class = AutoflowOptimizer

    def __init__(self, **args):

        super().__init__(**args)

        # Check for the presence of a Pipeline Count over-ride - this is used by the integration test
        if 'D3M_Pipeline_Count' in os.environ:
            self.pipeline_count = int(os.environ['D3M_Pipeline_Count'])

        # Define a callback for when pipelines are evaluated by TPOT
        def pipeline_eval_callback(pipeline, score, fragment_instance):
            _logger.info("In pipeline call back for fragment instance %s. Score: %s" % (self.fragment_instances[fragment_instance], score))
            if math.isinf(score):
                return
            # Ensure safe access to the nbest structure
            acquire = False
            try:
                acquire = self.mutex.acquire()
                _logger.debug("Trying to insert pipeline with score %f" % score)
                inserted = self.nbest.insert(pipeline, score, fragment_instance)
                if inserted:
                    _logger.info("Fragment: %s.Internal scores for Problem: %s. Inserted pipeline with score %f into nbest. Current size: (%d)" %
                                 (self.fragment_instances[fragment_instance], self.config.problem_id, score, len(self.nbest)))

                self.solution_fitted = True
            finally:
                self.mutex.release()
                if not acquire:
                    _logger.warning("Issue #31a: Potential deadlock avoided - self.mutex.acquire() failed to complete")

        # Save to field, because Gama optimization wants to instantiate a new optimizer with each round of fitting
        self.pipeline_eval_callback = pipeline_eval_callback

        # Define a function to call at the end of each TPOT generation
        def gen_callback():
            # We used to checkpoint pipelines here due to stability concerns but have since removed it due to thread lock issues.
            _logger.info("End of generation gen_callback() invoked from sri_tpot")

        # Save to field, because Gama optimization wants to instantiate a new optimizer with each round of fitting
        self.gen_callback = gen_callback

        optr_args = self.optimizer_arguments = {}
        self.get_optimizer_arguments(pipeline_eval_callback, gen_callback)

        #TODO: 20200805 Purge this if all looks good on CI
        # _logger.info("Creating optimizer: %s(%s)" % (self.optimizer_class.__name__, str(optr_args)))
        # _logger.info("Using %s CPU's", optr_args['n_jobs'])

        # self.pipeline_optimizer = self.optimizer_class(**optr_args)


    def get_pipeline_count(self):
        return self.pipeline_count


    def get_optimizer_arguments(self, pipeline_eval_callback, gen_callback):
        optr_args = self.optimizer_arguments

        optr_args['n_jobs'] = int(self.config.cpus)

        # Set any arguments specified by the config file
        for arg, val in self.config.optimizer_arguments():
            optr_args[arg] = val


    @threaded
    def spinoff_fit(self, search_id, solution_ids, ta3_preamble):
        """
        Threaded entrypoint for the TA3TA2api to initiate a fit call on a separate thread.
        """
        # Adjust the output paths to use the search_id as the directory between the dataset and the pipeline files
        self.config.update_paths(search_id)

        self.solution_ids = solution_ids
        for i in range(0, self.pipeline_count):
            self.solution_ids_to_index[solution_ids[i]] = i

        # Tell TPOT not to bother with generation-level timeout, because the signal-based timeout does not play well
        # with threading.
        #self.pipeline_optimizer.max_generation_time_mins = None

        # size = len(self.search_id_map)
        # _logger.info("Adding search_id: %s to other %s running searches" % (search_id, size))
        # self.search_id_map.add(search_id)
        # TODO: Can we associate this search_id with the thread so we can kill it when requested?
        #self.pipeline_optimizer.max_generation_time_mins = None
        self.fit(ta3_preamble=ta3_preamble)


    def fit(self, ta3_preamble=None):
        """
        Fit for the Classification/Regression Problems
        This step could actually be called search as it is searching for valid pipelines. After valid
        pipelines are discovered, fit_pipeline is called (see the pipelines function below) which then
        fits the data to each of the found pipelines.
        """
        _logger.info("optimizer::fit() called")
        # Ensure safe access to the nbest structure
        _logger.info("\toptimizer::fit() mutex acquired")
        acquire = False
        try:
            acquire = self.mutex.acquire()
            _logger.info("\toptimizer::fit() setting up nbest")
            # Set up pipeline tracking
            self.nbest = NBest(self.pipeline_count)
            self.nbest.set_item_hash(lambda p: self.clean_pipeline_string(p))
            _logger.info("\toptimizer::fit() nbest established")
        finally:
            _logger.info("\toptimizer::fit() mutex release")
            self.mutex.release()
            if not acquire:
                _logger.warning("Issue #31b: Potential deadlock avoided - self.mutex.acquire() failed to complete")

        _logger.info("\toptimizer::fit() calling prep_data")
        dataset = self.prep_data()

        # Provide a different pickle path for each instance of Gama
        _logger.info("Fitting %s" % self.config.backend)
        pickle_paths = []
        fragment_count = len(self.fragment_instances)
        for index in range(fragment_count):
            pickle_paths.append("%s/best_%s.mdl" % (self.config.temp, index))

        _logger.info("Fragment count is %s" % fragment_count)

        # Launch one Gama Search thread for each available preamble fragment
        with concurrent.futures.ThreadPoolExecutor(max_workers=fragment_count) as executor:
            executor.map(self.fit_sample_thread, repeat(dataset), list(pickle_paths), range(fragment_count))


    def fit_sample_thread(self, dataset, pickle_path, instance_index):
        # Check to see if this is a fully formed pipeline, if so, no search is required and we simply write it out
        # to be run directly.
        _logger.info("Checking for partial fragment: %s" % self.preamble_pipelines[instance_index].preamble_fragment)
        if not self.preamble_pipelines[instance_index].preamble_fragment:

            # Uncomment the following section to revert to the non optimized fully formed fragment pipeline generation
            # Since this pipeline was not found by GAMA lets give it a rank outside of the number of pipelines being
            # generated. We may want to use a generated score at some point
            # _logger.info("Setting rank to: %s" % 1)
            # pipeline = self._make_pipeline(rank=1, score=1.0,
            #                                pipeline=self.preamble_pipelines[instance_index], created=datetime.now(timezone.utc),
            #                           fragment_index=instance_index)
            # pipeline.log()
            # return

            # 1) Get n different dev/train slices of the data to average the scores over
            dev_datasets = []
            dataset_split_count = 5
            for i in range(dataset_split_count):
                dev_datasets.append(self.get_random_subset_of_data(dataset, percentage_for_dev=10))

            # 2) Get range of tuning HP values to use to create variations on the candidate pipelines in next step
            primitive_tuning_hp_values = dict()
            for primitive in self.preamble_pipelines[instance_index].primitives_to_tune:
                metadata = primitive.metadata.query()
                tuning_hps = metadata['hyperparams_to_tune']
                primitive_tuning_hp_values[primitive] = dict()
                for hp in tuning_hps:
                    # Get spread of values for the tuning hyper parameter
                    hyperparam = metadata['primitive_code']['class_type_arguments']['Hyperparams'].configuration[hp]
                    grid_values = grid(hyperparam)
                    primitive_tuning_hp_values[primitive][hp] = grid_values

            # 3) Seed the NBest structure which will keep track of the best pipelines with n initial variations
            pipeline_variation_count = 5
            n_best_candidate_pipelines = NBest(pipeline_variation_count)
            n_best_candidate_pipelines.set_item_hash(lambda p: self.clean_pipeline_string(p))
            pipeline = self.preamble_pipelines[instance_index]
            pipeline_variations = self.generate_variations(count=pipeline_variation_count, source=pipeline,
                                                           values=primitive_tuning_hp_values)
            for pipeline in pipeline_variations:
                score = self.score_pipeline(pipeline, dataset, dev_datasets, instance_index)
                n_best_candidate_pipelines.insert(pipeline, score, instance_index)

            # 4) Pipeline optimization loop
            generations = 5
            for generation in range(generations):
                # Get the scores and pipelines from the NBest structure so the softmax can choose what to evolve
                n_scores = []
                n_pipelines = []
                items = n_best_candidate_pipelines.all_items(include_scores=True)
                for item in items:
                    n_scores.append(item[0])
                    n_pipelines.append(item[1])

                # Use softmax function to rank the pipelines by score in a probability distribution
                probabilities = comparitors.softmax(n_scores)

                # Use the softmax probabilities to influence which pipelines are used in the next generation
                seed_candidate_pipelines = numpy.random.choice(n_pipelines, pipeline_variation_count, p=probabilities)

                # For each random candidate, vary the hps randomly
                candidate_pipelines = []
                for seed_pipeline in seed_candidate_pipelines:
                    pipeline_variations = self.generate_variations(count=1, source=seed_pipeline,
                                                                   values=primitive_tuning_hp_values)
                    candidate_pipelines.extend(pipeline_variations)

                # Score each new pipeline variation
                for pipeline in candidate_pipelines:
                    score = self.score_pipeline(pipeline, dataset, dev_datasets, instance_index)
                    # Add to the NBest structure which will maintain the list of the best pipelines
                    n_best_candidate_pipelines.insert(pipeline, score, instance_index)

            # This pipeline was not found by GAMA: give it a rank outside of the number of pipelines being generated.
            rank = str(int(self.pipeline_count) + int(self.full_pipeline_rank[instance_index]))
            _logger.info("Setting rank to: %s" % rank)

            # Write out the best pipeline
            _logger.info("Score is: %s" % str(n_best_candidate_pipelines.all_items(include_scores=True)[0][0]))
            best_pipeline_score = n_best_candidate_pipelines.all_items(include_scores=True)[0][0]
            best_pipeline = n_best_candidate_pipelines.all_items(include_scores=True)[0][1]
            best_pipeline_complete = self._make_pipeline(rank, score=best_pipeline_score, pipeline=best_pipeline,
                                            created=datetime.now(timezone.utc), fragment_index=instance_index)
            best_pipeline_complete.log()
            return

        features, targets = self.get_features_and_targets_from_dataset(dataset, instance_index, skip_augmentation=True)

        # Drop this down to accelerate learning (at the expense of model performance).
        # 1000 is the default, 200 for speed
        _logger.info("fit_sample_thread launched. Shape: %s. Mins Remaining: %s" % (features.shape[0], self._minutes_remaining()))
        sample_size = 1000
        while sample_size < features.shape[0] and self._minutes_remaining() > 2:
            _logger.info("Early fitting with %d examples" % sample_size)
            self.fit_sample(features, targets, self._minutes_remaining(), pickle_path, instance_index, sample_size)
            sample_size *= 10

        minutes_remaining = self._minutes_remaining()
        if minutes_remaining > 5:
            self.fit_sample(features, targets, minutes_remaining, pickle_path, instance_index)
        else:
            _logger.warning("Insufficient time remaining (%s minutes) to fit_sample, GAMA exiting..." % minutes_remaining)

        _logger.info("%s fitting completed" % self.config.backend)


    def score_pipeline(self, source_pipeline, dataset, dev_datasets, fragment_index):
        """
        Given a pipeline, a dataset, and a  set of dataset splits, score the pipeline over all the splits and return
        the average of the scores.
        """
        # Produce an average score for the pipeline over the available dataset splits
        averaged_score = 0.0

        # Create the pipeline and add the construct predictions primitive to it
        pipeline = self._make_pipeline(1, score=1.0, pipeline=source_pipeline,
                                       created=datetime.now(timezone.utc), fragment_index=fragment_index)
        d3m_pipeline = pipeline.as_d3m_pipeline("Autoflow Pipeline", "Pipeline generated AutoFlow ", None)
        fitted_pipeline = Runtime(d3m_pipeline, context=Base.Context.TESTING,
                                  volumes_dir=self.config.static_volumes)

        for dev_dataset in dev_datasets:
            fitted_pipeline.fit(inputs=[dataset])
            # Clear the target column for produce call
            dev_datasets[0]['learningData'][self.config.target_column_name] = ''
            predictions = fitted_pipeline.produce(inputs=[dev_dataset])

            correct = 0
            for entry in predictions.values['outputs.0'].values:
                d3m_index = entry[0]
                prediction = entry[1]
                adjusted_index = int(numpy.where(dataset['learningData']['d3mIndex'].values == d3m_index)[0][0])
                truth = dataset['learningData'][self.config.target_column_name][adjusted_index]
                if prediction == truth:
                    correct += 1
            score = correct / len(predictions.values['outputs.0'].values)
            averaged_score += score
            pipeline.score = score

        # divide by the number of datasets to get the average score
        averaged_score /= len(dev_datasets)
        _logger.info("Average Score for dataset split: %s" % averaged_score)
        return averaged_score


    def generate_variations(self, count, source, values):
        """
        Given a source pipeline, a range of tuning hyper-parameter values and a desired number of variations, make
        copies of the source pipeline, randomly alter some of the hyper-parameter values and return the variations
        as an array.
        """
        variations = []
        for pipeline_index in range(count):
            pipeline_variation = copy.deepcopy(source)
            self.tweak_pipeline(pipeline_variation, values)
            variations.append(pipeline_variation)
        return variations


    def tweak_pipeline(self, pipeline_variation, primitive_tuning_hp_values):
        """
        Given a pipeline and a range of tuning hyper-parameter values, randomly change some of the hyper-parameter
        values.
        """
        for primitive in pipeline_variation.primitives_to_tune:
            for step in pipeline_variation.steps:
                if str(step.primitive) in str(primitive):
                    primitive_instance = step
                    break
            metadata = primitive.metadata.query()
            tuning_hps = metadata['hyperparams_to_tune']
            for tuning_parameter in tuning_hps:
                values = primitive_tuning_hp_values[primitive][tuning_parameter]
                selection = numpy.random.choice(values, 1)[0]
                primitive_instance.hyperparams[tuning_parameter]['data'] = selection


    def get_random_subset_of_data(self, dataset, percentage_for_dev=10):
        """
        Given the provided dataset, create a new dataset that has a random selection of the rows that total the
        specified percentage. This is used to create random development datasets for testing performance of pipelines
        """
        total_size = len(dataset['learningData'])
        dev_row_count = int((total_size / 100) * percentage_for_dev)
        random_indexes = set()
        for index in range(dev_row_count):
            random_index = random.randint(0, total_size - 1)
            while random_index in random_indexes:
                random_index = random.randint(0, total_size - 1)
            random_indexes.add(random_index)
        dev_dataset = dataset.select_rows(row_indices_to_keep={'learningData': random_indexes})
        return dev_dataset


    def fit_optimizer(self, feats, targs, picklePath, instance_index, fragment_pipeline_optimizer):
        """
        Method introduced to accommodate differences in arguments between TPOT.fit and GAMA.fit
        """
        raise NotImplementedError()


    def running_pipelines(self):
        """
        Depending on the state of the pipelines this method will return a "running" pipeline
        structure or a completed pipeline structure to the TA3TA2-api layer for communication
        to the TA3 client
        """
        timestamp = Timestamp()
        start = timestamp
        end = timestamp
        start.GetCurrentTime()
        end.GetCurrentTime()

        state = "running"
        time_left = self._minutes_remaining()
        if time_left < 2:
            state = "completed"
            _logger.info("%s minutes left, tell TA3 so it can harvest the pipelines..." % time_left)
        else:
            _logger.info("%s minutes left, keep improving..." % time_left)

        # Ensure safe access to the nbest structure
        acquire = False
        try:
            acquire = self.mutex.acquire()
            completed_pipeline_count = 0
            pipeline_count = 0
            if hasattr(self, 'nbest'):
                completed_pipeline_count = self.nbest.item_list.__len__()
                pipeline_count = self.nbest.size

            if completed_pipeline_count == 0:
                _logger.info("No valid pipelines have been returned yet")
                for index in range(0, pipeline_count):
                    pipeline_state = dict(
                        state="running",
                        status="running",
                        start=start,
                        end=end,
                        done_ticks=0,
                        all_ticks=0,
                        solution_id=self.solution_ids[index],
                        score=0,
                        rank=index
                    )
                    yield pipeline_state
            else:
                _logger.info("Candidate pipelines present...")
                for index, pipeline in enumerate(self.nbest.item_list):
                    pipeline_score = pipeline[2]
                    if self.optimizer_class == GamaRegressor:
                        _logger.info("Converting pipeline score from %s" % pipeline_score)
                        pipeline_score *= -1
                        _logger.info("to %s" % pipeline_score)
                    pipeline_state = dict(
                        state=state,
                        status="successful",
                        start=start,
                        end=end,
                        done_ticks=0,
                        all_ticks=0,
                        solution_id=self.solution_ids[index],
                        score=pipeline_score,
                        rank=index + 1 # Rank is a 1 based system so there is no rank 0
                    )
                    yield pipeline_state

        finally:
            self.mutex.release()
            if not acquire:
                _logger.warning("Issue #31c: Potential deadlock avoided - self.mutex.acquire() failed to complete")

    def completed_pipeline(self, solution_id):
        """
        Once processing is complete return the score results to the TA3TA2-api layer
        """
        solution_index = self.solution_ids_to_index[solution_id]

        timestamp = Timestamp()
        start = timestamp
        end = timestamp
        start.GetCurrentTime()
        end.GetCurrentTime()

        pipeline_state = None

        # Ensure safe access to the nbest structure
        acquire = False
        try:
            acquire = self.mutex.acquire()
            completed_pipeline_count = self.nbest.item_list.__len__()

            if completed_pipeline_count <= solution_index:
                _logger.info("No valid pipelines have been returned yet")
                pipeline_state = dict(
                    state="running",
                    status="running",
                    start=start,
                    end=end,
                    metric=self.config.metric,
                    score=0.0,
                    rank=0.0
                )
            else:
                _logger.info("Candidate pipelines present...")
                pipeline_score = self.nbest.item_list[solution_index][2]
                # TODO: 20200805 Double check that this change works
                if self.optimizer_class == GamaRegressor:
                    # if isinstance(self.pipeline_optimizer, GamaRegressor):
                    _logger.info("Converting pipeline score from %s" % pipeline_score)
                    pipeline_score *= -1
                    _logger.info("to %s" % pipeline_score)
                pipeline_state = dict(
                    state="completed",
                    status="successful",
                    start=start,
                    end=end,
                    metric=self.config.metric,
                    score=pipeline_score,
                    rank=solution_index
                )
        finally:
            self.mutex.release()
            if not acquire:
                _logger.warning("Issue #31d: Potential deadlock avoided - self.mutex.acquire() failed to complete")

        return pipeline_state


    def fitted_result(self, solution_id=None):
        """
        Return the fitting of a solution status to the GetFitSolutionResults call in the TA3TA2-api layer
        """
        # This tells us which of the pipelines is associated with this solution id
        index = self.solution_ids_to_index[solution_id]
        timestamp = Timestamp()
        start = timestamp
        end = timestamp
        start.GetCurrentTime()
        end.GetCurrentTime()

        # Convert to a d3m pipeline so we can get the fitted output
        pipeline = self.pipelines(index=index)
        d3m_pipeline = pipeline[0].as_d3m_pipeline("Autoflow Pipeline", "Pipeline generated by AutoFlow optimizer", None)
        fitted_pipeline = Runtime(d3m_pipeline, context=Base.Context.TESTING, volumes_dir=self.config.static_volumes)
        fit_result = fitted_pipeline.fit(inputs=[self.dataset])

        pipeline_state = None

        # Ensure safe access to the nbest structure
        acquire = False
        try:
            acquire = self.mutex.acquire()
            completed_pipeline_count = self.nbest.item_list.__len__()

            if completed_pipeline_count <= index:
                _logger.info("No fitted pipelines have been returned yet")
                pipeline_state = dict(
                    state="running",
                    status="running",
                    start=start,
                    end=end,
                    metric=self.config.metric
                )
            else:
                _logger.info("Fitted pipelines now ready to return...")
                pipeline_state = dict(
                    state="completed",
                    status="successful",
                    start=start,
                    end=end,
                    metric=self.config.metric,
                    fit_result = fit_result
                )

        finally:
            self.mutex.release()
            if not acquire:
                _logger.warning("Issue #31e: Potential deadlock avoided - self.mutex.acquire() failed to complete")

        return pipeline_state


    def spinoff_produce_solution(self, dataset_schema, solution_id):
        while not self.solution_fitted:
            time.sleep(20)

        index = self.solution_ids_to_index[solution_id]

        predictions_file = None
        i = 0
        # Ensure safe access to the nbest structure
        for pipeline in self.pipelines():
            if i == index:
                if pipeline is None:
                    _logger.warning("There was a problem with this pipeline")
                    return None
                dataset = Dataset.load(dataset_schema)
                dataset = self.remove_attribute_from_target(dataset=dataset)
                try:
                    predictions_file = pipeline.produce_ta3(dataset=dataset, index=index)
                except Exception as e:
                    _logger.error("Exception encountered while producing. This is the Issue Tufts encountered "
                                  "at the July 2019 event that we could not repro. Should be fixed by Daraghs checkin on Nov 4th 2019...: %s", e)
                    _logger.warning(traceback.format_exc())
                break
            else:
                i += 1
        self.solution_ids_to_predictions[solution_id] = predictions_file


    def solution_results(self):
        return


    def spinoff_export(self, solution_id=None, rank=None):
        """
        Threaded entrypoint for the TA3TA2api to export the solutions to disk
        """
        _logger.info("Exporting pipeline for fitted_solution_id: %s and rank: %s" % (solution_id, rank))

        # what index goes with the fitted_solution_id specified
        solution_index = self.solution_ids_to_index[solution_id]
        self.checkpoint(index=solution_index)

        _logger.info("Exported pipelines for problem: %s", self.config.problem_id)


    def end_search(self, search_id):
        _logger.info("Removing search associated with search_id: %s" % search_id)
        # TODO: Probably need to purge the solution ids


    def convert_to_frame(self, table_index):
        table = self.dataset[table_index]
        columns = self.config.column_names(table_index)
        frame = pd.DataFrame(table, None, columns)

        # Suppress privileged columns for now
        # We'll have to figure out how to do something more sophisticated
        # for LUPI problems later
        if table_index in self.config.privileged_columns:
            pcols = self.config.privileged_columns[table_index]
            for col in pcols:
                #                print("Dropping privileged column", col)
                frame.drop(col, axis=1)

        for join_tabi, join_column in self.config.table_references(table_index):
            join_frame = self.convert_to_frame(join_tabi)
            frame = pd.merge(frame, join_frame, on=join_column, how='left')
        return frame


    def get_features(self):
        table_index, target_index = self.config.key_table_indexes()
        frame = self.convert_to_frame(table_index)
        target_name = self.config.get_target_name()
        frame = frame.drop(target_name, axis=1)
        return frame


    def get_targets(self, targs):
        raise NotImplementedError()


    def get_lupi_columns(self, dataset):
        tab, col = self.config.key_table_indexes()
        try:
            return list(self.config.privileged_columns[tab])
        except KeyError:
            return []


    def generate_preamble_pipelines(self, dataset, skip_augmentation=False):
        self.fragment_instances = []

        # this allows the user to force the systemt to use specific fragments. To use it add a ':' separated list to the
        # config like so:
        #       "fragments": "DSBoxTextFragment:SingleTableSemiFragment"
        if self.config.fragments is not None:
            fragments = self.config.fragments.split(":")
            _logger.info("Overriding fragments to use with:")
            for fragment in fragments:
                _logger.info("\tFragment: %s", fragment)
                self.fragment_instances.append(fragment)
        else:
            # Enhance this to study the data and selecting an appropriate ppln
            if self.config.taskType == 'semiSupervisedClassification':
                self.fragment_instances.append('SingleTableSemiFragment')
            elif self.config.taskType == 'timeSeriesForecasting':
                self.fragment_instances.append('TimeSeriesForecastingFragment')
            elif self.config.taskType == 'timeSeriesClassification':
                self.fragment_instances.append('TimeSeriesClassificationFragment')
            elif len(dataset) == 1:
                #TODO: Add this Fragment back in when we figure out how they are structuring the datasets with the
                # MIN_Metadata format (Jan 16 2020)
                # self.lupi_columns = self.get_lupi_columns(dataset)
                # if len(self.lupi_columns) > 0:
                #     self.fragment_instances.append('SingleTableLUPIFragment')
                # else:
                self.fragment_instances.append('SingleTableSRIFragment')
                # self.fragment_instances.append('SingleTableAutoRPIFragment')
            else:
                # Use multiple Fragments, one for each GAMA instance. SRI Standard text classification fragment:
                self.fragment_instances.append('MultiTableSRIFragment')

                # Notes:
                # 1) Corex seems to get stuck forever on 22_handgeometry_MIN_METADATA - may need to exclude it so we
                #    dont get stuck on some problems. The scores are not good for any known problem so probably better to
                #    mute it for now.
                # 2) Happens with 31_urbansound_MIN_METADATA as well - could be a media files issue
                #    ISI's Corex Text - scores suck on Leaderboard but try it anyway
                # 3) JIDO_SOHR_Tab_Articles_8569 gets stuck too - this and other such text heavy problems will do best to
                #    avoid this slow fragment
                # 4) Put this back in for now as it is killer good on the asam piracy data - we will need to have a custom
                #    assessment of text size to determine if it is worth including or not.
                # if 'audio' not in self.config.data_resource_types() and 'image' not in self.config.data_resource_types() \
                #         and 'video' not in self.config.data_resource_types() and 'text' not in self.config.data_resource_types():
                self.fragment_instances.append('DSBoxCorexTextFragment')

                # Testing new fragments
                self.fragment_instances.append('MLPTextFragment')
                self.fragment_instances.append('RandomForestTextFragment')
                self.fragment_instances.append('BaggingTextFragment')

                # This is a fully formed TFIDF Vectorizer pipeline - we are unable to use it with GAMA due to the 10's of
                # thousands of columns it produces so we will output it and use it directly.
                self.fragment_instances.append('TFIDFVectorizerFragment')

        # The fully specified fragments need to have their ranks set so that they dont step on the searched pipeline
        # ranks. Also - they need to be in sequence so they are picked up by CI properly
        pipeline_rank = 1
        self.full_pipeline_rank = {}
        bad_fragments = []
        fragment_weights = []
        self.preamble_pipelines = []
        for index in range (len(self.fragment_instances)):
            classname = self.fragment_instances[index]
            ppclass = find_pipeline_class(name=classname)

            if not ppclass.preamble_fragment:
                fragment_weights.append(0)
                _logger.info("Using Fully Rendered Pipeline: %s for %s problem. Rank: %s" % (classname, self.config.problem_id, pipeline_rank))
                self.full_pipeline_rank[index] = pipeline_rank
                pipeline_rank += 1
            else:
                fragment_weights.append(ppclass.fragment_weight)
                _logger.info("Using Fragment: %s for %s problem" % (classname, self.config.problem_id))

            if skip_augmentation:
                ahints = None
            elif self.config.augmentation_enabled():
                ahints = self.config.data_augmentation
            else:
                ahints = None
            _logger.info("Generate the preamble pipeline for %s" % classname)
            # If we encounter a problem building one of the fragments, remove it and allow the others to proceed.
            try:
                self.preamble_pipelines.append(ppclass.generate(dataset, config=self.config, augmentation_hints=ahints))
            except Exception as e:
                _logger.warning("Encountered error building pipeline for %s. Error: %s" % (classname, e))
                _logger.warning(traceback.format_exc())
                bad_fragments.append(index)
                continue
            _logger.info("Preamble pipeline generation complete for %s" % classname)
            preamble_args = dict(max_tokenized_expansion=30)
            for arg, val in self.config.preamble_arguments():
                preamble_args[arg] = val
            # Offset the index into the self.preamble_pipelines structure if some fragments failed to be added.
            preamble_args = dict((k, v) for k,v in preamble_args.items()
                                 if self.preamble_pipelines[index - len(bad_fragments)].has_configuration_option(k))
            self.preamble_pipelines[index - len(bad_fragments)].configure(**preamble_args)
            self.preamble_pipelines[index - len(bad_fragments)].study_dataset(dataset)

        # Clean up any remnants of failed fragments so they are not allocated resources.
        for bad_fragment in bad_fragments:
           del self.fragment_instances[bad_fragment]
           del fragment_weights[bad_fragment]

        # This method figures out how many cpus to give to each of the GAMA/Fragment instances based on their fragment
        # weight and the number cpus available
        self.apportion_cpus(tuple(fragment_weights))


    def allocate_by_weight(self, available, weights):
        distributed_amounts = []
        total_weights = sum(weights)
        for weight in weights:
            weight = float(weight)
            # Make sure we do not allocated CPU's to fragments that are pre set pipelines.
            if weight > 0:
                p = weight / total_weights
                distributed_amount = round(p * available)
                distributed_amounts.append(distributed_amount)
                total_weights -= weight
                available -= distributed_amount
        return distributed_amounts


    def apportion_cpus(self, fragment_weights):
        self.fragment_cpu_allocation = []
        fragment_count = len(self.fragment_instances)
        if fragment_count == 1:
            # nothing to do - the one instance will use all the available cpus
            self.fragment_cpu_allocation.append(self.optimizer_arguments['n_jobs'])
        else:
            cpu_count = self.optimizer_arguments['n_jobs']
            _logger.info("Sharing %s cpu's amongst the %s instances of GAMA" % (cpu_count, fragment_count))
            allocation = None
            for value in range(cpu_count + 1):
                allocation = self.allocate_by_weight(value, fragment_weights)
            for index in range (len(allocation)):
                cpus = allocation[index]
                _logger.info("Fragment %s gets %s cpus (weight: %s)" %
                             (self.fragment_instances[index], cpus, fragment_weights[index]))
                self.fragment_cpu_allocation.append(cpus)


    def execute_ta3_preamble(self, ta3_preamble, dataset):
        if ta3_preamble is None:
            self.ta3_preamble = None
            return dataset

        _logger.info("Executing TA3 preamble pipeline")
        self.ta3_preamble = ta3_preamble
        ta3_preamble = copy.copy(ta3_preamble)
        last_step = ta3_preamble.steps[-1]
        # If the last step is a placeholder, we remove it, making the
        # pipeline's output its inputs
        if isinstance(last_step, PlaceholderStep):
            outputs = [v['data'] for v in last_step.arguments.values()]
            ta3_preamble.steps.pop()
            ta3_preamble.outputs = outputs
        rt = Runtime(ta3_preamble, context=Base.Context.TESTING, volumes_dir=self.config.static_volumes)
        rt.fit(inputs=[dataset])
        outputs = rt.produce(inputs=[dataset])
        return outputs[0]


    def subselect_dataset(self, dataset):
        if not hasattr(self.config, 'maximum_rows'):
            return dataset
        maxrows = self.config.maximum_rows
        tab, col = self.config.key_table_indexes()
        df = dataset[tab]
        if df.shape[0] > maxrows:
            _logger.info("Selecting %d of %d rows" % (maxrows, df.shape[0]))
            dataset[tab] = df[0:maxrows]
        return dataset


    def prep_data(self, ta3_preamble=None):
        _logger.info("optimizer::prep_data called")

        dataset = self.remove_attribute_from_target()
        dataset = self.execute_ta3_preamble(ta3_preamble, dataset)
        dataset = self.subselect_dataset(dataset)

        _logger.info("\toptimizer::fit() getting preamble pipelines")
        # This method populates the self.preamble_pipelines member variable
        self.generate_preamble_pipelines(dataset, skip_augmentation=True)
        _logger.info("\toptimizer::fit() Preamble pipelines obtained")

        self.fitted_preambles = []
        for index in range (len(self.preamble_pipelines)):
            self.fitted_preambles.append(Runtime(self.preamble_pipelines[index], context=Base.Context.TESTING,
                                                   volumes_dir=self.config.static_volumes))
            _logger.info("\toptimizer::fit() Fitted Preamble obtained for Fragment %s (%s of %s)" %
                         (self.fragment_instances[index], index + 1, len(self.fragment_instances)))

        target_column_index = -1
        if hasattr(self.config, 'target_column_name'):
            # Use the column name to get the index of the target column
            target_column_index = int(numpy.where(dataset['learningData'].columns == self.config.target_column_name)[0][0])
        else:
            # This is for the open ml datasets which dont have a problemDoc.json containing a target_column_name
            for index, feature in enumerate(dataset['learningData'].columns):
                # When the suggested target is found, keep its column index and name
                if self.dataset.metadata.has_semantic_type(('learningData', Base.ALL_ELEMENTS, index),
                                                           'https://metadata.datadrivendiscovery.org/types/SuggestedTarget'):
                    target_column_index = index

        # Apply the TrueTarget metadata to the target column.
        dataset.metadata = dataset.metadata.add_semantic_type(('learningData', Base.ALL_ELEMENTS, target_column_index),
                                                             'https://metadata.datadrivendiscovery.org/types/TrueTarget')
        return dataset


    def get_features_and_targets_from_dataset(self, dataset, instance_index, skip_augmentation):
        _logger.info("In get_features_and_targets_from_dataset")

        # If this is a fully specified pipeline we will just dump it out instead of using Gama to find a solution
        if not self.preamble_pipelines[instance_index].preamble_fragment:
            return set(), set()

        _logger.info("Before fit for fragment %s" % self.fragment_instances[instance_index])
        fit_result = self.fitted_preambles[instance_index].fit(inputs=[dataset])
        _logger.info("After fit for fragment %s" % self.fragment_instances[instance_index])

        if fit_result.pipeline_run.status.get('state', None) == 'FAILURE':
            _logger.error(fit_result.pipeline_run.status['message'])
        # Now prep the data for the learner by running through a cleaning pipeline
        _logger.info("Before fitted_preamble.produce for Fragment %s (%s of %s)" %
                     (self.fragment_instances[instance_index], instance_index + 1, len(self.fragment_instances)))
        result = self.fitted_preambles[instance_index].produce(inputs=[dataset])
        # Manage failures or some of the fragments for some data formats:
        if 'outputs.0' in result.values and 'outputs.1' in result.values:
            features = result.values['outputs.0']
            targets = result.values['outputs.1']
            _logger.info("After fitted_preamble.produce for Fragment %s (%s of %s)" %
                         (self.fragment_instances[instance_index], instance_index + 1, len(self.fragment_instances)))
            _logger.info("Features for Fragment %s are: \n%s" % (self.fragment_instances[instance_index], features))
            _logger.info("Targets for Fragment %s are: \n%s" % (self.fragment_instances[instance_index], targets))
        else:
            _logger.error("Extracting the features and targets failed for Fragment %s. Donating allocated cpus"
                          % self.fragment_instances[instance_index])

            # Since the processes have already launched with a CPU allocation - this is probably not a useful thing to do
            if len(self.fragment_instances) > 1:
                available_cpus = self.fragment_cpu_allocation[instance_index]
                iterator = 0
                while available_cpus > 0:
                    adjusted_iterator = iterator % len(self.fragment_instances)
                    if instance_index != adjusted_iterator:
                        self.fragment_cpu_allocation[adjusted_iterator] += 1
                        available_cpus -= 1
                    iterator += 1

        return features, targets


    def list_primitives(self):
        """
        Collects and returns the primitive data assocaited with all available primitives for the the
        TA3 client to review
        """
        # need to be updated to current api, because `d3m.index.search()` does not longer return a dict
        primitives_info = []
        primitives = d3m.index.search()
        for primitive in primitives.values():
            id = primitive.metadata.query()['id']
            version = primitive.metadata.query()['version']
            python_path = primitive.metadata.query()['python_path']
            name = primitive.metadata.query()['name']
            digest = primitive.metadata.query()['digest']
            primitive_info = {
                'id': id,
                'version': version,
                'python_path': python_path,
                'name': name,
                'digest': digest
            }
            primitives_info.append(primitive_info)
        return primitive_info


    # This needs to be elaborated, so that we can resume search where we left off
    def get_state(self):
        return {}


    # Needs elaboration
    def set_state(self, stateobj):
        pass


class TPOTOptimizer(CROptimizer):
    # To use TPOT again you will need to (for all the following just look at the gama implementation for examples):
    # 1) Adapt to the multi fragment feature (see fit_sample_thread() in the CR Optimizer for how it is supported in
    #    GAMA). This will involve calculating the features and targets for each fragment and passing them in to a
    #    different thread for each TPOT instance
    # 2) Use the cpu_allocation for each fragment instead of using the cpu optimizer argument

    def get_optimizer_arguments(self, pipeline_eval_callback, gen_callback):
        optr_args = self.optimizer_arguments
        optr_args['scoring'] = self.config.metric
        optr_args['update_callback'] = pipeline_eval_callback
        optr_args['generation_callback'] = gen_callback
        # search_time = self.config.timeout * 0.66
        # _logger.info("Adjustime search time from %s to: %s" % (self.config.timeout, search_time))
        optr_args['max_time_mins'] = self.config.timeout
        optr_args['max_eval_time_mins'] = 4
        optr_args['verbosity'] = 2
        optr_args['random_state'] = 42
        config_path = self.get_config_path()
        _logger.info("Using config file: %s" % config_path)
        optr_args['config_dict'] = config_path
        super().get_optimizer_arguments(pipeline_eval_callback, gen_callback)


    def fit_sample(self, max_minutes, picklePath, sample_size=None):
        """
        Fit on a sample of the data.
        """
        if max_minutes <= 0:
            return
        if sample_size is None:
            self.pipeline_optimizer.max_time_mins = max_minutes
            feats = self.features
            targs = self.targets
        else:
            self.pipeline_optimizer.max_time_mins = None
            self.pipeline_optimizer.generations = 3
            permutation = list(range(self.features.shape[0]))
            numpy.random.shuffle(permutation)
            feats = self.features.iloc[permutation[0:sample_size], :]
            targs = self.targets.iloc[permutation[0:sample_size], :]
        self.fit_optimizer(feats, targs, picklePath)


    def fit_optimizer(self, feats, targs, picklePath):
        self.pipeline_optimizer.fit(feats, targs, picklePath, **dict(self.config.fit_arguments()))


    def clean_pipeline_string(self, p):
        return self.pipeline_optimizer.clean_pipeline_string(p)


    def pipelines(self, index=None):
        # Ensure safe access to the nbest structure
        acquire = False
        try:
            _logger.warning("Trying to acquire the mutex for pipelines()...")
            acquire = self.mutex.acquire()
            _logger.warning("Got it! Now to get the top pipelines...")
            top_pipelines = self.nbest.result(True, True)
            _logger.warning("Got Them!...")
        finally:
            self.mutex.release()
            if not acquire:
                _logger.warning("Issue #31f: Potential deadlock avoided - self.mutex.acquire() failed to complete")

        pipelines = []
        if index is not None:
            score, pipeline, created, fragment_index = top_pipelines[index]
            pln = TPOTPipeline(ta3_preamble=self.ta3_preamble,
                               preamble=self.preamble_pipelines[fragment_index],
                               fitted_preamble=self.fitted_preambles[fragment_index],
                               pipeline=pipeline,
                               optimizer=self,
                               score=score,
                               rank=index + 1,
                               created=created
                               )
            _logger.info("Created pipeline at index %s %s (%d, %f): %s" %
                         (index, pln.name, pln.rank, pln.score, pln))
            pipelines.append(pln)
        else:
            rank = 1
            for score, pipeline, created, fragment_index in top_pipelines:
                pln = TPOTPipeline(ta3_preamble=self.ta3_preamble,
                                       preamble=self.preamble_pipelines[fragment_index],
                                       fitted_preamble=self.fitted_preambles[fragment_index],
                                       pipeline=pipeline,
                                       optimizer=self,
                                       score=score,
                                       rank=rank,
                                       created=created
                                       )
                _logger.info("Created pipeline %s (%d, %f): %s" %
                             (pln.name, pln.rank, pln.score, pln))
                pipelines.append(pln)
                rank += 1
        return pipelines


    def get_pipeline(self, solution_id):
        solution_index = self.solution_ids_to_index[solution_id]
        _logger.info("Requesting Pipeline Description for solution_id %s and index %s" % (solution_id, solution_index))
        pipeline = self.pipelines(index=solution_index)
        if pipeline is None:
            _logger.info("Pipeline is not yet ready to describe for this solution id: %s" % solution_id)
        return pipeline


    def considered_pipelines(self):
        """
        Generator yielding all evaluated pipelines
        """
        # Ensure safe access to the nbest structure
        acquire = False
        try:
            acquire = self.mutex.acquire()
            all_pipelines = self.nbest.all_items(True, True)
            for score, pipeline, created, fragment_index in all_pipelines:
                pln = TPOTPipeline(ta3_preamble=self.ta3_preamble,
                                       preamble=self.preamble_pipelines[fragment_index],
                                       pipeline=pipeline,
                                       optimizer=self,
                                       score=score,
                                       created=created
                                       )
                yield pln
        finally:
            self.mutex.release()
            if not acquire:
                _logger.warning("Issue #31g: Potential deadlock avoided - self.mutex.acquire() failed to complete")


class TPOTRegressionOptimizer(TPOTOptimizer):

    if os.getenv('TPOT') is not None:
        optimizer_class = TPOTRegressor

    def get_config_path(self):
        return "%s/tpot-regressor-config.py" % self.config.code_directory()


class TPOTClassificationOptimizer(TPOTOptimizer):

    if os.getenv('TPOT') is not None:
        optimizer_class = TPOTClassifier

    def get_config_path(self):
        return "%s/tpot-classifier-config.py" % self.config.code_directory()


class GamaOptimizer(CROptimizer):

    def get_optimizer_arguments(self, pipeline_eval_callback, gen_callback):
        # Store the callbacks for a later step
        self.pipeline_evaluation_callback = pipeline_eval_callback
        self.generation_callback = gen_callback

        optr_args = self.optimizer_arguments
        optr_args['random_state'] = 42
        metric = self.config.metric if self.config.metric != 'f1_true' else 'f1'
        optr_args['scoring'] = metric
        # search_time = self.config.timeout * 0.66
        # _logger.info("Adjust search time from %s to: %s" % (self.config.timeout, search_time))
        optr_args['max_total_time'] = (self.config.timeout) * 60

        # We tried making this number equal to the max_total_time but it improved some problems and made others worse.
        # A more nuanced approach is likely required that adapts to the problem at hand.
        optr_args['max_eval_time'] = 4*60
        # We tried the following to see if the slow primitives might yield better performance given enough time - they
        # did not.
        # optr_args['max_eval_time'] = optr_args['max_total_time']

        fname, rule_name = self.config.grammar_rule()
        optr_args['grammar_file_name'] = fname
        optr_args['rule_name'] = rule_name
        optr_args['cache_dir'] = "%s/%s" % (self.config.temp, datetime.now().strftime("%Y%m%d_%H%M%s"))
        optr_args['post_processing_method'] = self.config.gama_postprocessor()
        _logger.info("Max total time: %s" % optr_args['max_total_time'])
        super().get_optimizer_arguments(pipeline_eval_callback, gen_callback)


    def clean_pipeline_string(self, p):
        return str(p)


    def fit_sample(self, features, targets, max_minutes, picklePath, instance_index, sample_size=None):
        """
        Fit on a sample of the data.
        """
        if max_minutes <= 0:
            return
        if sample_size is None:
            feats = features
            targs = targets
        else:
            permutation = list(range(features.shape[0]))
            numpy.random.shuffle(permutation)
            feats = features.iloc[permutation[0:sample_size], :]
            targs = targets.iloc[permutation[0:sample_size], :]

        self.optimizer_arguments['max_total_time'] = max_minutes * 60

        fragment_optimizer_args = dict(self.optimizer_arguments)

        # Ensure each Gama instance has a different cache area
        fragment_optimizer_args['cache_dir'] = "%s_%s" % (self.optimizer_arguments['cache_dir'], str(instance_index))

        # Use the allocated cpu count for this fragment:
        fragment_optimizer_args['n_jobs'] = self.fragment_cpu_allocation[instance_index]
        _logger.info("Fragment %s being launched with %s cpus" %
                     (self.fragment_instances[instance_index], fragment_optimizer_args['n_jobs']))

        fragment_pipeline_optimizer = self.optimizer_class(**fragment_optimizer_args)
        self.fit_optimizer(feats, targs, picklePath, instance_index, fragment_pipeline_optimizer)


    def fit_arguments(self):
        args = dict(self.config.fit_arguments())
        args['d3m_mode'] = True
        return args


    def fit_optimizer(self, feats, targs, picklePath, instance_index, fragment_pipeline_optimizer):
        peval_callback = self.pipeline_evaluation_callback
        gen_callback = self.gen_callback
        popt = fragment_pipeline_optimizer

        def eval_complete_func(pl):
            score = pl.fitness.values[0]
            peval_callback(pl, score, instance_index)

        popt.evaluation_completed(eval_complete_func)
        popt._observer.on_pareto_updated(lambda _: gen_callback())
        popt.fit(feats, targs, **self.fit_arguments())


    # Convenience method to make the next one shorter and easier to understand
    def _make_pipeline(self, rank, score=0, ensemble=None, pipeline=None, created=None, fragment_index=None):
        if ensemble:
            return GamaEnsemblePipeline(preamble=self.preamble_pipelines[fragment_index],
                                        ta3_preamble=self.ta3_preamble,
                                        fitted_preamble=self.fitted_preambles[fragment_index],
                                        ensemble=self.pipeline_optimizer.model,
                                        score=score,
                                        rank=rank,
                                        optimizer=self)
        else:
            _logger.info("Making a GamaPipeline for fragment %s (%s of %s)" %
                         (self.fragment_instances[fragment_index], fragment_index + 1, len(self.fragment_instances)))
            return GamaPipeline(ta3_preamble=self.ta3_preamble,
                                preamble=self.preamble_pipelines[fragment_index],
                                fitted_preamble=self.fitted_preambles[fragment_index],
                                pipeline=pipeline,
                                optimizer=self,
                                score=score,
                                rank=rank,
                                created=created,
                                fragment=self.fragment_instances[fragment_index])


    def pipelines(self, index=None):
        # Ensure safe access to the nbest structure
        acquire = False
        try:
            _logger.warning("Trying to acquire the mutex for pipelines()...")
            acquire = self.mutex.acquire()
            _logger.warning("Got it! Now to get the top pipelines...")
            top_pipelines = self.nbest.result(include_scores=True, include_timestamps=True, include_fragment_index=True)
            _logger.warning("Got Them!...")
        finally:
            self.mutex.release()
            if not acquire:
                _logger.warning("Issue #31h: Potential deadlock avoided - self.mutex.acquire() failed to complete")

        # Special case: The caller has requested a particular index. We return a single pipeline
        if index is not None:
            _logger.warning("Getting pipeline at index %s" % index)
            if len(top_pipelines) < index + 1:
                _logger.warning("Returning None")
                return None
            _logger.warning("Getting the top pipeline")
            score, pipeline, created, fragment_index = top_pipelines[index]
            _logger.warning("Returning! _make_pipeline")
            # Index is 0 based but rank is 1 based so we must increment first. Also get the correct preamble_pipeline and fitted_preamble
            return [self._make_pipeline(index + 1, score=score, pipeline=pipeline, created=created, fragment_index=fragment_index)]

        rank = 1
        pipelines = []

        if self.config.ensembling_enabled():
            # TODO: When we enable Ensembling we will need to experiment with creating the correct structure for TA3
            _logger.error("Implement proper TA3 handling of ensemble pipelines")
            # TODO: Figure out how to come up with a good score for ensembles
            pln = self._make_pipeline(1, ensemble=self.pipeline_optimizer.model)
            _logger.info("Created ensemble pipeline %s (%d, %f): %s" %
                             (pln.name, pln.rank, pln.score, pln))
            pipelines.append(pln)
            rank += 1

        # Initialize the structure for determining which fragments yielded the most top pipelines.
        fragment_shares = []
        for fragment_index in self.fragment_instances:
            fragment_shares.append(0)

        first = True
        top_pipeline_fragment = -1
        for score, pipeline, created, fragment_index in top_pipelines:
            if first:
                first = False
                top_pipeline_fragment = fragment_index
            # For each top_pipeline we need to get the correct preamble_pipeline and fitted_preamble
            pln = self._make_pipeline(rank, score=score, pipeline=pipeline, created=created, fragment_index=fragment_index)
            _logger.info("Created all pipeline %s (%d, %f): %s" % (pln.name, pln.rank, pln.score, pln))
            pipelines.append(pln)
            rank += 1
            fragment_shares[fragment_index] = fragment_shares[fragment_index] + 1

        _logger.info("Top pipeline used Fragment: %s" % self.fragment_instances[top_pipeline_fragment])
        _logger.info("Fragments used in top %s pipelines:" % self.pipeline_count)
        for index in range (len(fragment_shares)):
            _logger.info("\tFragment %s yielded %s top pipelines" % (self.fragment_instances[index],
                                                                     fragment_shares[index]))

        return pipelines


    def considered_pipelines(self):
        """
        Generator yielding the top-ranked pipelines
        """
        # Ensure safe access to the nbest structure
        acquire = False
        try:
            acquire = self.mutex.acquire()
            all_pipelines = self.nbest.all_items(include_scores=True, include_timestamps=True, include_fragment_index=True)
            for score, pipeline, created, fragment_index in all_pipelines:
                # Track of which pipeline goes with which preamble by using the fragment_index
                pln = GamaPipeline(ta3_preamble=self.ta3_preamble,
                                       preamble=self.preamble_pipelines[fragment_index],
                                       pipeline=pipeline,
                                       optimizer=self,
                                       score=score,
                                       created=created
                                       )
                yield pln
        finally:
            self.mutex.release()
            if not acquire:
                _logger.warning("Issue #31i: Potential deadlock avoided - self.mutex.acquire() failed to complete")

    def get_pipeline(self, solution_id):
        solution_index = self.solution_ids_to_index[solution_id]
        _logger.info("Requesting Pipeline Description for solution_id %s and index %s" % (solution_id, solution_index))
        pipeline = self.pipelines(index=solution_index)
        if pipeline is None:
            _logger.info("Pipeline is not yet ready to describe for this solution id: %s" % solution_id)
        return pipeline


class GamaClassificationOptimizer(GamaOptimizer):

    optimizer_class = GamaClassifier

    def get_optimizer_arguments(self, pipeline_eval_callback, gen_callback):
        super().get_optimizer_arguments(pipeline_eval_callback, gen_callback)
        classification_white_list_file = None
        if self.config.use_whitelist:
            classification_white_list_file = "whitelist_classification.json"
        self.optimizer_arguments['config'] = config_to_d3m(classifier_config_dict,
                                                           d3m_white_list_file=classification_white_list_file,
                                                           family="classification")

    def fit_arguments(self):
        args = super().fit_arguments()
        args['pos_label'] = self.config.pos_label
        return args

class GamaRegressionOptimizer(GamaOptimizer):

    optimizer_class = GamaRegressor

    def get_optimizer_arguments(self, pipeline_eval_callback, gen_callback):
        super().get_optimizer_arguments(pipeline_eval_callback, gen_callback)
        regression_white_list_file = None
        if self.config.use_whitelist:
            regression_white_list_file = "whitelist_regression.json"
        self.optimizer_arguments['config'] = config_to_d3m(regressor_config_dict,
                                                           d3m_white_list_file=regression_white_list_file,
                                                           family="regression")


class GamaTimeSeriesForecastingOptimizer(GamaOptimizer):

    optimizer_class = GamaTimeSeriesForecaster

    def fit_sample(self, features, targets, max_minutes, picklePath, instance_index, sample_size=None):
        # We never sample for time series problems (last param should always be None!). It introduces too many difficulties.
        super().fit_sample(features, targets, max_minutes, picklePath, instance_index, None)

    def get_features_and_targets_from_dataset(self, dataset, instance_index, skip_augmentation):
        features, targets = super().get_features_and_targets_from_dataset(dataset, instance_index, skip_augmentation)

        # Now introspect the data and tell data transformation primitives to keep their hands off
        # any time columns or grouping keys.
        # Currently we only have 1 timeseries forecasting fragment (hence the list index [0])
        sequestered_columns = features[0].metadata.list_columns_with_semantic_types([DATETIME_TYPE] + KEY_TYPES)
        if len(sequestered_columns) > 0:
            for family in TRANSFORMER_FAMILIES:
                #D3MWrapper.set_family_hyperpameters(family, exclude_columns=sequestered_columns)
                D3MWrapper.set_family_hyperpameters(family, exclude_columns=sequestered_columns,
                                                    use_semantic_types=True, return_result='append')

        # Also, as a temporary workaround for a bug in VAR, add the target type to target columns
        target_columns = targets[0].metadata.list_columns_with_semantic_types([TRUE_TARGET_TYPE])
        for column_index in target_columns:
            targets[0].metadata = targets[0].metadata.add_semantic_type((Base.ALL_ELEMENTS, column_index), TARGET_TYPE)

        return features, targets

    def get_optimizer_arguments(self, pipeline_eval_callback, gen_callback):
        super().get_optimizer_arguments(pipeline_eval_callback, gen_callback)
        regression_white_list_file = None
        if self.config.use_whitelist:
            regression_white_list_file = "whitelist_regression.json"
        # self.optimizer_arguments['n_jobs'] = 1
        # You can use 'tsf_config_dict' or 'tsf_config_dict_pure' below to get different behaviors
        self.optimizer_arguments['config'] = config_to_d3m(tsf_config_dict,
                                                           d3m_white_list_file=regression_white_list_file,
                                                           family="regression")


class GamaTimeSeriesClassificationOptimizer(GamaOptimizer):

    optimizer_class = GamaTimeSeriesClassifier

    def fit_sample(self, features, targets, max_minutes, picklePath, instance_index, sample_size=None):
        # We never sample for time series problems (last param should always be None!). It introduces too many difficulties.
        super().fit_sample(features, targets, max_minutes, picklePath, instance_index, None)

    def get_optimizer_arguments(self, pipeline_eval_callback, gen_callback):
        super().get_optimizer_arguments(pipeline_eval_callback, gen_callback)
        classification_white_list_file = None
        if self.config.use_whitelist:
            classification_white_list_file = "whitelist_classification.json"
        self.optimizer_arguments['config'] = config_to_d3m(classifier_config_dict,
                                                           d3m_white_list_file=classification_white_list_file,
                                                           family="classification")


