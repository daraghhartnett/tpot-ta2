import logging
import sys
import os
import subprocess
import json
import plac

# init logger
logging.basicConfig(level=logging.INFO, format='%(asctime)s [%(levelname)s] %(name)s -- %(message)s')
_logger = logging.getLogger(__name__)

'''
This class runs fit-score on all the pipelines in the supplied directory using the specified docker image and seed data 
set directory. It was originally designed to make running the challenge problem pipelines easy but also it can be 
used to run the CI for our sample pipelines.
Here is a sample invocation:

> pwd 
./tpot-ta2/autoflow/integ-test
> python run_challenge_pipelines /Users/daraghhartnett/Projects/D3M/evals/20190607/challenge/challenge-pipelines/ registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:20190607-devel /datasets/seed_datasets_current/

'''


class RunPipelines(object):

    def __init__(self, **args):
        self._add_attrs(args, 'pipeline_directory', 'docker_image', 'dataset_home')

    def _add_attrs(self, args, *attrs):
        for attr in attrs:
            if attr in args:
                setattr(self, attr, args[attr])

    '''
    This main method builds the parameters to the docker commands and calls them in succession. 
    '''

    def main(self):
        _logger.info("Run Pipelines in the directory: %s" % self.pipeline_directory)

        # 1 Get the list of pipelines to run
        pipelines, metafiles, logfiles = self.get_pipelines(pipeline_directory=self.pipeline_directory)

        # 2 Create a docker run command for each one
        docker_commands = self.build_docker_commands(pipelines=pipelines, metafiles=metafiles, logfiles=logfiles)

        # 3 Run and produce output
        for command in docker_commands:
            # Launch the command
            _logger.info("Launching: %s", command)
            try:
                ret_stdout = subprocess.check_output(command, shell=True)
                _logger.info("...Command completed")
            except Exception as e:
                _logger.info("...Command failed during execution")
                _logger.warning(e)

        _logger.info("Commands completed")

    '''
    This method finds the pipelines in the supplied directory and returns a list of them pipeline and meta file 
    relative paths. It also returns the output log file for each pipeline. 
    '''

    def get_pipelines(self, pipeline_directory):
        pipeline_list = []
        meta_file_list = []
        log_file_list = []

        # r=root, d=directories, f = files
        for r, d, f in os.walk(pipeline_directory):
            for file in f:
                # avoiding any primitive.json files as they are primitives, not pipelines
                if '.json' in file and 'primitive.json' not in file:
                    full_file_path = os.path.join(r, file)
                    relative = full_file_path.replace(pipeline_directory, "")
                    meta_file = relative.replace("json", "meta")
                    log_file = full_file_path.replace("json", "log")
                    pipeline_list.append(relative)
                    meta_file_list.append(meta_file)
                    log_file_list.append(log_file)

        return pipeline_list, meta_file_list, log_file_list

    '''
    This method builds a valid docker run command for each pipeline.
    The following is an example of what we are trying to duplicate with this method:

        docker run -it 
                   --rm 
                   -v /D3M/datasets/seed_datasets_current/:/data 
                   -v /D3M/challenge/challenge-pipelines/:/pipelines 
                   registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:20190607-devel 
                   python3 -m d3m runtime 
                           -d /data 
                           fit-score 
                           -p /pipelines/vertex-classification/4a55d0bf-3e54-4117-ac2f-fb06b192c1de.json 
                           -m /pipelines/vertex-classification/4a55d0bf-3e54-4117-ac2f-fb06b192c1de.meta 
                           > vertex-classification/4a55d0bf-3e54-4117-ac2f-fb06b192c1de.log
    '''

    def build_docker_commands(self, pipelines, metafiles, logfiles):
        docker_commands = []

        for pipeline, metafile, logfile in zip(pipelines, metafiles, logfiles):
            command = "docker run -i --rm -v %s:/data -v %s:/pipelines %s python3 -m d3m runtime -d /data fit-score " \
                      "-p /pipelines/%s -m /pipelines/%s  2>&1 | tee %s" % (
                      self.dataset_home, self.pipeline_directory, self.docker_image, pipeline, metafile, logfile)
            docker_commands.append(command)

        return docker_commands


'''
Entry point - required to make python happy
'''
if __name__ == "__main__":
    def main(pipeline_directory: "Directory that contains the pipelines",
             docker_image: "Docker Image to use",
             dataset_home: "Location of the seed datasets"):
        RunPipelines(pipeline_directory=pipeline_directory, docker_image=docker_image, dataset_home=dataset_home).main()


    plac.call(main)