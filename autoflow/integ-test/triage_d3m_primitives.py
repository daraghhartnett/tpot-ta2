import logging
import sys
import os
import os.path
import subprocess
import yaml
from datetime import datetime
from dateutil import parser
import json
import csv
import concurrent.futures
from itertools import repeat

CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(CURRENT_DIR))
sys.path.append(os.path.abspath('config'))

# init logger
logging.basicConfig(level=logging.INFO, format='%(asctime)s [%(levelname)s] %(name)s -- %(message)s')
_logger = logging.getLogger(__name__)

class TriageD3MPrimitives(object):
    '''
    This script is run manually (or through the generate_whitelist CI job) when we want to create a new primitive white
    list.
    This class uses the latest docker image with all of the D3M primitives to figure out which ones are well behaved.
    It does this by doing the following:
      1) Calls integ-test/generate_d3m_primitive_pipelines.py which loads all available primitives and checks to see
         if their hyper parameters are well formed - if all looks ok they are written to a candidate config file with
         a range of hyper parameter values that should be tried (d3m_primitive_config.json). If they are not well formed,
         they are written to the black list instead. From the valid white list primitives, the ones that are in the
         families 'classification', 'regression', 'feature_selection', 'data_preprocessing' or 'data_transformation' are
         added to a simple pipeline and written to disk for later use. If this process fails for any reason they are
         then added to the black list.
      2) Control then returns to this script which attempts to run each of the candidate primitive pipelines on a fast
         (185_baseball) and then slow (LL1_VTXC_1343_cora) dataset.
      3) If the pipeline does not complete without error or fails to produce a score, it is added to the black list.
      4) Next, the list of candidate primitives is loaded back up from the config file generated in step 1
         (d3m_primitive_config.json). The black list is also loaded and for each primitive listed in it, the
         corresponding primitive json and associated hyperparam json values is purged from the config. Once complete a
         new purged file is written to disk: d3m_primitive_config_purged.json
      5) This file, d3m_primitive_config_purged.json can be hand edited or left as is. At TA2 load time, this file is
         imported by config_converter.py::config_to_d3m and used by GAMA for pipeline optimization.
    '''


    def __init__(self, **args):
        self._add_attrs(args)

    def _add_attrs(self, args, *attrs):
        for attr in attrs:
            if attr in args:
                setattr(self, attr, args[attr])

    def main(self):
        # Grab the environment parameters
        self.dataset_home = os.environ['D3M_Dataset_Home']
        self.output_home = os.environ['D3M_Output_Home'] + "/primitive_validation"
        self.docker_image = os.environ['D3M_Docker_Image']
        self.scoring_home = os.environ['D3M_Scoring_Home']
        self.threads = int(os.environ['THREADS'])

        input_mapping = "-v %s:/input/" % self.dataset_home
        output_mapping = "-v %s:/output/" % self.output_home
        blacklist_file = "blacklist_file.tsv"
        whitelist_file = "whitelist_file.tsv"
        d3m_primitive_configfile = "d3m_primitive_config_regression.json"

        if not os.path.exists(self.output_home):
            os.makedirs(self.output_home)

        # Purge previous pipelines & runs
        for the_file in os.listdir(self.output_home):
            file_path = os.path.join(self.output_home, the_file)
            try:
                if os.path.isfile(file_path):
                    os.unlink(file_path)
            except Exception as e:
                _logger.warning(e)

        blacklist_file_handle = open(self.output_home + "/" + blacklist_file, "w+")
        blacklist_file_handle.write("%s\t%s\n" % ("Primitive", "Failure Message"))
        blacklist_file_handle.flush()
        blacklist_file_handle.close()

        whitelist_file_handle = open(self.output_home + "/" + whitelist_file, "w+")
        # name \t family-type \t fast-time \t fast-score \t slow-time \t slow-score
        whitelist_file_handle.write("%s\t%s\t%s\t%s\t%s\t%s\t%s\n" % ("Primitive", "Family", "Quick Dataset Duration (MicroSec)",
                                                                  "Quick Dataset Score", "Slow Dataset Duration (MicroSec)",
                                                                  "Slow Dataset Score", "Metric"))
        whitelist_file_handle.flush()
        whitelist_file_handle.close()

        pipeline_instances = 3

        # Generate the test pipelines
        _logger.info(">> Generate the Pipelines for all D3M primitives")
        generate_command = "docker run -i --rm %s %s %s python3 integ-test/generate_d3m_primitive_pipelines.py /output %s %s %s" \
                           % (input_mapping, output_mapping, self.docker_image, blacklist_file, d3m_primitive_configfile,
                              pipeline_instances)
        subprocess.check_output(generate_command, shell=True)

        # Create a set of all valid pipelines.
        pipeline_names = set()
        pipeline_names_list = []
        pipeline_names_param = ""
        for pipeline in os.listdir(self.output_home):
            # Prevents non pipelines and primitive list files from being picked up for run
            if not pipeline.endswith(".json") or pipeline.startswith(d3m_primitive_configfile[:-5]):
                continue
            pipeline_name = pipeline.replace(".json", "")
            pipeline_names.add(pipeline_name)
            pipeline_names_list.append(pipeline_name)
            pipeline_names_param += "%s:" % pipeline_name

        # Remove trailing :
        pipeline_names_param = pipeline_names_param[:-1]

        # First run with a quick dataset
        # Classification:
        # fast_dataset = "185_baseball_MIN_METADATA"
        # Regression:
        fast_dataset = "196_autoMpg_MIN_METADATA"
        # Next run with a slower dataset to see what difference it makes (a successful run on either will be accepted)
        slow_dataset = "LL1_VTXC_1343_cora_MIN_METADATA"

        # For each pipeline, run it on the fast dataset in a thread
        thread_count = self.threads
        with concurrent.futures.ThreadPoolExecutor(max_workers=thread_count) as executor:
            executor.map(self.run_pipeline_thread, pipeline_names_list, repeat(fast_dataset))

        # For each pipeline, run it on the slow dataset in a thread
        # with concurrent.futures.ThreadPoolExecutor(max_workers=thread_count) as executor:
        #     executor.map(self.run_pipeline_thread, pipeline_names_list, repeat(slow_dataset))

        # Used to track how many instances of this primitive have been tested
        instance_failures_map = {}
        instance_success_set = set()

        blacklist_file_handle = open(self.output_home + "/" + blacklist_file, "a+")
        whitelist_file_handle = open(self.output_home + "/" + whitelist_file, "a+")
        for pipeline in pipeline_names:
            base_primitive = pipeline.split("-", 1)[0]
            if base_primitive in instance_success_set:
                _logger.info("Primitive %s already passed! Skipping run" % base_primitive)
                continue

            _logger.info("Validating pipeline : %s" % pipeline)

            package_parts = pipeline.split('.')
            family = package_parts[2]

            fast_execution_successful = True
            score_file = "%s/%s-%s-scores.csv" % (self.output_home, pipeline, fast_dataset)
            valid_fast_score_file = self.check_score_file(score_file)
            if valid_fast_score_file:
                fast_dataset_score = self.load_scores_data(score_file)
            else:
                fast_execution_successful = False
                fast_dataset_score = ["n/a", -1]

            # slow_execution_successful = True
            # score_file = "%s/%s-%s-scores.csv" % (self.output_home, pipeline, slow_dataset)
            # valid_slow_score_file = self.check_score_file(score_file)
            # if valid_slow_score_file:
            #     slow_dataset_score = self.load_scores_data(score_file)
            # else:
            slow_execution_successful = False
            slow_dataset_score = ["n/a", -1]

            if slow_execution_successful or fast_execution_successful:
                _logger.info("Primitive %s passed! Adding it to the success set" % base_primitive)
                instance_success_set.add(base_primitive)
                if fast_execution_successful:
                    pipeline_run_file = "%s/%s-%s-pipeline_run.yml" % (self.output_home, pipeline, fast_dataset)
                    fast_dataset_duration = self.get_primitive_duration(pipeline_run_file, self.output_home)
                else:
                    fast_dataset_duration = "-1"

                if slow_execution_successful:
                    pipeline_run_file = "%s/%s-%s-pipeline_run.yml" % (self.output_home, pipeline, slow_dataset)
                    slow_dataset_duration = self.get_primitive_duration(pipeline_run_file, self.output_home)
                else:
                    slow_dataset_duration = "-1"

                # name \t family-type \t fast-time \t fast-score \t slow-time \t slow-score \t metric
                metric = fast_dataset_score[0]
                fast_score = str(round(float(fast_dataset_score[1]), 2))
                slow_score = str(round(float(slow_dataset_score[1]), 2))
                whitelist_file_handle.write(
                    "%s\t%s\t%s (ms)\t%s\t%s (ms)\t%s\t%s\n" % (base_primitive, family, fast_dataset_duration,
                                                                    fast_score, slow_dataset_duration,
                                                                    slow_score, metric))
                whitelist_file_handle.flush()
            else:
                base_primitive = pipeline.split("-", 1)[0]
                if base_primitive not in instance_failures_map:
                    instance_failures_map[base_primitive] = 0
                failure_count = instance_failures_map[base_primitive]
                failure_count += 1
                _logger.info("Primitive %s has now failed %s times" % (base_primitive, failure_count))
                instance_failures_map[base_primitive] = failure_count
                # Only fail the primitive if all instances fail to run
                if failure_count == pipeline_instances:
                    blacklist_file_handle.write("%s\t%s\n" % (base_primitive, "Execution of pipeline failed"))
                    blacklist_file_handle.flush()

        # Close the file handles when we are all done
        blacklist_file_handle.close()
        whitelist_file_handle.close()

        _logger.info(">> Generate the config for the permissible D3M primitives")
        self.dump_primitive_json_config(self.output_home, whitelist_file, d3m_primitive_configfile)


    def run_pipeline_thread(self, pipeline_name, dataset):
        input_mapping = "-v %s:/input/" % self.dataset_home
        output_mapping = "-v %s:/output/" % self.output_home
        scoring_mapping = "-v %s:/scoring/" % self.scoring_home
        static_folder_mapping = "-v %s:/volumes/" % "/D3M/static/"
        run_command = "docker run -i --rm --env D3MSTATICDIR=/volumes %s %s %s %s %s python3 integ-test/run_d3m_primitive_pipeline.py %s %s" \
                      % (input_mapping, output_mapping, scoring_mapping, static_folder_mapping, self.docker_image,
                         pipeline_name, dataset)
        subprocess.check_output(run_command, shell=True)


    def dump_primitive_json_config(self, output_home, whitelist_file, d3m_primitive_configfile):
        """
        Dump any primitive that does not appear in the whitelist. We do this to ensure that the whitelist only contains
        primitives that we are not already loading as part of our basic initialization (I am a little surprised by this,
        I thought the whitelist replaced the default mechanism but that is not the case - will have to identify this)
        """
        # Load up the white list to filter the primitives we will use
        whitelist_primitive_name_set = set()
        lines = [line.rstrip('\n') for line in open("%s/%s" % (output_home, whitelist_file))]
        first_line = True
        # Parse out all the white listed primitives
        for primitive_line in lines:
            if first_line:
                first_line = False
                continue
            primitive_name = primitive_line.split("\t")[0]
            whitelist_primitive_name_set.add(primitive_name)

        # Load up the set of d3m primitives found in the complete image
        primitive_json = self.load_json("%s/%s" % (output_home, d3m_primitive_configfile))

        purged_configfile = d3m_primitive_configfile.replace(".json", "_purged.json")

        _logger.info("Primitives before purge: %s" % len(primitive_json))

        updated_primitive_dict = primitive_json.copy()

        for primitive in primitive_json:
            if primitive not in whitelist_primitive_name_set:
                updated_primitive_dict.pop(primitive)

        _logger.info("Primitives after purge: %s" % len(updated_primitive_dict))

        # Write out the updated d3m primitive config file that will be loaded by TA2 at run time
        self.write_config(updated_primitive_dict, "%s/%s" % (output_home, purged_configfile))


    def load_json(self, file):
        with open(file) as json_file:
            data = json.load(json_file)
            return data


    def write_config(self, config, config_file):
        _logger.info("Writing primitive data to config file %s" % config_file)
        with open(config_file, 'w') as file_handle:
            json.dump(config, file_handle, indent=4)


    def get_duration(self, start_time):
        """
        Used to track how long each pipeline takes to complete
        """
        minutes = round(((datetime.now() - start_time).total_seconds() / 60), 2)
        return minutes


    def load_scores_data(self, scores_file):
        """
        Load the scores data from the scores file provided
        """
        with open(scores_file) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            line_count = 0
            for row in csv_reader:
                if line_count == 1:
                    return row
                else:
                    line_count += 1


    def get_primitive_duration(self, pipeline_run_file, output_home):
        temp_file_handle = open("%s/temp.yml" % output_home, "w+")
        lines = [line.rstrip('\n') for line in open(pipeline_run_file)]
        start_writing = False
        for line in lines:
            if not start_writing:
                if line == "---":
                    start_writing = True
                    continue
            else:
                temp_file_handle.write("%s\n" % line)
        temp_file_handle.flush()
        temp_file_handle.close()

        with open("%s/temp.yml" % output_home, 'r') as stream:
            try:
                pipeline_run_data = yaml.safe_load(stream)

                # Ideally we would like to use the digest of the target primitive to find this in the pipeline run but
                # the digests do not match for some reason (i.e they are differenct between the pipeline and the pipeline run)
                start = parser.parse(pipeline_run_data['steps'][7]['start'])
                end =  parser.parse(pipeline_run_data['steps'][7]['end'])

                duration = end - start
                return duration.microseconds

            except yaml.YAMLError as exc:
                _logger.info("Could not open pipeline run file %s" % pipeline_run_file)
        return "Error determining duration"


    def check_score_file(self, score_file):
        try:
            if os.path.getsize(score_file) > 0:
                return True
            else:
                # Empty score file - execution probably failed
                return False
        except OSError as e:
            # File does not exist
            return False


'''
Entry point - required to make python happy
'''
if __name__ == "__main__":
    # Grab the command line parameters
    TriageD3MPrimitives().main()
