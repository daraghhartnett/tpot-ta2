import logging
import sys
import os
import subprocess
import json

# init logger
logging.basicConfig(level=logging.INFO, format='%(asctime)s [%(levelname)s] %(name)s -- %(message)s')
_logger = logging.getLogger(__name__)

'''
This script allows us to quickly run a fit-score on an existing pipeline.

This script relies on 3 environment variables for each mode:

    # Location of the seed datasets
    D3M_Dataset_Home="/Users/daraghhartnett/Projects/D3M/evals/061118/datasets/seed_datasets_current"
    # Where the output data should be written
    D3M_Output_Home="/Users/daraghhartnett/Projects/D3M/evals/061118/tpot-ta2/data/outputs"
    # What docker image to run
    D3M_Docker_Image="registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:latest"

This script also requires 4 command line parameters:

    run (if false, just print out the formulated run command, if true, also run the command)
    problemName (185_baseball for example, it is important that this be the name of the dat directory in the seed data)
    rank (from 1 to n - which of the generated pipelines should be executed?)

An example run is follows:

    python run_test.py False 185_baseball 1
    
'''

class RunTest(object):

    def __init__(self, **args):
        self._add_attrs(args, 'problemName', 'rank', 'mode')


    def _add_attrs(self, args, *attrs):
        for attr in attrs:
            if attr in args:
                setattr(self, attr, args[attr])

    def main(self):
        _logger.info("Run Test: Configuration manager")

        # Grab the environment parameters
        datasetHome = os.environ['D3M_Dataset_Home']
        outputHome = os.environ['D3M_Output_Home']
        dockerImage = os.environ['D3M_Docker_Image']

        # Generate the run command
        command = self.generate_command(datasetHome, dockerImage, outputHome)
        if command is None:
            raise Exception("Unable to run pipeline specified")

        _logger.info("Command Line:")
        _logger.info(command)

        # Launch the command
        ret_stdout = subprocess.check_output(command, shell=True)
        _logger.info("Command completed")


    '''
    This method finds the pipeline with the specified rank for this problem
    '''
    def getPipelineByRank(self, outputHome):
        # First get the pipeline scripts most recently created in case we have multiple runs present:
        import glob
        import os

        if self.mode == "ta2ta3":
            search_id = self.get_search_id_directory(outputHome, self.problemName)
            files_path = os.path.join(outputHome + "/" + self.problemName + "/" + search_id + "/pipelines_ranked/", '*.rank')
        else:
            files_path = os.path.join(outputHome + "/" + self.problemName + "/pipelines_ranked/", '*.rank')

        files = sorted(glob.iglob(files_path), key=os.path.getctime, reverse=True)

        for file in files:
            with open(file) as f:
                rank = f.read().replace('\n', '')
                if int(rank) == int(self.rank):
                    index = file.rfind("/") + 1
                    pipelineName = file[index:-5]
                    _logger.info("Pipeline with rank " + str(self.rank) + " is " + pipelineName)
                    return pipelineName
        _logger.error("No pipeline with rank " + str(self.rank) + " found for problem " + self.problemName)
        return None


    def get_search_id_directory(self, output_home, problem):
        # In TA3 Mode there is now a search id directory under the problem - we must find out what this is so we can
        # find the results and process them
        path = "%s/%s" % (output_home, problem)
        search_id = next(os.walk(path))[1]
        # The evaluation contract requires us to have a temp folder in the problem name directory
        search_id.remove("temp")
        if len(search_id) > 1:
            logging.warning("Old runs are being kept by CI! Manually clear the output!")
        return search_id[0]


    '''
    This method builds the a command that calls fit-score on a pipeline using the reference runtime.
          
    For fit-score:
      docker run 
        -i --rm  
        -v /datasets/test/185_baseball:/input/ 
        -v /Users/daraghhartnett/Projects/D3M/evals/012119/tpot-ta2/data/output/185_baseball:/output/ 
        registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:2019.2.27-devel2 
        python3 -m d3m runtime fit-score 
          -p /output/pipelines_ranked/AWOGCBE3LU.json 
          -n /input/UCBerkeley_scoring.yml 
          -r /input/185_baseball_problem/problemDoc.json 
          -i /input/TRAIN/dataset_TRAIN/datasetDoc.json 
          -t /input/TEST/dataset_TEST/datasetDoc.json 
          -a /input/TEST/dataset_TEST/datasetDoc.json
    '''
    def generate_command(self, datasetHome, dockerImage, outputHome):
        pipeline_name = self.getPipelineByRank(outputHome)
        if pipeline_name is None:
            # Pipeline probably failed during search
            return None
        pipeline = "%s/%s.json" % ("/output/pipelines_ranked", pipeline_name)
        scoringPath = "/D3M/scoring/"

        problem_doc = "%s/%s_problem/problemDoc.json" % ("/input", self.problemName)
        train_dataset_doc = "%s/TRAIN/dataset_TRAIN/datasetDoc.json" % "/input"
        test_dataset_doc = "%s/TEST/dataset_TEST/datasetDoc.json" % "/input"
        path_to_score_file = "/SCORE/dataset_TEST/datasetDoc.json"
        scoring_dataset_doc = "%s%s" % ("/input", path_to_score_file)

        problemDataPath = datasetHome + "/" + self.problemName
        if not os.path.exists("%s%s" % (problemDataPath, path_to_score_file)):
            scoring_dataset_doc = "%s/SCORE/dataset_SCORE/datasetDoc.json" % "/input"

        predictions_file = "%s/%s/predictions.csv" % ("/output/predictions", pipeline_name)
        pipeline_run = "%s/%s/%s" % ("/output/pipeline_runs", pipeline_name, "pipeline_run.yml")
        scores_file = "%s/%s/%s" % ("/output/scores", pipeline_name, "scores.csv")

        # Build the components of the command line
        if self.mode == "ta2ta3":
            search_id = self.get_search_id_directory(outputHome, self.problemName)
            outputDataPath = "%s/%s/%s" % (outputHome, self.problemName, search_id)
        else:
            outputDataPath = "%s/%s" % (outputHome, self.problemName)

        # TODO: Purge the scoring stuff as it is no longer needed
        scoringMount = " -v " + scoringPath + ":/scoring/"
        inputMount = " -v " + problemDataPath + ":/input/"
        outputMount = " -v " + outputDataPath + ":/output/"
        staticVolumeMount = " -v /D3M/static/:/volumes/"

        # Create the output predictions path
        predictions_path = "%s/predictions/%s/" % (outputDataPath, pipeline_name)
        logging.info("Trying to create predictions_path dir: %s" % predictions_path)
        if not os.path.exists(predictions_path):
            logging.info("Not already there...")
            os.makedirs(predictions_path)
        pipeline_run_path = "%s/pipeline_runs/%s/" % (outputDataPath, pipeline_name)
        logging.info("Trying to create pipeline_run_path dir: %s" % pipeline_run_path)
        if not os.path.exists(pipeline_run_path):
            logging.info("Not already there...")
            os.makedirs(pipeline_run_path)
        scores_path = "%s/scores/%s/" % (outputDataPath, pipeline_name)
        logging.info("Trying to create scores_path dir: %s" % scores_path)
        if not os.path.exists(scores_path):
            logging.info("Not already there...")
            os.makedirs(scores_path)

        command = "python3 -m d3m runtime -v /volumes fit-score -p %s -r %s -i %s -t %s -a %s -o %s -O %s -c %s" % \
                  (pipeline, problem_doc, train_dataset_doc, test_dataset_doc, scoring_dataset_doc,
                   predictions_file, pipeline_run, scores_file)

        # Current Working Datamart URLs, as of Oct 15th 2019
        # "--env DATAMART_URL_NYU=https://auctus.vida-nyu.org" + \
        # "--env DATAMART_URL_NYU=http://dsbox02.isi.edu:9000" + \

        # Assemble the full command
        testCmd = "docker run -i --rm --env DATAMART_URL_NYU=https://auctus.vida-nyu.org " + \
                    inputMount + \
                    outputMount + \
                    scoringMount + \
                    staticVolumeMount + \
                    " " + dockerImage + \
                    " " + command

        return testCmd


'''
Entry point - required to make python happy
'''
if __name__ == "__main__":
    # Grab the command line parameters
    problemName = sys.argv[1]
    rank = sys.argv[2]
    mode = sys.argv[3]
    RunTest(problemName=problemName, rank=rank, mode=mode).main()
