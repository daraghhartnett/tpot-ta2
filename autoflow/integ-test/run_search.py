import logging
import sys
import os
import subprocess
import json
import plac
import time

# init logger
logging.basicConfig(level=logging.INFO, format='%(asctime)s [%(levelname)s] %(name)s -- %(message)s')
_logger = logging.getLogger(__name__)

'''
This script find pipelines for a particular problem.

This script relies on 5 environment variables for each mode:

    # Location of the seed datasets
    D3M_Dataset_Home="/Users/daraghhartnett/Projects/D3M/evals/061118/datasets/seed_datasets_current"
    # Where the output data should be written
    D3M_Output_Home="/Users/daraghhartnett/Projects/D3M/evals/061118/tpot-ta2/data/outputs"
    # Location of the config files (embedded mode will use an existing config, local mode will generate one here)
    D3M_Config_Home="/Users/daraghhartnett/Projects/D3M/evals/061118/tpot-ta2/config"
    # What docker image to run in embedded mode
    D3M_Docker_Image="registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:latest"

This script also requires 5 command line parameters:

    run (if false, just print out the formulated run command, if true, also run the command)
    problemName (185_baseball for example, it is important that this be the name of the dat directory in the seed data)
    runMode (ta2 for simple search or ta2ta3 to start the ta2 in server mode and respond to ta3 requests) 
    debug (if true, add parameters to make search run faster)
    configfile name (the name of the config file to use from the config directory)

An example run for each of ta2 and ta2ta3 search modes are as follows:

    python run_search.py False 185_baseball ta2 search_config_tpot_local.json
    python run_search.py False 185_baseball ta2ta3 search_config_gama_docker.json
    
'''
class RunSearch(object):

    def __init__(self, **args):
        self._add_attrs(args, 'problemName', 'runMode', 'debugMode', 'configfile', 'thread_id')
        # Use an obscure docker container name to reduce the possibility of a name clash
        self.secret_docker_name = "digbymuggins"


    def _add_attrs(self, args, *attrs):
        for attr in attrs:
            if attr in args:
                setattr(self, attr, args[attr])


    def main(self):
        _logger.info("Run Search: Configuration manager")

        # Grab the environment parameters
        datasetHome = os.environ['D3M_Dataset_Home']
        configHome = os.environ['D3M_Config_Home']
        outputHome = os.environ['D3M_Output_Home']
        dockerImage = os.environ['D3M_Docker_Image']
        if self.runMode == "ta2ta3":
            self.timeout = os.environ['TIMEOUT']
            self.cpus = os.environ['D3MCPU']

        # Make sure dirs are there (for local testing)
        if not os.path.exists(outputHome + "/" + self.problemName + "/pipelines_ranked"):
            os.makedirs(outputHome + "/" + self.problemName + "/pipelines_ranked", exist_ok=True)
        if not os.path.exists(outputHome + "/" + self.problemName + "/pipelines_scored"):
            os.makedirs(outputHome + "/" + self.problemName + "/pipelines_scored", exist_ok=True)
        if not os.path.exists(outputHome + "/" + self.problemName + "/pipelines_searched"):
            os.makedirs(outputHome + "/" + self.problemName + "/pipelines_searched", exist_ok=True)
        if not os.path.exists(outputHome + "/" + self.problemName + "/subpipelines"):
            os.makedirs(outputHome + "/" + self.problemName + "/subpipelines", exist_ok=True)
        if not os.path.exists(outputHome + "/" + self.problemName + "/pipeline_runs"):
            os.makedirs(outputHome + "/" + self.problemName + "/pipeline_runs", exist_ok=True)
        if not os.path.exists(outputHome + "/" + self.problemName + "/additional_inputs"):
            os.makedirs(outputHome + "/" + self.problemName + "/additional_inputs", exist_ok=True)
        if not os.path.exists(outputHome + "/" + self.problemName + "/temp"):
            os.makedirs(outputHome + "/" + self.problemName + "/temp", exist_ok=True)

        if (self.runMode == "ta2"):
            # Embedded mode is when you are calling search using docker run. This is how NIST will call our TA2 system
            command = self.runTa2(configHome, datasetHome, dockerImage, outputHome)
            _logger.info("TA2 Search Command Line: %s" % command)
            # Enable conda Environment (jan2019 is an example of a conda env name to activate):
            # environment = "source activate jan2019 && "
            environment = ""
            timeout = self.get_timeout_from_config_in_seconds("%s/%s" % (configHome, self.configfile))
            # TODO: NOTE!!! If there are any further evaluations we should make sure Autoflow strictly adheres to these
            #  search time constraints. It does not look like there will be but lets keep it in mind.
            # Add a 5 minutes grace period so we dont kill a search that is wrapping up.
            timeout += 300
            process = subprocess.Popen(environment + command, shell=True)
            try:
                logging.info("Launching search with timeout = %s seconds" % str(timeout))
                out, err = process.communicate(timeout=timeout)
            except subprocess.TimeoutExpired:
                logging.info("Maximum search time has been reached for %s, stopping search" % self.problemName)
                # We hit the maximum search time, ask Autoflow to wrap up its search
                container_name = self.secret_docker_name + "_" + str(self.thread_id)
                ret_stdout = subprocess.check_output("docker stop %s" % container_name, shell=True)
                _logger.info("Autoflow has been stopped for for %s." % self.problemName)
        elif (self.runMode == "ta2ta3"):
            try:
                # ta2ta3 mode is when you want to use our custom ta3 client code to call our ta2 in server mode.
                command = self.runTa2Server(configHome, datasetHome, dockerImage, outputHome)
                _logger.info("TA2 Server Command Line: %s" % command)
                # Spawn off a process to start the ta2 server.
                os.system(command)

                # Sleep for 20 seconds to allow the ta2 server to start
                time.sleep(20)
                _logger.info("TA2 Server up and running... Starting TA3 Client")

                # Run the TA3 client in another docker instance
                command = self.runTa3Client(configHome, datasetHome, dockerImage, outputHome, self.problemName)
                _logger.info("TA3 Client Command Line: %s" % command)
                ret_stdout = subprocess.check_output(command, shell=True)
                _logger.info("TA3 Client Completed")
            finally:
                # Now shut down the TA2 server
                container_name = self.secret_docker_name + "_" + str(self.thread_id)
                ret_stdout = subprocess.check_output("docker stop %s" % container_name, shell=True)
                _logger.info("TA2 Server has been stopped.")
        else:
            _logger.error("Run mode must be specified as 'ta2 or 'ta2ta3'")
            return


    def get_timeout_from_config_in_seconds(self, config_file_path):
        config_data = json.load(open(config_file_path))
        minutes = config_data['timeout']
        logging.info("Timeout detected as %s minutes" % minutes)
        return int(minutes) * 60


    '''
    This method prepares a ta2 server command for the ta3 client to send requests to
    '''

    def runTa2Server(self, configHome, datasetHome, dockerImage, outputHome):
        pipeline_count = 1
        if 'D3M_Pipeline_Count' in os.environ:
            pipeline_count = os.getenv("D3M_Pipeline_Count")
            logging.info("Overriding default pipeline count to %s" % pipeline_count)

        # Build the components of the command line
        problemDataPath = "%s/" % (datasetHome)
        outputDataPath = "%s/%s/" % (outputHome, self.problemName)
        configMount = " -v " + configHome + ":/home/code/"
        inputMount = " -v " + problemDataPath + ":/inputs/"
        outputMount = " -v " + outputDataPath + ":/output/"
        staticVolumeMount = " -v /D3M/static/:/volumes/"
        container_name = self.secret_docker_name + "_" + str(self.thread_id)
        exposed_port = 45042 + int(self.thread_id)

        # Assemble the full command
        ta2ServerCommand = "docker run " \
                           "--name " + container_name + \
                           " -i --rm --net=d3mnetwork -p " + str(exposed_port) + ":45042 " \
                           "--env D3MOUTPUTDIR=/output/ " \
                           "--env D3MTIMEOUT=" + self.timeout + " " + \
                           "--env D3MCPU=" + self.cpus + " " + \
                           "--env D3MSTATICDIR=/volumes " + \
                           "--env D3M_Pipeline_Count=" + pipeline_count + " --entrypoint /bin/bash" + \
                           configMount + \
                           inputMount + \
                           outputMount + \
                           staticVolumeMount + \
                           " --entrypoint=ta2_server" + \
                           " " + dockerImage + \
                           " GAMA &"

        return ta2ServerCommand


    '''
    This method prepares a TA3 client command to send requests to the TA2 Server
    '''
    def runTa3Client(self, configHome, datasetHome, dockerImage, outputHome, problem):
        #     ret_stdout = subprocess.check_output(
        #         "python3 ta2-ta3/sri_ta3.py -p /%s/%s_problem/problemDoc.json -d /%s/%s_dataset/datasetDoc.json -e localhost -t 600 -l %s -r /inputs" %
        #         (self.problemName, self.problemName, self.problemName, self.problemName, datasetHome), shell=True)

        # Build the components of the command line
        problemDataPath = "%s/" % (datasetHome)
        outputDataPath = "%s/%s/" % (outputHome, self.problemName)
        inputMount = " -v " + problemDataPath + ":/inputs/"
        outputMount = " -v " + outputDataPath + ":/output/"
        container_name = self.secret_docker_name + "_" + str(self.thread_id)

        # Assemble the full command
        ta3_client_command = "docker run " \
                    " -i --rm --net=d3mnetwork " \
                    "--entrypoint /bin/bash" + \
                    inputMount + \
                    outputMount + \
                    " --entrypoint=ta3_client" + \
                    " " + dockerImage + \
                    " -p /inputs/" + problem + "/" + problem + "_problem/problemDoc.json " + \
                    " -d /inputs/" + problem + "/" + problem + "_dataset/datasetDoc.json " + \
                    " -e " + container_name + \
                    " -t " + self.timeout

        return ta3_client_command


    '''
    This method prepares a ta2 search command which does not need a ta3 to find pipelines
    '''
    def runTa2(self, configHome, datasetHome, dockerImage, outputHome):
        # Build the components of the command line
        problemDataPath = datasetHome + "/"
        outputDataPath = outputHome
        configMount = " -v " + configHome + ":/home/code/"
        inputMount = " -v " + problemDataPath + ":/inputs/"
        outputMount = " -v " + outputDataPath + ":/output/"
        staticVolumeMount = " -v /D3M/static/:/volumes/"

        score_test_data = " "
        if 'USETESTDATASET' in os.environ:
            score_test_data = "--env USETESTDATASET=True "

        pipeline_count = 1
        if 'D3M_Pipeline_Count' in os.environ:
            pipeline_count = os.getenv("D3M_Pipeline_Count")
            logging.info("Overriding default pipeline count to %s" % pipeline_count)

        import socket
        _logger.info("Host machine is: %s " % socket.gethostname())
        gpu_option = ""
        if "Bolero" in socket.gethostname():
            gpu_option = "--gpus all --shm-size=1g"

        # Current Working Datamart URLs, as of Oct 15th 2019
        # "--env DATAMART_URL_NYU=https://auctus.vida-nyu.org" + \
        # "--env DATAMART_URL_NYU=http://dsbox02.isi.edu:9000" + \
        # We create a unique container name so we can ask the process to stop if it exceeds the time limit.
        container_name = self.secret_docker_name + "_" + str(self.thread_id)

        # Assemble the full command
        # searchCmd = "docker run --gpus all --shm-size=1g -i --rm " \
        searchCmd = "docker run " \
                    "--name " + container_name + \
                    " -i " + gpu_option + " --rm " \
                    "--env D3M_Pipeline_Count=" + pipeline_count + \
                    " --env D3MSTATICDIR=/volumes " + \
                    "--env USEWHITELIST=True " + \
                    "--env DATAMART_URL_NYU=https://auctus.vida-nyu.org" + \
                    score_test_data + \
                    "--entrypoint /bin/bash" + \
                    configMount + \
                    inputMount + \
                    outputMount + \
                    staticVolumeMount + \
                    " --entrypoint=ta2_search" + \
                    " " + dockerImage + \
                    " " + self.problemName + " /home/code/" + self.configfile

        return searchCmd


'''
Entry point - required to make python happy
'''
if __name__ == "__main__":

    def main(problemName: "Name of D3M problem",
             runMode: "'ta2' or 'ta2ta3'",
             debugMode: "'true' or 'false'",
             configfile: "Name of file to write",
             thread_id: "id to allow coordination of ta2/ta3 pairs in multi process mode"):
        debugMode = debugMode.lower() == "true"
        RunSearch(problemName=problemName, runMode=runMode, debugMode=debugMode, configfile=configfile,
                  thread_id=thread_id).main()

    plac.call(main)

