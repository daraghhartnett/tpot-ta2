import sys
from d3m_outputs import Predictions
from d3m_outputs import pipeline_logs_validator

output_root = sys.argv[1]
pipeline_name = sys.argv[2]
score_root = sys.argv[3]

prediction_file = "%s/predictions/%s/predictions.csv" % (output_root, pipeline_name)
pipeline_uri = "%s/pipelines/%s.json" % (output_root, pipeline_name)
ground_truth_path = "%s/targets.csv" % score_root

prediction = Predictions(prediction_file, score_root)
prediction_is_valid = prediction.is_valid()
scores = prediction.score(ground_truth_path)
pipeline_is_valid = pipeline_logs_validator.is_pipeline_valid(pipeline_uri=pipeline_uri)

print("Prediction file is valid = %s" % str(prediction_is_valid))
print("Metric = %s" % scores.scores[0].metric)
print("Score = %s" % scores.scores[0].scorevalue)
print("Pipeline is valid = %s" % pipeline_is_valid)
