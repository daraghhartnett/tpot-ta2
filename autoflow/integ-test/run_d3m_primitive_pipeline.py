import logging
import sys
import os
import subprocess

_logger = logging.getLogger(__name__)

'''
This is adapted from run_d3m_primitive_pipelines.py - we broke it out to make it run just one pipeline
with the intention of running these tasks in parallel to speed things up!
'''

class RunD3MPrimitivePipeline(object):

    def __init__(self, **args):
        self._add_attrs(args, 'pipeline_name', 'problem')

    def _add_attrs(self, args, *attrs):
        for attr in attrs:
            if attr in args:
                setattr(self, attr, args[attr])

    def main(self):
        pipeline = self.pipeline_name

        # The docker image always has the following mounted
        # /input (seed dataset to run on)
        # /output (where the output files should be written to)
        # /scoring (where the pipeline scoring file can be found)

        problem_doc = "%s/%s/%s_problem/problemDoc.json" % ("/input", self.problem, self.problem)
        train_dataset_doc = "%s/%s/TRAIN/dataset_TRAIN/datasetDoc.json" % ("/input", self.problem)
        test_dataset_doc = "%s/%s/TEST/dataset_TEST/datasetDoc.json" % ("/input", self.problem)
        path_to_score_file = "/SCORE/dataset_TEST/datasetDoc.json"
        scoring_dataset_doc = "%s/%s%s" % ("/input", self.problem, path_to_score_file)

        problemDataPath = "/input" + "/" + self.problem
        if not os.path.exists("%s%s" % (problemDataPath, path_to_score_file)):
            scoring_dataset_doc = "%s/%s/SCORE/dataset_SCORE/datasetDoc.json" % ("/input", self.problem)

            _logger.info("Running Pipeline: %s on problem %s" % (pipeline, self.problem))

        pipeline_file = "%s/%s.json" % ("/output", pipeline)

        predictions_file = "%s/%s-%s-predictions.csv" % ("/output", pipeline, self.problem)
        pipeline_run = "%s/%s-%s-pipeline_run.yml" % ("/output", pipeline, self.problem)
        scores_file = "%s/%s-%s-scores.csv" % ("/output", pipeline, self.problem)

        command = "python3 -m d3m runtime fit-score -p %s -r %s -i %s -t %s -a %s -o %s -O %s -c %s" % \
              (pipeline_file, problem_doc, train_dataset_doc, test_dataset_doc, scoring_dataset_doc,
               predictions_file, pipeline_run, scores_file)

        try:
            subprocess.check_output(command, shell=True)
            _logger.info("Pipeline %s executed successfully" % pipeline)
        except Exception as e:
            _logger.info("Pipeline %s failed during execution" % pipeline)
            _logger.warning(e)

        # Used to generate the pipeline describe file
        # command = "python3 -m d3m pipeline describe %s > %s/%s.pr.desc" % (pipeline_file, "/output", pipeline)
        # _logger.info("Describe command is: %s" % command)
        # try:
        #     subprocess.check_output(command, shell=True)
        # except Exception as e:
        #     _logger.info("Pipeline %s failed to generate description" % pipeline)
        #     _logger.warning(e)


'''
Entry point - required to make python happy
'''
if __name__ == "__main__":
    # Grab the command line parameters
    pipeline_name = sys.argv[1]
    problem = sys.argv[2]
    RunD3MPrimitivePipeline(pipeline_name=pipeline_name, problem=problem).main()
