import logging
import sys
import os
import shutil
from run_search import RunSearch
from run_test import RunTest
from datetime import datetime
import json
import concurrent.futures
import threading
from itertools import repeat
import plac


# init logger
logging.basicConfig(level=logging.INFO, format='%(asctime)s [%(levelname)s] %(name)s -- %(message)s')
_logger = logging.getLogger(__name__)

'''
This script allows us to run search, test and validation across all seed datasets. See the sub scripts 
run_search and run_test for required environment variables that must be set in order for this script to 
work properly. 

USAGE:
    search-test-score-all.py FILESTEM MODE [SUPPRESS...]

FILESTEM is partial file name.  We suggest using YYYY.MM.DD format for the file so you can keep track 
  of the run date. The order of problems will be the same which should allow us to run
  diff to see differences. 

The script outputs two files based on FILESTEM:
    FILESTEM.csv:  The problem name, validity, metric and scores for each problem will be written to 
                     this file in CSV format.
    FILESTEM.jra : Same as the above but formatted to be dropped directly into Jira - see 
                     https://jira.sri.com/browse/D3M-66

MODE how the tests should be run:
    ta2 = stand alone pipeline search
    ta2ta3 = start the ta2 as a server and wait for requests from the ta3 client code.

If any SUPPRESS arguments are provided, the system will skip any datasets matching any of the supplied
patterns using simple string containment match.
                
An example run is as follows:

   python integ-test/search-test-score-all.py ../seed-datasets-results/2018.06.30a ta2ta3
    
'''
class SearchTestScoreAll(object):

    def __init__(self, **args):
        self._add_attrs(args, 'output_template', 'mode', 'threads', 'suppress')

        # Create a lock for managing access to the problem result map
        self._lock = threading.Lock()

        self.red = "{color:#DE350B}"
        self.green = "{color:#00875A}"
        self.ran_clean = 0
        self.search_failed = 0
        self.all_failed = 0
        self.no_pipelines = 0
        self.problem_result_map = {}
        self.gitlab_result_map = {}
        self.problem_start_times = {}


    def _add_attrs(self, args, *attrs):
        for attr in attrs:
            if attr in args:
                setattr(self, attr, args[attr])


    def main(self):
        # This is a hard coded list of dataset matches that we want to avoid running
        #skip = ['fudan', 'Geolife', 'margin', 'hyperparameter']
        skip = []
        keep = None

        # The file stem to use when generating the results file
        stem = self.output_template
        jira_table_file = "%s.jra" % stem
        gitlab_table_file = "%s.git" % stem

        # If 'keep' is the first entry - just run with these problems - otherwise exclude the specified problems
        adding_to_keep = False
        if self.suppress is not None:
            patterns = self.suppress.split(":")
            for pattern in patterns:
                pattern = pattern.strip()
                pattern = pattern.replace("\\", "")
                if pattern == 'keep':
                    adding_to_keep = True
                    keep = []
                    continue
                if adding_to_keep:
                    keep.append(pattern)
                else:
                    skip.append(pattern)

        # Get an array of all the problem names
        dataset_home = os.environ['D3M_Dataset_Home']
        output_home = os.environ['D3M_Output_Home']

        _logger.info("D3M_Dataset_Home: %s" % dataset_home)
        _logger.info("D3M_Output_Home: %s" % output_home)

        problem_list = sorted(self.get_problem_list(dataset_home))
        if keep is not None:
            problem_list = [p for p in problem_list
                            if any(pat for pat in keep if pat in p)]
        problem_list = [p for p in problem_list
                        if not any(pat for pat in skip if pat in p)]

        _logger.info("Testing the following problems: %s" % problem_list)

        # Purge the current contents in the output directory so we only have the latest pipelines in there.
        for the_file in os.listdir(output_home):
            file_path = os.path.join(output_home, the_file)
            try:
                if os.path.isfile(file_path):
                    os.unlink(file_path)
                elif os.path.isdir(file_path): shutil.rmtree(file_path)
            except Exception as e:
                _logger.warning(e)

        # Create and open the run summary file
        with open(jira_table_file, 'w') as jira_file, open(gitlab_table_file, 'w') as gitlab_file:
            jira_file.write("||Problem|Type|Duration|P Valid?|Metric|Score|P Failed|Jira||\n")
            jira_file.flush()
            gitlab_file.write("|Problem|Type|Duration|P Valid?|Metric|Score|P Failed|Fragment|\n")
            gitlab_file.write("|-------|----|--------|--------|------|-----|--------|--------|\n")
            gitlab_file.flush()

            _logger.info("Run Search - Test - Score in %s mode" % self.mode)

            # For each problem, perform search
            thread_count = self.threads

            _logger.info("Total of %s problems will be searched" % len(problem_list))

            # Debugging note: If for some reason this method does not seem to be getting called - scrutinize the
            # parameters to the method - one is probably missing or of the wrong type, causing the method not to
            # be found - this will happily ignore the call and not result in (what would be) a helpful error!
            with concurrent.futures.ThreadPoolExecutor(max_workers=thread_count) as executor:
                executor.map(self.search_thread, problem_list, repeat(dataset_home), range(len(problem_list)))

            # For each problem, that did not fail, get the ranked pipelines and run them
            with concurrent.futures.ThreadPoolExecutor(max_workers=thread_count) as executor:
                executor.map(self.run_thread, repeat(output_home), problem_list, repeat(dataset_home))

            # Use the problem name map to output the problems in the correct order
            missing = 0
            for problemstatus in problem_list:
                if problemstatus not in self.problem_result_map:
                    missing += 1
                    _logger.warning("Warning: The problem %s does not appear in the problem result map" % problemstatus)
                else:
                    jira_file.write(self.problem_result_map[problemstatus])
                    jira_file.flush()
                    gitlab_file.write(self.gitlab_result_map[problemstatus])
                    gitlab_file.flush()

            debug_line = ("  %s Problems missing from status count...\n" % missing)

            total_line = ("  %s Total Problems run\n" % len(problem_list))
            succeeded_line = ("  %s completed with at least 1 good pipelines\n" % self.ran_clean)
            search_failed_line = ("  %s search failure\n" % self.search_failed)
            no_pipelines_line = ("  %s found no pipelines\n" % self.no_pipelines)
            all_failed_line = ("  %s all pipelines failed to run\n" % self.all_failed)
            jira_file.write("\nSummary:\n")
            jira_file.write(total_line)
            # Only print this if an error is detected
            if missing > 0:
                jira_file.write(debug_line)
            jira_file.write(succeeded_line)
            jira_file.write(search_failed_line)
            jira_file.write(no_pipelines_line)
            jira_file.write(all_failed_line)
            jira_file.flush()
            jira_file.close()

            gitlab_file.write("\nSummary:\n")
            gitlab_file.write("- %s" % total_line)
            # Only print this if an error is detected
            if missing > 0:
                gitlab_file.write("- %s" % debug_line)
            gitlab_file.write("- %s" % succeeded_line)
            gitlab_file.write("- %s" % search_failed_line)
            gitlab_file.write("- %s" % no_pipelines_line)
            gitlab_file.write("- %s" % all_failed_line)
            gitlab_file.flush()
            gitlab_file.close()


    def search_thread(self, problem, dataset_home, thread_id):
        start_time = datetime.now()
        self.set_start_time(problem, start_time)
        try:
            task_type = self.havest_task_type(problem=problem, dataset_home=dataset_home)
            _logger.info("Running search on: %s, type: %s, thread_id: %s" % (problem, task_type, thread_id))
            search = RunSearch(run=True, problemName=problem, runMode=self.mode, configfile="search_config_gama_docker.json", thread_id=thread_id)
            search.main()
        except Exception as e:
            _logger.error("Search failed on problem %s. Reporting to result map. Exception: %s" % (problem, e))
            duration = self.get_duration(start_time=start_time)
            self.update_result_map("search_failed", problem, task_type, duration)
        _logger.info("%s complete" % problem)


    def run_thread(self, output_home, problem, dataset_home):
        # check to see if this problems status has already been reported
        if problem in self.problem_result_map:
            return

        if self.mode == "ta2ta3":
            search_id = self.get_search_id_directory(output_home, problem)
            path = "%s/%s/%s/pipelines_ranked" % (output_home, problem, search_id)
        else:
            path = "%s/%s/pipelines_ranked" % (output_home, problem)

        ranked_pipelines = self.get_ranked_pipelines_count(path)

        pipeline_test_completion_list = []
        pipeline_scores_list = []
        pipeline_fragment_list = []

        # Run all of the pipelines to make sure they are valid, but only save the score of the first ranked one.
        for pipeline_rank in range(0, ranked_pipelines):

            # Add boolean indicating success or failure for each pipeline
            _logger.info("Running test: %s" % problem)

            # Pipeline rank is a 1 based system
            rank = pipeline_rank + 1
            success = True
            try:
                test = RunTest(problemName=problem, rank=rank, mode=self.mode)
                test.main()
            except Exception as e:
                _logger.error("Test failed on problem %s, pipeline rank %s, exception: %s" % (problem, rank, e))

                success = False

            pipeline_test_completion_list.append(success)
            _logger.info("Test complete: %s" % problem)

            scores = []
            fragment = None
            if success:
                _logger.info("Running score on Problem: %s, Pipeline Rank: %s" % (problem, pipeline_rank + 1))
                scores, fragment = self.score_pipeline(problem, pipeline_rank + 1, output_home)
                _logger.info("Score complete on Problem: %s, Pipeline Rank: %s" % (problem, pipeline_rank + 1))
                if len(scores) > 0 and scores[1] is not "n/a":
                    _logger.info("Score for Problem: %s, Rank: %s = %s" % (problem, pipeline_rank + 1, scores[1]))
            pipeline_scores_list.append(scores)
            pipeline_fragment_list.append(fragment)


        task_type = self.havest_task_type(problem=problem, dataset_home=dataset_home)
        _logger.info("Number of pipeline_scores for %s is %s" % (problem, len(pipeline_scores_list)))
        # Search did not fail but no pipelines were created - this may be due to a timeout. (primitives taking too
        # long to load? Max timeout not long enough to find valid pipelines?)
        if len(pipeline_scores_list) == 0:
            _logger.warning("Search did not find pipeline for problem %s. Reporting to result map" % problem)
            start_time = self.get_start_time(problem)
            duration = self.get_duration(start_time=start_time)
            task_type = self.havest_task_type(problem=problem, dataset_home=dataset_home)
            self.update_result_map("no_pipelines", problem, task_type, duration)
            return

        primary_pipeline_valid = False
        at_least_one_working_pipeline = False
        bad_pipelines = 0
        highest_score = 0
        highest_score_rank = 0
        lowest_score = 0
        lowest_score_rank = 0
        best_working_index = -1
        top_pipeline_fragment = None

        for index in range(0, len(pipeline_scores_list)):
            _logger.info("Checking pipeline %s for %s" % (index, problem))
            if index is 0:
                if len(pipeline_scores_list[index]) > 0 and pipeline_scores_list[index][1] is not "n/a":
                    at_least_one_working_pipeline = True
                    primary_pipeline_valid = True
                    highest_score_rank = 1
                    highest_score = float(pipeline_scores_list[index][1])
                    lowest_score_rank = 1
                    lowest_score = float(pipeline_scores_list[index][1])
                    top_pipeline_fragment = pipeline_fragment_list[index]
            else:
                if len(pipeline_scores_list[index]) == 0:
                    bad_pipelines += 1
                elif pipeline_scores_list[index][1] is "n/a":
                    bad_pipelines += 1
                else:
                    at_least_one_working_pipeline = True
                    if float(pipeline_scores_list[index][1]) > highest_score:
                        best_working_index = index
                        highest_score_rank = index + 1
                        highest_score = float(pipeline_scores_list[index][1])
                        top_pipeline_fragment = pipeline_fragment_list[index]
                    if float(pipeline_scores_list[index][1]) < lowest_score:
                        lowest_score_rank = index + 1
                        lowest_score = float(pipeline_scores_list[index][1])

        _logger.info("primary_pipeline_valid?: %s for problem %s" % (primary_pipeline_valid, problem))

        # Make sure we have a valid score to work with before capturing score spread logs
        if primary_pipeline_valid:
            highest_ranked_score = float(pipeline_scores_list[0][1])
            if not (highest_ranked_score >= lowest_score and highest_ranked_score <= highest_score):
                _logger.warning("For Problem: %s: The rank 1 score %s is not the best available. Metric: %s:" %
                             (problem, highest_ranked_score, str(pipeline_scores_list[0][0])))
            else:
                _logger.info("For Problem: %s: The rank 1 score is %s. Metric: %s:" %
                             (problem, highest_ranked_score, str(pipeline_scores_list[0][0])))

            _logger.info("\tFor Problem: %s: Highest Score: %s, Rank: %s" % (problem, highest_score, highest_score_rank))
            _logger.info("\tFor Problem: %s: Lowest Score: %s, Rank: %s" % (problem, lowest_score, lowest_score_rank))
            metric = str(pipeline_scores_list[0][0])
            score = str(round(float(highest_score), 5))
            primary_pipeline_valid = "success"
        elif at_least_one_working_pipeline:
            # If any pipeline is successful DMC will treat this as success
            metric = str(pipeline_scores_list[best_working_index][0])
            score = str(round(float(pipeline_scores_list[best_working_index][1]), 5))
            top_pipeline_fragment = pipeline_fragment_list[best_working_index]
            primary_pipeline_valid = "success"
        else:
            primary_pipeline_valid = "scoring_failed"
            metric = "n/a"
            score = "n/a"

        start_time = self.get_start_time(problem)
        duration = self.get_duration(start_time=start_time)
        # Check if any pipeline ran successfully
        if at_least_one_working_pipeline:
            _logger.info("ran_clean %s. Reporting to result map" % problem)
            self.update_result_map("ran_clean", problem, task_type, duration)
        else:
            _logger.error("all_failed %s. Reporting to result map" % problem)
            self.update_result_map("all_failed", problem, task_type, duration)

        # Create the appropriate log entry
        _logger.info("primary_pipeline_valid?: %s: %s. Reporting to result map" % (primary_pipeline_valid, problem))
        self.update_result_map(primary_pipeline_valid, problem, task_type, duration, metric, score, bad_pipelines,
                               top_pipeline_fragment)


    def get_search_id_directory(self, output_home, problem):
        # In TA3 Mode there is now a search id directory under the problem - we must find out what this is so we can
        # find the results and process them
        path = "%s/%s" % (output_home, problem)
        search_id = next(os.walk(path))[1]
        # The evaluation contract requires us to have a temp folder in the problem name directory
        search_id.remove("temp")
        if len(search_id) > 1:
            logging.warning("Old runs are being kept by CI! Manually clear the output!")
        return search_id[0]


    def update_result_map(self, status, problem, task_type, duration, metric=None, score=None, bad_pipelines=None,
                          top_pipeline_fragment=None):
        with self._lock:
            if metric is not None:
                metric = self.get_metric_abbreviation(metric)
            if task_type is not None:
                task_type = self.get_task_abbreviation(task_type)
            _logger.info("update_result_map. Problem: %s. Status: %s" % (problem, status))
            if status is "search_failed":
                self.search_failed += 1
                self.problem_result_map[problem] = "|%s%s{color}|%s|%s|Search failed|n/a|n/a|n/a|n/a|\n" % (self.red, problem, task_type, duration)
                self.gitlab_result_map[problem] = "|<b>%s|<b>%s|<b>%s|<b>Search failed|<b>n/a|<b>n/a|<b>n/a|<b>n/a|\n" % (problem[0:10], task_type, duration)
            elif status is "no_pipelines":
                self.no_pipelines += 1
                self.problem_result_map[problem] = "|%s%s{color}|%s|%s|0 plines found|n/a|n/a|n/a|n/a|\n" % (self.red, problem, task_type, duration)
                self.gitlab_result_map[problem] = "|<b>%s|<b>%s|<b>%s|<b>0 plines found|<b>n/a|<b>n/a|<b>n/a|<b>n/a|\n" % (problem[0:10], task_type, duration)
            elif status is "all_failed":
                self.all_failed += 1
            elif status is "ran_clean":
                self.ran_clean += 1
            elif status is "scoring_failed":
                self.problem_result_map[problem] = "|%s%s{color}|%s|%s|%s|%s|%s|%s|%s|\n" % \
                     (self.red, problem, task_type, duration, "Scoring failed", metric, score, bad_pipelines, "n/a")
                self.gitlab_result_map[problem] = "|<b>%s|<b>%s|<b>%s|<b>%s|<b>%s|<b>%s|<b>%s|<b>%s|\n" % \
                     (problem[0:10], task_type, duration, "Scoring failed", metric, score, bad_pipelines, top_pipeline_fragment)
            elif status is "success":
                self.problem_result_map[problem] = "|%s|%s|%s|%s|%s|%s|%s|%s|\n" % \
                             (problem, task_type, duration, "True", metric, score,
                              str(bad_pipelines), "n/a")
                self.gitlab_result_map[problem] = "|%s|%s|%s|%s|%s|%s|%s|%s|\n" % \
                             (problem[0:10], task_type, duration, "True", metric, score,
                              str(bad_pipelines), top_pipeline_fragment)
            else:
                _logger.info("not sure what to do with status: %s" % status)


    def get_metric_abbreviation(self, metric):
        if metric == 'ACCURACY':
            return "acc"
        elif metric == 'F1_MACRO':
            return "f1_mac"
        elif metric == 'MEAN_SQUARED_ERROR':
            return "mse"
        elif metric == 'ROOT_MEAN_SQUARED_ERROR':
            return "rmse"
        elif metric == 'F1':
            return "f1"
        elif metric == 'MEAN_ABSOLUTE_ERROR':
            return "mae"
        elif metric == 'NORMALIZED_MUTUAL_INFORMATION':
            return "nmi"
        else:
            return metric


    def get_task_abbreviation(self, task_type):
        if task_type == 'classification':
            return "class"
        elif task_type == 'regression':
            return "regr"
        elif task_type == 'graphMatching':
            return "grp_mat"
        elif task_type == 'timeSeriesForecasting':
            return "ts_forc"
        elif task_type == 'linkPrediction':
            return "link_pred"
        elif task_type == 'timeSeriesClassification':
            return "ts_cla"
        elif task_type == 'communityDetection':
            return "comm_det"
        elif task_type == 'vertexClassification':
            return "vert_class"
        elif task_type == 'objectDetection':
            return "obj_det"
        elif task_type == 'semiSupervisedClassification':
            return "semi_sup"
        else:
            return task_type


    def set_start_time(self, problem, start_time):
        with self._lock:
            self.problem_start_times[problem] = start_time


    def get_start_time(self, problem):
        # This is a hack to prevent non search runs from failing.
        if problem not in self.problem_start_times:
            return datetime.now()
        return self.problem_start_times[problem]


    def get_duration(self, start_time):
        """
        Used to track how long each dataset takes to complete search, validate and score
        """
        minutes = round(((datetime.now() - start_time).total_seconds() / 60), 2)
        return minutes



    def havest_task_type(self, problem, dataset_home):
        """
        Used to retrieve the task type from the problemDoc.json
        """

        taskKeyword_taskType_set = {
            "classification",
            "regression",
            "linkPrediction",
            "vertexNomination",
            "vertexClassification",
            "graphClustering",
            "graphMatching",
            "collaborativeFiltering",
            "objectDetection",
            "communityDetection",
            "semiSupervised"
        }

        with self._lock:
            problemDoc = json.load(open('%s/%s/TRAIN/problem_TRAIN/problemDoc.json' % (dataset_home, problem)))
            taskKeywords = problemDoc["about"]["taskKeywords"]
            taskType = taskKeyword_taskType_set.intersection(taskKeywords)
            if 'timeSeries' in taskKeywords and 'forecasting' in taskKeywords:
                task_type = 'timeSeriesForecasting'
            elif 'timeSeries' in taskKeywords and 'classification' in taskKeywords:
                task_type = 'timeSeriesClassification'
            elif 'semiSupervised' in taskKeywords and 'classification' in taskKeywords:
                task_type = 'semiSupervisedClassification'
            elif len(taskType) == 1:
                task_type = taskType.pop()
            elif len(taskType) == 0:
                _logger.warning("Supported Task Type is not found in taskKeywords: %s from problemDoc" % taskKeywords)
                return None
            elif len(taskType) > 1:
                _logger.warning("More than one Supported Task Type Task Type found in taskKeywords: %s from problemDoc."
                                " Continuing with the first one detected" % taskKeywords)
                task_type = taskType.pop()
            else:
                return None
            _logger.info("Task Type selected is: %s" % task_type)
            return task_type


    '''
    Try to execute the pipeline at the specified rank
    '''
    def run_pipeline(self, problem, rank):
        # call test
        try:
            test = RunTest(run=True, problemName=problem, rank=rank, mode=self.mode)
            test.main()
            return True
        except Exception as e:
            _logger.error("Test failed on problem %s, pipeline rank %s, exception: %s" % (problem, rank, e))
            return False


    '''
    Try to score the output of executing a pipeline 
    '''
    def score_pipeline(self, problem, rank, output_home):
        with self._lock:
            # call score
            scores = []
            try:
                runtest = RunTest(run=True, problemName=problem, rank=rank, mode=self.mode)
                pipeline_name = runtest.getPipelineByRank(output_home)

                if self.mode =="ta2ta3":
                    search_id = self.get_search_id_directory(output_home, problem)
                    path = "%s/%s/%s" % (output_home, problem, search_id)
                else:
                    path = "%s/%s" % (output_home, problem)

                prediction_file = os.path.join("%s/predictions/%s/predictions.csv" % (path, pipeline_name))
                scores_file = os.path.join("%s/scores/%s/scores.csv" % (path, pipeline_name))

                pipeline_is_valid = os.stat(prediction_file).st_size > 0

                # _logger.info("Problem: %s" % problem)

                if (pipeline_is_valid):
                    scores = self.load_scores_data(scores_file)
                else:
                    scores[0] = "n/a"
                    scores[1] = "n/a"

                # Also load the fragment data if it is available since we now have the pipeline id:
                fragment = ""
                frag_path = "%s/temp/%s.frag" % (path, pipeline_name)
                if os.path.exists(frag_path):
                    with open(frag_path, "r") as fragment_file:
                        fragment = fragment_file.read()

                return scores, fragment
            except Exception as e:
                _logger.error("Evaluation failed on problem %s, exception: %s" % (problem, e))
                return scores


    '''
    Get the number of ranked pipelines this run produced
    '''
    def get_ranked_pipelines_count(self, pipelines_ranked_folder):
        count = 0
        for file in os.listdir(pipelines_ranked_folder):
            if file.endswith('.json'):
                count +=1
        return count


    '''
    Load the scores data from the scores file provided
    '''
    def load_scores_data(self, scores_file):
        import csv

        with open(scores_file) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            line_count = 0
            for row in csv_reader:
                if line_count == 1:
                    return row
                else:
                    line_count += 1


    '''
    Returns a list of problem names from the seed data set directory indicated in the environment variable
    '''
    def get_problem_list(self, dataset_home):
        problem_list = []

        exists = os.path.isdir(dataset_home)
        _logger.info("Dataset directory exists? %s" % exists)
        readable = os.access(dataset_home, os.R_OK)
        _logger.info("Dataset directory readable? %s" % readable)
        _logger.info("Dataset home is: %s" % dataset_home)
        # Note: if this is blowing up for you - make sure you are not looking for a local dir while running in a
        # docker interpreter (or vice versa)
        directories_list = next(os.walk(dataset_home))[1]
        for directory in directories_list:
            _logger.info("About to walk: %s/%s" % (dataset_home, directory))
            # Ensure this is an actual problem dir
            sub_directories_list = next(os.walk("%s/%s" % (dataset_home, directory)))[1]
            for sub_directory in sub_directories_list:
                if sub_directory == "SCORE":
                    # This is a legit problem dir
                    problem_list.append(directory)
                    break
        return problem_list


'''
Entry point - required to make python happy
'''
if __name__ == "__main__":

    def main(output_template: "file template to use for output summary",
             mode: "'ta2' or 'ta2ta3'",
             threads: "number of simultaneous threads to use for search and test",
             suppress: ("matching problem names to suppress", 'option')):
        threads = int(threads)
        SearchTestScoreAll(output_template=output_template, mode=mode, threads=threads, suppress=suppress).main()

    plac.call(main)
