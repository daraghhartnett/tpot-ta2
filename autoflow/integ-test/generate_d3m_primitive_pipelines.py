import sys
import os
import copy
import logging
import numpy
import json
import datetime
import traceback
import random
from pydoc import locate
CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(CURRENT_DIR))
sys.path.append(os.path.abspath('config'))
from autoflow.config.generate_config import GenerateConfig
from pipelines import find_pipeline_class
from d3m.metadata.pipeline import Pipeline, PrimitiveStep, PlaceholderStep
from d3m.metadata.base import ArgumentType, PrimitiveFamily
from d3m.primitives.data_transformation.construct_predictions import Common as ConstructPredictions
from d3m.metadata.base import Context as PipelineContext

logging.basicConfig(level=logging.INFO, format='%(asctime)s [%(levelname)s] %(name)s -- %(message)s')
_logger = logging.getLogger(__name__)

'''
This script tests out all d3m primitives to make sure they work as advertised. Any that fail this validation are 
excluded from consideration during search. This script is intended to be run offline any time there is a new 
primitives repo release.

Need to support:
    
    CLASSIFICATION
    REGRESSION
    FEATURE_SELECTION 
    DATA_PREPROCESSING 
    DATA_TRANFORMATION
    
Full List:

    CLASSIFICATION
    CLUSTERING
    COLLABORATIVE_FILTERING
    COMMUNITY_DETECTION
    DATA_AUGMENTATION
    DATA_CLEANING
    DATA_COMPRESSION
    DATA_GENERATION
    DATA_PREPROCESSING
    DATA_TRANSFORMATION
    DATA_VALIDATION
    DATA_WRANGLING
    DIGITAL_IMAGE_PROCESSING
    DIGITAL_SIGNAL_PROCESSING
    EVALUATION
    FEATURE_CONSTRUCTION
    FEATURE_EXTRACTION
    FEATURE_SELECTION
    GRAPH_CLUSTERING
    GRAPH_MATCHING
    LAYER
    LEARNER
    LINK_PREDICTION
    LOSS_FUNCTION
    METALEARNING
    NATURAL_LANGUAGE_PROCESSING
    NORMALIZATION
    OBJECT_DETECTION
    OPERATOR
    REGRESSION
    SEMISUPERVISED_CLASSIFICATION
    SEMISUPERVISED_REGRESSION
    SIMILARITY_MODELING
    TIME_SERIES_CLASSIFICATION
    TIME_SERIES_EMBEDDING
    TIME_SERIES_FORECASTING
    TIME_SERIES_SEGMENTATION
    VERTEX_CLASSIFICATION
    VERTEX_NOMINATION
    VIDEO_PROCESSING
    SCHEMA_DISCOVERY
    
'''
def main(argv):
    _logger.info("Validating the D3M primitives")

    # Args:
    # argv1 = output directory to write the generate the test pipelines to
    # argv2 = blacklist file name to write the badly behaved primitive names to
    # argv3 = Number of instances of a primitive to generate pipelines for

    output_dir = argv[1]
    blacklist_file = "%s/%s" % (output_dir, argv[2])
    config_file = argv[3]
    pipeline_instances = int(argv[4])

    # Get the primitives
    generate_config = GenerateConfig()
    d3m_primitives, d3m_primitive_strings = generate_config.load_d3m_primitives(blacklist_file)
    generate_config.write_config(config=d3m_primitive_strings, config_file="%s/%s" % (output_dir, config_file))

    # Get the families
    primitive_families = generate_config.parse_primitive_families(d3m_primitives)

    # Print some statistics
    _logger.info("Total primitive count: %s" % len(d3m_primitives))
    _logger.info("Total family count: %s" % len(primitive_families))
    for family in primitive_families:
        _logger.info("\tFamily: %s has %s primitives" % (family, len(primitive_families[family])))

    # Get the single table pipeline as a template
    preamble_pipeline = get_preamble_pipeline()

    # Open handle to the blacklist file so we can document failure to add primitives to pipeline template
    blacklist_file_handle = open(blacklist_file, "a+")

    exception_list = ['d3m.primitives.data_transformation.construct_predictions.Common',
                      'd3m.primitives.data_transformation.horizontal_concat.DataFrameCommon',
                      'd3m.primitives.classification.vertex_nomination.VertexClassification' # will need special handling
                      ]

    for primitive in list(d3m_primitives):
        # Dont add primitives such as Construct Predictions to the battery
        if primitive in exception_list:
            continue
        hyperparams_raw = d3m_primitives[primitive]
        hyperparams = build_hyper_params(hyperparams_raw)

        package_parts = primitive.split('.')
        family = package_parts[2]

        # For classification whitelist:
        # if family == 'classification' or family == 'feature_selection' or\
        #     family == 'data_preprocessing' or family == 'data_transformation':
        # For regression whitelist:
        if family == "regression" or family == 'feature_selection' or\
            family == 'data_preprocessing' or family == 'data_transformation':
            # if primitive == 'd3m.primitives.classification.extra_trees.SKlearn':
                #     logging.info("Found %s" % primitive)
                failure_reason = ""
                failure_instances = 0
                for instance in range(pipeline_instances):
                    try:
                        create_pipeline(primitive, hyperparams, preamble_pipeline, output_dir, d3m_primitives, instance)
                    except Exception as e:
                        failure_reason = e
                        failure_instances += 1
                if failure_instances == pipeline_instances:
                    _logger.info("All %s instances were bad. Putting %s on the naughty step exception: %s" % (pipeline_instances, primitive, str(failure_reason)))
                    _logger.warning(traceback.format_exc())
                    blacklist_file_handle.write("%s\t%s\n" % (primitive, failure_reason))

    # Close the blacklist file handle when we are all done
    blacklist_file_handle.close()


def create_pipeline(primitive_name, hyperparams, preamble_pipeline, output_dir, d3m_primitives, instance):
    my_class = locate(primitive_name)

    # Create an instance of the primitive we want to evaluate
    _logger.info("Creating pipeline instance %s for primitive: %s" % (str(instance + 1), primitive_name))
    primitive_instance = my_class(hyperparams=hyperparams)

    # Create a pipeline object
    pipeline = Pipeline(pipeline_id=primitive_name + "-" + str(instance + 1),
                        context=PipelineContext.PRETRAINING,
                        source={
                            "name": "SRI Autoflow",
                            "contact": "mailto:freitag@ai.sri.com"
                        },
                        name=primitive_name,
                        description=primitive_name,
                        created=datetime.datetime.now(datetime.timezone.utc))

    # Add the input handle
    pipeline.add_input(name="inputs")

    # Add the primitive we want to test to the end of the pipeline
    output = add_d3m_pipeline_steps(pipeline, preamble_pipeline, primitive_instance, d3m_primitives, primitive_name)

    # Add the output handle to the pipeline
    pipeline.add_output(data_reference=output)

    # Convert the format to json for saving
    obj = pipeline.to_json_structure()

    # Json serializer doesn't know what to do with int64s
    def default(o):
        if isinstance(o, numpy.int64):
            return int(o)
        if isinstance(o, numpy.bool_):
            return bool(o)
        print("Can't serialize %s of type %s" % (o, type(o)))
        raise TypeError

    # Dump the json pipeline to disk
    json_string = json.dumps(obj, indent=4, default=default)

    output_file = "%s/%s.json" % (output_dir, primitive_name + "-" + str(instance + 1))
    _logger.info("Output file is: %s" % output_file)
    with open(output_file, "w") as outfile:
        outfile.write(json_string)

    # Used to generate the description of a pipeline
    # command = "python3 -m d3m pipeline describe %s/%s.json > %s/%s.desc" % (output_dir, primitive_name, output_dir, primitive_name)
    # _logger.info("Describe command is: %s" % command)
    # try:
    #     subprocess.check_output(command, shell=True)
    # except Exception as e:
    #     _logger.info("Pipeline %s failed to generate description" % primitive_name)
    #     _logger.warning(e)


def add_d3m_pipeline_steps(pipeline, preamble, primitive_instance, d3m_primitives, primitive_name, **args):
    package_parts = primitive_name.split('.')
    family = package_parts[2]

    nsteps = len(pipeline.steps)

    def update_argument(arg):
        if nsteps == 0:
            return
        data = arg['data']
        if data == 'inputs.0':
            arg['data'] = 'steps.%d.produce' % nsteps
        else:
            steps, stepi, meth = arg['data'].split('.')
            stepi = nsteps + int(stepi)
            arg['data'] = 'steps.%d.%s' % (stepi, meth)

    for step in preamble.steps:
        scopy = copy.copy(step)
        scopy.arguments = dict((k, update_argument(v)) for k, v in scopy.arguments.items())
        pipeline.steps.append(step)

    node = 'steps.%d.produce' % (len(pipeline.steps) - 1)
    target = preamble.outputs[1]['data']
    steps, stepi, meth = target.split('.')
    target = 'steps.%d.%s' % (int(stepi) + nsteps, meth)

    # Find the Dataset To DataFrame step and use that as the input to the last step: ConstructPredictions. This
    # is required as the ConstructPredictions primitive needs the original input data with the d3mIndex column
    for idx, step in enumerate(pipeline.steps):
        if step.primitive.__name__ == 'DatasetToDataFramePrimitive':
            reference_step = 'steps.%s.produce' % idx

    # Also now add the primitive to be tested here.
    node = add_d3m_pipeline_step(pipeline, primitive_instance, inputs=node, outputs=target)

    _logger.info("Family: %s" % primitive_instance.metadata._current_metadata.metadata['primitive_family'])

    # For data cleaning primitives we need to add a classification primitive right afterwards.
    if family == 'feature_selection' or family == 'data_preprocessing' or family == 'data_transformation':
        classification_primitive = "d3m.primitives.classification.gradient_boosting.SKlearn"
        hyperparams_raw = d3m_primitives[classification_primitive]
        hyperparams = build_hyper_params(hyperparams_raw)
        classification_class = locate(classification_primitive)
        classification_instance = classification_class(hyperparams=hyperparams)
        node = add_d3m_pipeline_step(pipeline, classification_instance, inputs=node, outputs=target)

    # Add in the ConstructPredictions primitive as the last step - this will format the predictions appropriately
    format_predictions = ConstructPredictions(hyperparams={})

    # The ConstructPredictions primitive will need a reference=DataFrame during produce
    node = add_d3m_pipeline_step(pipeline, format_predictions, inputs=node,
                                 reference=reference_step, outputs=target)

    return node


def add_d3m_pipeline_step(pipeline, prim, **args):
    pclass = type(prim)
    mdata = pclass.metadata.query()
    pstep = PrimitiveStep(primitive_description=mdata)
    pargs = mdata['primitive_code'].get('arguments', {})
    for arg, node in args.items():
        if pargs.get(arg, None) is not None:
            pstep.add_argument(arg, ArgumentType.CONTAINER, node)
    pstep.add_output("produce")
    for hp, val in prim.hyperparams.items():
        pstep.add_hyperparameter(name=hp,
                                 argument_type=ArgumentType.VALUE,
                                 data=val)
    pipeline.add_step(pstep)
    return "steps.%d.produce" % (len(pipeline.steps) - 1)


def get_preamble_pipeline():
    ppclass = find_pipeline_class(name='SingleTableSRIFragment')
    logging.info("Generate the preamble pipeline")
    preamble_pipeline = ppclass.generate(None, config=None, augmentation_hints=None)
    logging.info("Preamble pipeline generation complete")
    preamble_args = dict(max_tokenized_expansion=30)
    # for arg, val in self.config.preamble_arguments():
    #     preamble_args[arg] = val
    preamble_pipeline.configure(**preamble_args)
    # self.preamble_pipeline.study_dataset(dataset)
    return preamble_pipeline


def build_hyper_params(hyperparams_raw):
    hyper_params = dict()
    for hyperparam in hyperparams_raw:
        key = str(hyperparam)
        index = 0
        if len(hyperparams_raw[hyperparam]) > 1:
            index = random.randint(0, len(hyperparams_raw[hyperparam]) - 1)
        default = hyperparams_raw[hyperparam][index]
        hyper_params.update([(key, default)])
    return hyper_params


'''
Entry point - required to make python happy
'''
if __name__ == "__main__":
    main(sys.argv)