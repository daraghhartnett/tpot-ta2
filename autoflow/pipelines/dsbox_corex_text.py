from .base import AutoflowPipelineFragment, ENTRY_POINT_TYPE, ATTRIBUTE_TYPE, TRUE_TARGET_TYPE, KEY_TYPES

from d3m.primitives.data_transformation.column_parser import Common as ColumnParser
from d3m.primitives.data_transformation.extract_columns_by_semantic_types import Common as ExtractColumnsBySemanticTypes
from d3m.primitives.data_transformation.dataset_to_dataframe import Common as DatasetToDataFrame
from d3m.primitives.schema_discovery.profiler import Common as SimpleProfiler

from d3m.primitives.data_preprocessing.dataset_text_reader import DatasetTextReader
from d3m.primitives.data_transformation.denormalize import Common as Denormalize

from d3m.primitives.data_cleaning.cleaning_featurizer import DSBOX as CleaningFeaturizer
from d3m.primitives.data_transformation.encoder import DistilTextEncoder as Encoder
from d3m.primitives.feature_construction.corex_text import DSBOX as CorexText
from d3m.primitives.data_transformation.to_numeric import DSBOX as ToNumeric
from d3m.primitives.data_cleaning.mean_imputation import DSBOX as MeanImputation

__all__ = ('DSBoxCorexTextFragment',)

#############
# 20200731: We could not retrieve the pipeline from the MLDB for the pipeline we reviewed from the leaderboard - probably
#           because it did not upload cleanly for some reason. Thus we were unable to see how the HP's were set. However,
#           with all the default values being set from CleaningFeaturizer to MeanImputation we were able to get a bump
#           of about 0.1 F1. This will be our primary fallback from MultiTableSRIFragment in the Gama multi fragment
#           feature experiment.
#
# 20210119: We have a couple sample pipelines for 32_wikiqa in the docs folder - lets experiment and see what sort of
#           scores we can git.
#############

class DSBoxCorexTextFragment(AutoflowPipelineFragment):

    name = "DSBox Corex Text Fragment"
    description = "Based on the MultiTableSRIFragment, this was adapted to test out the scores achievable with the " \
                  "ISI DSBoxCorexText in addition to the supporting primitives. See " \
                  "https://gitlab.com/daraghhartnett/tpot-ta2/-/issues/2 for details on test results"
    label = "distil_text_encoder"
    configuration = dict(resource_id=(2, 'dataframe_resource'))
    fragment_weight = 1.5

    def study_dataset(self, dataset):
        resource_id = None
        for rid in dataset.keys():
            stypes = dataset.metadata.query((rid,)).get('semantic_types', [])
            if ENTRY_POINT_TYPE in stypes:
                resource_id = rid
                break
        if resource_id is None:
            raise ValueError("Cannot find entry resource")
        self.configure(resource_id=resource_id)

    def generate_dataset_steps(self):
        # TODO: CMU does not use the DatasetTextReader - it goes straight to denormalize - might be worth a look
        step_0_tr = DatasetTextReader(hyperparams={})
        step_1_dn = Denormalize(hyperparams={})
        node = "inputs.0"
        node = self.add_af_step(step_0_tr, node)
        node = self.add_af_step(step_1_dn, node)
        return node

    def generate_dataframe_steps(self, node):
        step_2_tdf = DatasetToDataFrame(hyperparams={})

        # TODO: that CMU uses a common profiler - we may want to look at that as an alternative to this
        simple_profiler_defaults = SimpleProfiler.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams'].defaults()
        step_3_simple_profiler = SimpleProfiler(hyperparams=simple_profiler_defaults.defaults().replace({
                # TODO: Check that this is rendered as expected in the output pipeline.
                'categorical_max_absolute_distinct_values': None, # The alternative to this (unlimited) is to specify an int
                'categorical_max_ratio_distinct_values': 1
            }),)

        step_4_ext_targ = ExtractColumnsBySemanticTypes(hyperparams=dict(semantic_types=(TRUE_TARGET_TYPE,)))
        step_5_cp = ColumnParser(hyperparams={})
        # TODO: CMU Just takes the input of the Column Parser and does not specify the ATTRIBUTE_TYPE HP.
        step_6_ext_attr = ExtractColumnsBySemanticTypes(hyperparams=dict(semantic_types=(ATTRIBUTE_TYPE,)))

        cleaning_featurizer_defaults = CleaningFeaturizer.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams'].defaults()
        step_7_cleaning_featurizer = CleaningFeaturizer(hyperparams=cleaning_featurizer_defaults)

        # TODO: ISI has their own encoder (d3m.primitives.data_preprocessing.encoder.DSBOX), try that instead of the
        #  distil one which needs an output specified
        encoder_defaults = Encoder.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams'].defaults()
        step_8_encoder = Encoder(hyperparams=encoder_defaults)

        corex_text_defaults = CorexText.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams'].defaults()
        step_9_corext_text = CorexText(hyperparams=corex_text_defaults)

        to_numeric_defaults = ToNumeric.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams'].defaults()
        step_10_to_numeric = ToNumeric(hyperparams=to_numeric_defaults)

        mean_imputation_defaults = MeanImputation.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams'].defaults()
        step_11_mean_imputation = MeanImputation(hyperparams=mean_imputation_defaults)

        node = self.add_af_step(step_2_tdf, node)

        # Add primitive to add the metadata back in
        node = self.add_af_step(step_3_simple_profiler, node)

        # Extract the target
        tnode = self.add_af_step(step_4_ext_targ, node)
        rtnode = tnode

        # Extract features
        node = self.add_af_step(step_5_cp, node)
        node = self.add_af_step(step_6_ext_attr, node)

        node = self.add_af_step(step_7_cleaning_featurizer, node)
        # TODO: 2021.01.19 Need to review what to set the outputs to in the sample pipeline when the MLDB is back up.
        #  It is also likely that we will need to set other HP's to work properly on the dataset such as
        #  the columns to use and the metric
        node = self.add_af_step(step_8_encoder, node, outputs=tnode)
        node = self.add_af_step(step_9_corext_text, node)
        node = self.add_af_step(step_10_to_numeric, node)
        node = self.add_af_step(step_11_mean_imputation, node)

        return node, tnode, rtnode

