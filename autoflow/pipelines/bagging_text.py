from .base import AutoflowPipelineFragment, ENTRY_POINT_TYPE, ATTRIBUTE_TYPE, TRUE_TARGET_TYPE, KEY_TYPES

from d3m.primitives.data_transformation.column_parser import Common as ColumnParser
from d3m.primitives.data_transformation.extract_columns_by_semantic_types import Common as ExtractColumnsBySemanticTypes
from d3m.primitives.data_transformation.dataset_to_dataframe import Common as DatasetToDataFrame
from d3m.primitives.schema_discovery.profiler import Common as Profiler
from d3m.primitives.data_cleaning.imputer import SKlearn as Imputer
from d3m.primitives.data_preprocessing.dataset_text_reader import DatasetTextReader
from d3m.primitives.data_transformation.denormalize import Common as Denormalize
from d3m.primitives.feature_construction.corex_text import DSBOX as CorexText
from d3m.primitives.data_transformation.to_numeric import DSBOX as ToNumeric
from d3m.primitives.data_cleaning.robust_scaler import SKlearn as RobustScaler
from d3m.primitives.data_transformation.text_reader import Common as TextReader
from d3m.primitives.data_transformation.encoder import DistilTextEncoder as DistilTextEncoder

__all__ = ('BaggingTextFragment',)

# Based on ./docs/30_personae_CMU_ta2_summer_2020_eval.json

class BaggingTextFragment(AutoflowPipelineFragment):

    name = "Bagging Text Fragment"
    description = "Based on the MultiTableSRIFragment, this was adapted to test out the scores achievable with " \
                  "Bagging in addition to the supporting primitives. See " \
                  "https://gitlab.com/daraghhartnett/tpot-ta2/-/issues/2 for details on test results"
    label = "bagging_text_encoder"
    configuration = dict(resource_id=(2, 'dataframe_resource'))
    fragment_weight = 1.5

    def study_dataset(self, dataset):
        resource_id = None
        for rid in dataset.keys():
            stypes = dataset.metadata.query((rid,)).get('semantic_types', [])
            if ENTRY_POINT_TYPE in stypes:
                resource_id = rid
                break
        if resource_id is None:
            raise ValueError("Cannot find entry resource")
        self.configure(resource_id=resource_id)

    def generate_dataset_steps(self):
        step_0_tr = DatasetTextReader(hyperparams={})
        step_1_dn = Denormalize(hyperparams={})
        node = "inputs.0"
        node = self.add_af_step(step_0_tr, node)
        node = self.add_af_step(step_1_dn, node)
        return node

    def generate_dataframe_steps(self, node):
        step_2_tdf = DatasetToDataFrame(hyperparams={})

        profiler_defaults = Profiler.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams'].defaults()
        step_3_profiler = Profiler(hyperparams=profiler_defaults.defaults().replace({
                'categorical_max_absolute_distinct_values': None, # The alternative to this (unlimited) is to specify an int
                'categorical_max_ratio_distinct_values': 1
            }),)

        step_4_cp = ColumnParser(hyperparams={})
        step_5_ext_attr = ExtractColumnsBySemanticTypes(hyperparams=dict(semantic_types=(ATTRIBUTE_TYPE,)))
        step_6_ext_targ = ExtractColumnsBySemanticTypes(hyperparams=dict(semantic_types=(TRUE_TARGET_TYPE,)))

        text_reader_defaults = TextReader.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams'].defaults()
        step_7_text_reader = TextReader(hyperparams=text_reader_defaults.defaults().replace({
                'return_result': 'replace',
            }),)

        imputer_defaults = Imputer.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams'].defaults()
        step_8_imputer = Imputer(hyperparams=imputer_defaults.defaults().replace({
                'error_on_no_input': False,
                'return_result': 'replace',
                'use_semantic_types': True
            }),)

        text_encoder_defaults = DistilTextEncoder.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams'].defaults()
        step_9_text_encoder = DistilTextEncoder(hyperparams=text_encoder_defaults.defaults().replace({
                'encoder_type': 'tfidf'
            }),)

        node = self.add_af_step(step_2_tdf, node)

        # Add primitive to add the metadata back in
        node = self.add_af_step(step_3_profiler, node)
        profiled_node = node

        node = self.add_af_step(step_4_cp, node)
        # Extract features
        node = self.add_af_step(step_5_ext_attr, node)

        # Extract the target
        tnode = self.add_af_step(step_6_ext_targ, profiled_node)
        rtnode = tnode

        node = self.add_af_step(step_7_text_reader, node)
        node = self.add_af_step(step_8_imputer, node)
        node = self.add_af_step(step_9_text_encoder, node, outputs=tnode)


        return node, tnode, rtnode

