from .base import AutoflowPipelineFragment, SCALAR_TYPES

from d3m.primitives.schema_discovery.profiler import Common as Profiler
from d3m.primitives.data_transformation.remove_semantic_types import Common as RemoveSemanticTypes
from d3m.primitives.data_transformation.column_parser import Common as ColumnParser
from d3m.primitives.feature_selection.joint_mutual_information import AutoRPI as AutoRPI

from d3m.primitives.data_transformation.simple_column_parser import DataFrameCommon as SimpleColumnParser
from d3m.primitives.data_transformation.extract_columns_by_semantic_types import Common as ExtractColumnsBySemanticTypes

from d3m.primitives.data_transformation.dataset_to_dataframe import Common as DatasetToDataFrame
from d3m.primitives.data_transformation.conditioner import Conditioner
from d3m.primitives.data_preprocessing.dataset_text_reader import DatasetTextReader

__all__ = ('SingleTableAutoRPIFragment',)

class SingleTableAutoRPIFragment(AutoflowPipelineFragment):

    name = "Single Table Preamble, AutoRPI"
    description = "Common pipeline prefix for single-table problems, with Auto RPI"
    label = "single_table_autorpi"

    def generate_dataset_steps(self):
        tr = DatasetTextReader(hyperparams={})
        node = self.add_af_step(tr, "inputs.0")
        return node

    def generate_dataframe_steps(self, node):
        todf = DatasetToDataFrame(hyperparams=dict(dataframe_resource=None))

        profiler_defaults = Profiler.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams'].defaults()
        profiler = Profiler(hyperparams=profiler_defaults.defaults())

        remove_semantic_types_defaults = RemoveSemanticTypes.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams'].defaults()
        remove_semantic_types = RemoveSemanticTypes(hyperparams=remove_semantic_types_defaults.defaults())

        column_parser = ColumnParser(hyperparams={})

        ext_targ = ExtractColumnsBySemanticTypes(hyperparams=dict(semantic_types=("https://metadata.datadrivendiscovery.org/types/TrueTarget",)))

        auto_rpi_defaults = AutoRPI.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams'].defaults()
        auto_rpi = AutoRPI(hyperparams=auto_rpi_defaults.defaults())

        # Get a dataframe
        node = self.add_af_step(todf, node)

        # Add primitive to add the metadata back in
        node = self.add_af_step(profiler, node)

        # Remove semantic types
        node = self.add_af_step(remove_semantic_types, node)
        node = self.add_af_step(column_parser, node)

        # Step 6 needs outputs specified

        # Extract target
        tnode = self.add_af_step(ext_targ, node)
        rtnode = tnode

        node = self.add_af_step(auto_rpi, node)

        return node, tnode, rtnode

