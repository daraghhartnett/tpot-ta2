from .base import AutoflowPipelineFragment, ENTRY_POINT_TYPE, ATTRIBUTE_TYPE, TRUE_TARGET_TYPE, KEY_TYPES, PRIMARY_KEY_TYPE

from d3m.primitives.data_transformation.extract_columns_by_semantic_types import Common as ExtractColumnsBySemanticTypes
from d3m.primitives.data_transformation.dataset_to_dataframe import Common as DatasetToDataFrame
from d3m.primitives.data_cleaning.mean_imputation import DSBOX as Imputer
from d3m.primitives.data_preprocessing.dataset_text_reader import DatasetTextReader
from d3m.primitives.data_transformation.denormalize import Common as Denormalize
from d3m.primitives.feature_construction.corex_text import DSBOX as CorexText
from d3m.primitives.data_transformation.to_numeric import DSBOX as ToNumeric
from d3m.primitives.data_cleaning.cleaning_featurizer import DSBOX as CleaningFeaturizer
from d3m.primitives.data_transformation.encoder import DSBOX as Encoder

__all__ = ('RandomForestTextFragment',)

# Based on ./docs/32_wiki_ISI_ta1_winter_2020_eval.json

class RandomForestTextFragment(AutoflowPipelineFragment):

    name = "Random Forest Text Fragment"
    description = "Based on the MultiTableSRIFragment, this was adapted to test out the scores achievable with the " \
                  "Random Forest in addition to the supporting primitives. See " \
                  "https://gitlab.com/daraghhartnett/tpot-ta2/-/issues/2 for details on test results"
    label = "random_forest_text_encoder"
    configuration = dict(resource_id=(2, 'dataframe_resource'))
    fragment_weight = 1.5

    def study_dataset(self, dataset):
        resource_id = None
        for rid in dataset.keys():
            stypes = dataset.metadata.query((rid,)).get('semantic_types', [])
            if ENTRY_POINT_TYPE in stypes:
                resource_id = rid
                break
        if resource_id is None:
            raise ValueError("Cannot find entry resource")
        self.configure(resource_id=resource_id)

    def generate_dataset_steps(self):
        step_0_tr = DatasetTextReader(hyperparams={})
        step_1_dn = Denormalize(hyperparams={})
        node = "inputs.0"
        node = self.add_af_step(step_0_tr, node)
        node = self.add_af_step(step_1_dn, node)
        return node

    def generate_dataframe_steps(self, node):
        step_2_tdf = DatasetToDataFrame(hyperparams={})

        step_3_ext_attrs_primary = ExtractColumnsBySemanticTypes(hyperparams=dict(semantic_types=(PRIMARY_KEY_TYPE,
                                                                                         ATTRIBUTE_TYPE)))

        step_4_cleaning_featurizer = CleaningFeaturizer(hyperparams={})

        step_5_encoder = Encoder(hyperparams={})

        corex_text_defaults = CorexText.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams'].defaults()
        step_6_corext_text = CorexText(hyperparams=corex_text_defaults.defaults().replace({
                'n_hidden': 50,
                'max_df': 0.6,
                'n_grams': 1
            }),)

        to_numeric_defaults = ToNumeric.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams'].defaults()
        step_7_to_numeric = ToNumeric(hyperparams=to_numeric_defaults)

        mean_imputer_defaults = Imputer.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams'].defaults()
        step_8_mean_imputer = Imputer(hyperparams=mean_imputer_defaults)

        step_9_ext_targ = ExtractColumnsBySemanticTypes(hyperparams=dict(semantic_types=(TRUE_TARGET_TYPE,)))

        to_numeric_defaults = ToNumeric.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams'].defaults()
        step_10_to_numeric = ToNumeric(hyperparams=to_numeric_defaults.defaults().replace({
            'drop_non_numeric_columns': False
        }),)


        node = self.add_af_step(step_2_tdf, node)
        df_node = node

        node = self.add_af_step(step_3_ext_attrs_primary, node)
        node = self.add_af_step(step_4_cleaning_featurizer, node)
        node = self.add_af_step(step_5_encoder, node)
        node = self.add_af_step(step_6_corext_text, node)
        node = self.add_af_step(step_7_to_numeric, node)
        node = self.add_af_step(step_8_mean_imputer, node)

        tnode = self.add_af_step(step_9_ext_targ, df_node)
        rtnode = tnode

        node = self.add_af_step(step_10_to_numeric, node)

        return node, tnode, rtnode

