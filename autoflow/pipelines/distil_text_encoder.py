from .base import AutoflowPipelineFragment, ENTRY_POINT_TYPE, ATTRIBUTE_TYPE, TRUE_TARGET_TYPE, KEY_TYPES

# from d3m.primitives.data_transformation.simple_column_parser import DataFrameCommon as SimpleColumnParser
from d3m.primitives.data_transformation.column_parser import Common as ColumnParser
from d3m.primitives.data_transformation.extract_columns_by_semantic_types import Common as ExtractColumnsBySemanticTypes
from d3m.primitives.data_transformation.dataset_to_dataframe import Common as DatasetToDataFrame
from d3m.primitives.schema_discovery.profiler import Common as SimpleProfiler

from d3m.primitives.data_transformation.conditioner import Conditioner
from d3m.primitives.data_preprocessing.dataset_text_reader import DatasetTextReader
from d3m.primitives.data_transformation.denormalize import Common as Denormalize

from d3m.primitives.data_transformation.text_reader import Common as TextReader
from d3m.primitives.data_cleaning.imputer import SKlearn as Imputer
from d3m.primitives.data_transformation.encoder import DistilTextEncoder as DistilTextEncoder

# We are no longer using this fragment as it is a duplicate of Bagging Text Fragment

__all__ = ('DistilTextEncoderFragment',)

# 20200731: We get marginally (+.02 F1 for 30_personae) better scores than our own MultiTableSRIFragment with this
#           Fragment but it is slower for problems with lots of data. There may be a way to improve the scores if we
#           try different sampling strategies for problems with small amounts of training data (30_personae for example)
#           We will use this Fragment for now as one of the candidate Fragments for the multi Fragment Gama feature.

class DistilTextEncoderFragment(AutoflowPipelineFragment):

    name = "Distil Text Encoder Fragment"
    description = "Based on the MultiTableSRIFragment, this was adapted to test out the scores achievable with the " \
                  "Uncharted  DistilTextEncoder in addition to the supporting primitives. See " \
                  "https://gitlab.com/daraghhartnett/tpot-ta2/-/issues/2 for details on test results"
    label = "distil_text_encoder"
    configuration = dict(resource_id=(2, 'dataframe_resource'))
    fragment_weight = 0.5

    def study_dataset(self, dataset):
        resource_id = None
        for rid in dataset.keys():
            stypes = dataset.metadata.query((rid,)).get('semantic_types', [])
            if ENTRY_POINT_TYPE in stypes:
                resource_id = rid
                break
        if resource_id is None:
            raise ValueError("Cannot find entry resource")
        self.configure(resource_id=resource_id)

    def generate_dataset_steps(self):
        tr = DatasetTextReader(hyperparams={})
        dn = Denormalize(hyperparams={})
        node = "inputs.0"
        node = self.add_af_step(tr, node)
        node = self.add_af_step(dn, node)
        return node

    def generate_dataframe_steps(self, node):
        todf = DatasetToDataFrame(hyperparams={})

        # Use the HP's listed in the diego pipeline. Still need to add the other one - need to look at the constructor
        # to see how to form the value. This is what it looks like in json:
        # "categorical_max_absolute_distinct_values": {
        #     "data": {
        #         "case": "unlimited",
        #         "value": null
        #     },
        #     "type": "VALUE"
        # },
        # import typing
        # typing.Union[case, "unlimited"],
        simple_profiler_defaults = SimpleProfiler.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams'].defaults()
        simple_profiler = SimpleProfiler(hyperparams=simple_profiler_defaults.defaults().replace({
                # 'categorical_max_absolute_distinct_values': ["unlimited"],
                'categorical_max_ratio_distinct_values': 1
            }),)

        cp = ColumnParser(hyperparams={})
        ext_attr = ExtractColumnsBySemanticTypes(hyperparams=dict(semantic_types=(ATTRIBUTE_TYPE,)))

        text_reader_defaults = TextReader.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams'].defaults()
        textReader = TextReader(hyperparams=text_reader_defaults)

        imputer_defaults = Imputer.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams'].defaults()
        imputer = Imputer(hyperparams=imputer_defaults.defaults().replace({
                'strategy': 'most_frequent'
            }),)

        text_encoder_defaults = DistilTextEncoder.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams'].defaults()
        textEncoder = DistilTextEncoder(hyperparams=text_encoder_defaults.defaults().replace({
                'encoder_type': 'tfidf'
            }),)

        ext_targ = ExtractColumnsBySemanticTypes(hyperparams=dict(semantic_types=(TRUE_TARGET_TYPE,)))

        node = self.add_af_step(todf, node)

        # Add primitive to add the metadata back in
        node = self.add_af_step(simple_profiler, node)

        # Extract the target
        tnode = self.add_af_step(ext_targ, node)
        rtnode = tnode

        # Extract features
        node = self.add_af_step(cp, node)
        node = self.add_af_step(ext_attr, node)

        # node = self.add_af_step(cond, node)
        node = self.add_af_step(textReader, node)
        node = self.add_af_step(imputer, node)

        node = self.add_af_step(textEncoder, node, outputs=tnode)

        return node, tnode, rtnode

