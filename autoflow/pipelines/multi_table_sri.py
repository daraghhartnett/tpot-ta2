from .base import AutoflowPipelineFragment, ENTRY_POINT_TYPE, ATTRIBUTE_TYPE, TRUE_TARGET_TYPE, KEY_TYPES

from d3m.primitives.data_transformation.simple_column_parser import DataFrameCommon as SimpleColumnParser
from d3m.primitives.data_transformation.extract_columns_by_semantic_types import Common as ExtractColumnsBySemanticTypes
from d3m.primitives.data_transformation.dataset_to_dataframe import Common as DatasetToDataFrame
from d3m.primitives.schema_discovery.profiler import Common as SimpleProfiler

from d3m.primitives.data_transformation.conditioner import Conditioner
from d3m.primitives.data_preprocessing.dataset_text_reader import DatasetTextReader
from d3m.primitives.data_transformation.denormalize import Common as Denormalize

__all__ = ('MultiTableSRIFragment',)

class MultiTableSRIFragment(AutoflowPipelineFragment):

    name = "Multi Table Preamble, SRI"
    description = "Common pipeline prefix for multi-table problems, with SRI Conditioner"
    label = "multi_table_sri"
    configuration = dict(resource_id=(2, 'dataframe_resource'),
                         max_tokenized_expansion=(8, 'maximum_expansion')
    )
    fragment_weight = 1.0

    def study_dataset(self, dataset):
        resource_id = None
        for rid in dataset.keys():
            stypes = dataset.metadata.query((rid,)).get('semantic_types', [])
            if ENTRY_POINT_TYPE in stypes:
                resource_id = rid
                break
        if resource_id is None:
            raise ValueError("Cannot find entry resource")
        self.configure(resource_id=resource_id)

    def generate_dataset_steps(self):
        # step 0
        tr_0 = DatasetTextReader(hyperparams={})
        # step 1
        dn_1 = Denormalize(hyperparams={})
        node = "inputs.0"
        node = self.add_af_step(tr_0, node)
        node = self.add_af_step(dn_1, node)
        return node

    def generate_dataframe_steps(self, node):
        # step 2
        todf_2 = DatasetToDataFrame(hyperparams={})
        # step 3
        simple_profiler_3 = SimpleProfiler(hyperparams={})

        # step 4
        ext_targ_4 = ExtractColumnsBySemanticTypes(hyperparams=dict(semantic_types=(TRUE_TARGET_TYPE,)))

        # step 5
        scp_5 = SimpleColumnParser(hyperparams={})

        # step 6
        cp_6 = SimpleColumnParser(hyperparams={})
        # step 7
        ext_attr_7 = ExtractColumnsBySemanticTypes(hyperparams=dict(semantic_types=(ATTRIBUTE_TYPE,)))
        # step 8
        cond_8 = Conditioner(hyperparams=dict(ensure_numeric=True, maximum_expansion=30))

        node = self.add_af_step(todf_2, node)

        # Add primitive to add the metadata back in
        node = self.add_af_step(simple_profiler_3, node)

        # Extract the target
        tnode = self.add_af_step(ext_targ_4, node)
        rtnode = tnode
        tnode = self.add_af_step(scp_5, tnode)

        # Extract features
        node = self.add_af_step(cp_6, node)
        node = self.add_af_step(ext_attr_7, node)
        node = self.add_af_step(cond_8, node)

        return node, tnode, rtnode

