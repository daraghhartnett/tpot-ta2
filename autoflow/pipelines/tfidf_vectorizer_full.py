from .base import AutoflowPipelineFragment, ENTRY_POINT_TYPE,  TRUE_TARGET_TYPE, BOOLEAN_TYPE, ATTRIBUTE_TYPE, TEXT_TYPE

from d3m.primitives.data_transformation.extract_columns_by_semantic_types import Common as ExtractColumnsBySemanticTypes
from d3m.primitives.data_transformation.dataset_to_dataframe import Common as DatasetToDataFrame
from d3m.primitives.data_transformation.denormalize import Common as Denormalize

from d3m.primitives.data_transformation.text_reader import Common as TextReader
from d3m.primitives.data_transformation.add_semantic_types import Common as AddSemanticTypes
from d3m.primitives.data_transformation.column_parser import Common as ColumnParser
from d3m.primitives.feature_extraction.tfidf_vectorizer import SKlearn as TFIDF
from d3m.primitives.classification.bagging import SKlearn as Bagging
from d3m.primitives.data_transformation.construct_predictions import Common as ConstructPredictions

__all__ = ('TFIDFVectorizerFragment',)


class TFIDFVectorizerFragment(AutoflowPipelineFragment):

    name = "TFIDF Vectorizer Fragment"
    description = "TFIDF Vectorizer fragment for  multi-table problems"
    label = "tfidf_vectorizer"
    configuration = dict(resource_id=(1, 'dataframe_resource'))
    fragment_weight = 1.0
    preamble_fragment = False
    primitives_to_tune = []

    def study_dataset(self, dataset):
        resource_id = None
        for rid in dataset.keys():
            stypes = dataset.metadata.query((rid,)).get('semantic_types', [])
            if ENTRY_POINT_TYPE in stypes:
                resource_id = rid
                break
        if resource_id is None:
            raise ValueError("Cannot find entry resource")
        self.configure(resource_id=resource_id)

    def generate_dataset_steps(self):
        # step 0
        dn_0 = Denormalize(hyperparams={})
        node = "inputs.0"
        node = self.add_af_step(dn_0, node)
        return node

    def generate_dataframe_steps(self, node):
        # step 1
        todf_1 = DatasetToDataFrame(hyperparams={})

        # step 2
        text_reader_defaults = TextReader.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams'].defaults()
        text_reader_2 = TextReader(hyperparams=text_reader_defaults.replace({
                'return_result': 'replace'
            }),)

        # step 3
        add_semantic_types_defaults_tt = AddSemanticTypes.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams'].defaults()
        add_semantic_types_tt_3 = AddSemanticTypes(hyperparams=add_semantic_types_defaults_tt.replace({
                'columns': [1],
                'semantic_types': (TRUE_TARGET_TYPE,)
            }),)

        # step 4
        add_semantic_types_defaults_b = AddSemanticTypes.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams'].defaults()
        add_semantic_types_b_4 = AddSemanticTypes(hyperparams=add_semantic_types_defaults_b.replace({
                'columns': [1],
                'semantic_types': (BOOLEAN_TYPE,)
            }),)

        # step 5
        add_semantic_types_defaults_a = AddSemanticTypes.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams'].defaults()
        add_semantic_types_a_5 = AddSemanticTypes(hyperparams=add_semantic_types_defaults_a.replace({
                'columns': [2],
                'semantic_types': (ATTRIBUTE_TYPE,)
            }),)

        # step 6
        add_semantic_types_defaults_t = AddSemanticTypes.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams'].defaults()
        add_semantic_types_t_6 = AddSemanticTypes(hyperparams=add_semantic_types_defaults_t.replace({
                'columns': [2],
                'semantic_types': (TEXT_TYPE,)
            }),)

        # step 7
        column_parser_7 = ColumnParser(hyperparams={})

        # step 8
        ext_attr_defaults = ExtractColumnsBySemanticTypes.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams'].defaults()
        ext_attr_8 = ExtractColumnsBySemanticTypes(hyperparams=ext_attr_defaults.replace({
                'exclude_columns': [],
                'semantic_types': (ATTRIBUTE_TYPE,)
            }),)

        # step 9
        # Note 1: Even though min_df = 1 is the default it breaks the search as no terms are left over after parsing them
        #         so we need to set it to 1 to avoid the error.
        # Note 2: Also note that we *have* to specify the values below - if
        #         we do not specify them (using defaults instead) the constructor complains, even though the sample
        #         pipeline does not set them. This and the other note make me think this sample pipeline was hand
        #         crafted, not evolved from search
        # Note 3: This pipeline will only run on a beefy machine. Otherwise a call to fit-score will sit for a few
        #         minutes and then exit with no error.
        tfidf_vectorizer_defaults = TFIDF.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams'].defaults()
        tfidf_vectorizer_9 = TFIDF(hyperparams=tfidf_vectorizer_defaults.replace({
                'use_semantic_types': True,
                'return_result': 'replace',
                'min_df': 0
            }),)
        self.primitives_to_tune.append(tfidf_vectorizer_9)

        # step 10
        ext_targ_10 = ExtractColumnsBySemanticTypes(hyperparams=dict(semantic_types=(TRUE_TARGET_TYPE,)))

        # TODO: Try this fragment without the bagging to see if we get good/better/equivalent scores (0.97 is attained
        #  with the below)
        #  step 11
        bagging_classifier_11_defaults = Bagging.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams'].defaults()
        bagging_classifier_11 = Bagging(hyperparams=bagging_classifier_11_defaults)

        node = self.add_af_step(todf_1, node)
        node = self.add_af_step(text_reader_2, node)

        node = self.add_af_step(add_semantic_types_tt_3, node)
        node = self.add_af_step(add_semantic_types_b_4, node)
        node = self.add_af_step(add_semantic_types_a_5, node)
        node = self.add_af_step(add_semantic_types_t_6, node)

        rtnode = tnode = node

        node = self.add_af_step(column_parser_7, node)
        node = self.add_af_step(ext_attr_8, node)
        node = self.add_af_step(tfidf_vectorizer_9, node)

        # In the current state this does go to GAMA but it never finds a score-able pipeline. Maybe we
        # need to make this an end to end pipeline like with Graphs.
        tnode = self.add_af_step(ext_targ_10, tnode)

        # 42,698 columns! With this we do get pipelines but they are un-runnable. Bypass GAMA and get it to run the
        # pipeline directly
        node = self.add_af_step(bagging_classifier_11, node, outputs=tnode)

        return node, tnode, rtnode

