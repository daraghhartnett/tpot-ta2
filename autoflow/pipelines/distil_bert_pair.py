from .base import AutoflowPipelineFragment, ENTRY_POINT_TYPE, ATTRIBUTE_TYPE, TRUE_TARGET_TYPE, KEY_TYPES

from d3m.primitives.data_transformation.column_parser import Common as ColumnParser
from d3m.primitives.data_transformation.extract_columns_by_semantic_types import Common as ExtractColumnsBySemanticTypes
from d3m.primitives.data_transformation.dataset_to_dataframe import Common as DatasetToDataFrame
from d3m.primitives.schema_discovery.profiler import Common as SimpleProfiler

from d3m.primitives.data_preprocessing.dataset_text_reader import DatasetTextReader
from d3m.primitives.data_transformation.denormalize import Common as Denormalize

from d3m.primitives.classification.bert_classifier import DistilBertPairClassification as DistilBertPairClassification

__all__ = ('DistilBertPairFragment',)

# 20200731: Note the need to identify text columns. To make this more general a fragment we would need to
#           have a primitive that finds these first. Also - it seems it needs a GPU (we got a tensorflow error)
#           to run. For now we will mothball this fragment

class DistilBertPairFragment(AutoflowPipelineFragment):

    name = "Distil Bert Pair Fragment"
    description = "Based on the MultiTableSRIFragment, this was adapted to test out the scores achievable with the " \
                  "Uncharted  DistilBertPairClassification in addition to the supporting primitives. See " \
                  "https://gitlab.com/daraghhartnett/tpot-ta2/-/issues/2 for details on test results"
    label = "distil_bert_pair"
    configuration = dict(resource_id=(2, 'dataframe_resource'))

    def study_dataset(self, dataset):
        resource_id = None
        for rid in dataset.keys():
            stypes = dataset.metadata.query((rid,)).get('semantic_types', [])
            if ENTRY_POINT_TYPE in stypes:
                resource_id = rid
                break
        if resource_id is None:
            raise ValueError("Cannot find entry resource")
        self.configure(resource_id=resource_id)

    def generate_dataset_steps(self):
        tr = DatasetTextReader(hyperparams={})
        dn = Denormalize(hyperparams={})
        node = "inputs.0"
        node = self.add_af_step(tr, node)
        node = self.add_af_step(dn, node)
        return node

    def generate_dataframe_steps(self, node):
        todf = DatasetToDataFrame(hyperparams={})
        simple_profiler = SimpleProfiler(hyperparams={})
        scp = ColumnParser(hyperparams={})
        cp = ColumnParser(hyperparams={})
        ext_attr = ExtractColumnsBySemanticTypes(hyperparams=dict(semantic_types=(ATTRIBUTE_TYPE,)))
        ext_targ = ExtractColumnsBySemanticTypes(hyperparams=dict(semantic_types=(TRUE_TARGET_TYPE,)))

        dilbert_defaults = DistilBertPairClassification.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams'].defaults()
        dilbert = DistilBertPairClassification(hyperparams=dilbert_defaults.defaults().replace({
                'doc_col_0': 1,
                'doc_col_1': 3,
                'batch_size': 16
            }),)

        node = self.add_af_step(todf, node)

        # Add primitive to add the metadata back in
        node = self.add_af_step(simple_profiler, node)

        # Extract the target
        tnode = self.add_af_step(ext_targ, node)
        rtnode = tnode
        tnode = self.add_af_step(scp, tnode)

        # Extract features
        node = self.add_af_step(cp, node)
        node = self.add_af_step(ext_attr, node)

        node = self.add_af_step(dilbert, node, outputs=tnode)

        return node, tnode, rtnode

