import numpy as np

# Classification
from d3m.primitives.classification.ada_boost import SKlearn as AdaBoost
from d3m.primitives.classification.bagging import SKlearn as BaggingClassifier
from d3m.primitives.classification.bernoulli_naive_bayes import SKlearn as SKBernoulliNB
from d3m.primitives.classification.decision_tree import SKlearn as SKDecisionTreeClassifier
from d3m.primitives.classification.extra_trees import SKlearn as SKExtraTreesClassifier
from d3m.primitives.classification.gaussian_naive_bayes import SKlearn as SKGaussianNB
from d3m.primitives.classification.gradient_boosting import SKlearn as SKGradientBoostingClassifier
from d3m.primitives.classification.k_neighbors import SKlearn as SKKNearestNeighbors
from d3m.primitives.classification.linear_discriminant_analysis import SKlearn as SKLinearDiscriminantAnalysis
from d3m.primitives.classification.linear_svc import SKlearn as SKLinearSVC
from d3m.primitives.classification.logistic_regression import SKlearn as SKLogisticRegression
from d3m.primitives.classification.mlp import SKlearn as SKMlp
from d3m.primitives.classification.multinomial_naive_bayes import SKlearn as SKMultinomialNB
from d3m.primitives.classification.nearest_centroid import SKlearn as SKNearestCentroid
from d3m.primitives.classification.passive_aggressive import SKlearn as SKPassiveAggressive
from d3m.primitives.classification.quadratic_discriminant_analysis import SKlearn as SKQuadraticDiscriminantAnalysis
from d3m.primitives.classification.random_forest import SKlearn as SKRandomForestClassifier
from d3m.primitives.classification.sgd import SKlearn as SKSGD
from d3m.primitives.classification.svc import SKlearn as SKSVC

# Cleaning TODO: Expand
from d3m.primitives.data_cleaning.standard_scaler import SKlearn as SKStandardScaler
from d3m.primitives.data_cleaning.min_max_scaler import SKlearn as SKMinMaxScaler

# Transformation TODO: Expand
from d3m.primitives.data_transformation.fast_ica import SKlearn as SKFastICA
from d3m.primitives.data_transformation.nystroem import SKlearn as SKNystroem
from d3m.primitives.data_transformation.one_hot_encoder import SKlearn as SKOneHotEncoder
from d3m.primitives.data_transformation.polynomial_features import SKlearn as SKPolynomialFeatures
from d3m.primitives.data_transformation.rbf_sampler import SKlearn as SKRBFSampler

# Feature Extraction TODO: Expand
from d3m.primitives.feature_extraction.feature_agglomeration import SKlearn as SKFeatureAgglomeration
from d3m.primitives.feature_extraction.pca import SKlearn as SKPCA

# Feature Selection TODO: Expand
from d3m.primitives.feature_selection.select_percentile import SKlearn as SKSelectPercentile


classifier_config_dict = {

    # Classifiers
    AdaBoost: {
        'add_index_columns': [True],
        'use_semantic_types': [True],
        "n_estimators": [
            50,
            100,
            5,
            166,
            5000,
            15,
            50000,
            50,
            500,
            25
        ],
        "learning_rate": [
            0.1,
            0.019191336689899793,
            0.44108504476590915,
            0.2299015019952191,
            0.6040909708603276,
            0.8094906864218753,
            0.9214634656073781,
            1.3146428648819914,
            1.3915547129450747,
            1.5668945978096747,
            1.8044119874576818,
            1.9908900601774795
        ],
        "algorithm": [
            "SAMME.R",
            "SAMME.R",
            "SAMME"
        ],
        "use_inputs_columns": [
            []
        ],
        "use_outputs_columns": [
            []
        ],
        "exclude_inputs_columns": [
            []
        ],
        "exclude_outputs_columns": [
            []
        ],
        "return_result": [
            "new"
        ],
        "use_semantic_types": [
            False
        ],
        "add_index_columns": [
            False
        ],
        "error_on_no_input": [
            True
        ],
        "return_semantic_type": [
            "https://metadata.datadrivendiscovery.org/types/PredictedTarget"
        ]
    },

    BaggingClassifier: {
        'add_index_columns': [True],
        'use_semantic_types': [True]
    },

    SKBernoulliNB: {
        'alpha': [1e-3, 1e-2, 1e-1, 1., 10., 100.],
        'fit_prior': [True, False],
    },

    SKDecisionTreeClassifier: {
        'criterion': ["gini", "entropy"],
        'max_depth': range(1, 11),
        'min_samples_split': range(2, 21),
        'min_samples_leaf': range(1, 21)
    },

    SKExtraTreesClassifier: {
        'n_estimators': [100],
        'criterion': ["gini", "entropy"],
        'max_features': np.arange(0.05, 1.01, 0.05),
        'min_samples_split': range(2, 21),
        'min_samples_leaf': range(1, 21),
        'bootstrap': [True, False]
    },

    SKGaussianNB: {
    },

    SKGradientBoostingClassifier: {
        'n_estimators': [100],
        'criterion': ["friedman_mse", "mae", "mse"],
        'min_weight_fraction_leaf': [0.],
        'min_impurity_decrease': [0.],
        'learning_rate': [1e-3, 1e-2, 1e-1, 0.5, 1.],
        'max_depth': range(1, 11),
        'min_samples_split': range(2, 21),
        'min_samples_leaf': range(1, 21),
        'subsample': np.arange(0.05, 1.01, 0.05),
        'max_features': np.arange(0.05, 1.01, 0.05)
    },

    SKKNearestNeighbors: {
    },

    SKLinearDiscriminantAnalysis: {
    },

    SKLinearSVC: {
    },

    # Disable for testing. Too slow for debugging!
    SKLogisticRegression: {
        'penalty': ["l1", "l2"],
        'C': [1e-4, 1e-3, 1e-2, 1e-1, 0.5, 1., 5., 10., 15., 20., 25.],
        'dual': [True, False]
    },

    SKMlp: {
    },

    SKMultinomialNB: {
        'alpha': [1e-3, 1e-2, 1e-1, 1., 10., 100.],
        'fit_prior': [True, False]
    },

    SKNearestCentroid: {
    },

    SKPassiveAggressive: {
    },

    SKQuadraticDiscriminantAnalysis: {
    },

    SKRandomForestClassifier: {
        'n_estimators': [100],
        'criterion': ["gini", "entropy"],
        'max_features': np.arange(0.05, 1.01, 0.05),
        'min_samples_split': range(2, 21),
        'min_samples_leaf': range(1, 21),
        'bootstrap': [True, False]
    },

    SKSGD: {

    },

    SKSVC: {
        'C': [2 ** i for i in range(-5, 6)],
        'kernel': [
            'rbf',
#            'poly',
            'sigmoid'],
        'degree': range(1, 6),
        'gamma': [1e-5, 1e-4, 1e-3, 1e-2, 1e-1, 1, 10],
        'coef0': np.arange(-1, 1.01, 0.2),
        'shrinking': [True, False],
        'tol': [1e-5, 1e-4, 1e-3, 1e-2, 1e-1]
    },

    #SKKNeighborsClassifier: {
    #    'n_neighbors': range(1, 101),
    #    'weights': ["uniform", "distance"],
    #    'p': [1, 2]
    #},


    SKFastICA: {
        'tol': np.arange(0.0, 1.01, 0.05)
    },

    SKFeatureAgglomeration: {
        'linkage': ['ward', 'complete', 'average'],
        'affinity': ['euclidean', 'l1', 'l2', 'manhattan', 'cosine', 'precomputed']
    },

    SKMinMaxScaler: {
    },

    SKNystroem: {
        'kernel': ['rbf', 'cosine', 'chi2', 'laplacian', 'polynomial', 'poly', 'linear', 'additive_chi2', 'sigmoid'],
        'gamma': np.arange(0.0, 1.01, 0.05),
        'n_components': range(1, 11)
    },

#    SKPCA: {
#        'svd_solver': ['randomized'],
#        'iterated_power': range(1, 11)
#    },

    SKPolynomialFeatures: {
        'degree': [2],
        'include_bias': [False],
        'interaction_only': [False]
    },

    SKRBFSampler: {
        'gamma': np.arange(0.0, 1.01, 0.05)
    },

    SKStandardScaler: {
    },

    #ZeroCount: {
    #},

    SKOneHotEncoder: {
        'minimum_fraction': [0.05, 0.1, 0.15, 0.2, 0.25],
        'sparse': [False],
        'handle_unknown': ['ignore']
    },

    SKSelectPercentile: {
        'percentile': range(1, 100),
        'score_func': ['f_classif']
    },

#    RPIFeatureSelector: {
#        'nbins': range(2, 21),
#        'method': ['counting', 'pseudoBayesian', 'fullBayesian'],
#        'strategy': ['uniform', 'quantile']
#    }

}