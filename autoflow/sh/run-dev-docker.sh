#!/bin/bash

# Runs a version of the docker images with development directories conveniently mounted

docker run -v `pwd`/data:/home/data -v `pwd`/config:/home/config -v `pwd`/code:/home/dev-code -v `pwd`/tpot:/home/tpot -v /media/external/WDMyPassport2TB/d3m/datasets/seed_datasets_current:/home/data/input -it registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:latest
