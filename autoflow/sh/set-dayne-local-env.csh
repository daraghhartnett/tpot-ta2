#!/bin/tcsh

setenv D3M_Dataset_Home "/Users/freitag/project/d3m/stage/data/input"
setenv D3M_Output_Home "/Users/freitag/project/d3m/stage/data/output"
setenv D3M_Config_Home "/Users/freitag/project/d3m/stage/tpot-ta2/config"
setenv D3M_Search_Executable "/Users/freitag/project/d3m/stage/tpot-ta2/autoflow/main.py"
setenv D3M_Docker_Image "registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:latest"

