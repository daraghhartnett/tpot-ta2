#!/bin/bash

rm -rf /src/bbn-primitives 
rm -rf /src/crocd3mwrapper 
rm -rf /src/dsbox-corex 
rm -rf /src/dsbox-dataprofiling 
rm -rf /src/dsbox-featurizer 
rm -rf /src/duked3mwrapper 
rm -rf /src/fastlvm 
rm -rf /src/featuretools-ta1 
rm -rf /src/find-projections 
rm -rf /src/lupi-svm 
rm -rf /src/nk-croc 
rm -rf /src/simond3mwrapper 
rm -rf /src/spider
