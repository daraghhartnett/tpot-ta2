Instructions on installing and running the JIDO dataset in Jupyter Notebook
===========================================================================
1) Create a conda environment with all the needed libraries:

    > sh scripts/jido_env.sh


2) Activate the environment

    > source activate jido


3) Get the  JMiner data from sharepoint and extract it in a location of your choosing:

    > wget https://sriintl.sharepoint.us/:u:/r/sites/SDTechLunchSeminar/Shared%20Documents/config_02_5000_basics.zip?csf=1&web=1&e=aE2a52


4) Create a symbolic link from the autoflow/util directory to the docs folder in the TERM_BY_DOC_COCL_OLD_FORMAT/docs
   folder (this is necessary to allow jupyter notebook to load the documents from its working directory):

    > sudo ln -s /.../TERM_BY_DOC_COCL_OLD_FORMAT/docs docs

5) Start Jupyter Notebook:

    > cd autoflow/util
    > jupyter notebook JIDO_analysis_config_02.ipynb


6) Adjust the path to the JMiner output in the second cell of the notebook so that it points to the location on your
   machine:

    * input_dir = "/.../TERM_BY_DOC_COCL_OLD_FORMAT"
    * adj_input_dir = "/.../TERM_BY_ADJ_COCL_OLD_FORMAT"


