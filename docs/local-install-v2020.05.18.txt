This file contains details on how to set up your local system with version 2019.11.10 of the core libraries.
This will be the target environment for the December 2019 Dry Run.

1) Create a directory for this new environment version:

    mkdir 20200518
    cd 20200518

2) Clone this repo into it:

    git clone https://gitlab.com/daraghhartnett/tpot-ta2.git
    cd tpot-ta2

3) Run the script to generate the 20200518 environement:

    sh scripts/20200518-env.sh

    Note: During one of the sd tech lunch talks Prateeth recommended that we use "python -m pip install..." instead
    of "pip install" when creating the miniconda environment as a library could be installed in an unexpected location
    (outside of the environment) otherwise - lets keep this in mind and perhaps switch to it for the next release

4) Activate the new environment:

    source activate 20200518

5) Enable that environment in your pycharm project.
    (https://www.jetbrains.com/help/pycharm/conda-support-creating-conda-virtual-environment.html)
    - Pycharm > Preferences
    - Project Interpreter
    - Click the cog to configure
    - Conda Environment
    - Specify the required interpreter: use the drop-down list, or click ... to Select an interpreter and find one in your
      file system.
    - Select the check-box Make available to all projects, if needed.
    - Click OK to complete the task.


6) If you run into a matlab issue:

    ImportError: Python is not installed as a framework. The Mac OS X backend will not be able to function correctly
    if Python is not installed as a framework. See the Python documentation for more information on installing Python
    as a framework on Mac OS X. Please either reinstall Python as a framework, or try one of the other backends.
    If you are using (Ana)Conda please install python.app and replace the use of 'python' with 'pythonw'. See
    'Working with Matplotlib on OSX' in the Matplotlib FAQ for more information.

   when you run then edit the file:

      /Users/daraghhartnett/miniconda3/envs/20200518/lib/python3.6/site-packages/rltk/evaluation/evaluation.py

   And add the following before importing matplotlib.pyplot

      import matplotlib
      matplotlib.use('PS')

   (For background see https://github.com/scikit-optimize/scikit-optimize/issues/637)

7) To get the correct version of the seed datasets, do the following:

    > git lfs clone https://gitlab.datadrivendiscovery.org/d3m/datasets.git
    > cd datasets
    > git pull
    > git lfs fetch
    > git checkout tags/2020.05.15.summer-dryrun

   There is no need to switch to a branch for this release as we are working from the master branch

8) The docker image we will be using for this release is:

    * Test/throwaway builds: registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:20200518
    * Candidate builds: registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:20200518-candi-01 (02 03 etc)
    * Release: registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:20200518-final

    # registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:20200518-candi-01 (Final Summer 2020 Dry Run Image )
    # registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:20200518-candi-? (Final Summer 2020 Eval Image )



