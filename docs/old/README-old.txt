This is the original README for the repo - it was out dated due to many changes in the program but there is still some
useful stuff in here so we will sequester it here.:

Repository Description
======================
This repo lives at https://gitlab.com/daraghhartnett/tpot-ta2. It houses the dockerfile and code that is used to
build the SRI TA2 docker image. This images is then pushed to the SRI TA2 docker repository at
https://gitlab.datadrivendiscovery.org/j18_TA2Eval/SRI_TPOT. This second repo has the gitlab configuration that runs
tests on the docker image to make sure it is compliant with the program submission requirements.


Creating the Docker Image:
==========================

1) Get a copy of the base docker file that suits your needs. In the current version we are using the JPL Complete
   python 3.6 version:

2) In the same directory as this readme file run the following commands. Note that he first command will request your
   docker registry username/password:

      docker login registry.datadrivendiscovery.org
      docker build --pull -f docker/python_3.6/Dockerfile -t registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:latest .

      Note: the --pull ensure that the underlying FROM image is updated to the latest version - this can also be done by
      doing a pull on that image.

      Note: If you want to pull the latest version of a docker image from the registry without building it locally do:
          docker pull registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot

3) Get the id of the image that was just created for use in the next step:

      docker images

   The results look like the following:

      > docker images
            REPOSITORY                   TAG                 IMAGE ID            CREATED             SIZE
            sri-test-0.1                 latest              2df1c97d392b        9 hours ago         2.43GB

    The image id is: 2df1c97d392b

4) To run and interact with the docker image, run the following in separate terminals:

      > docker run <image-id>                   # starts the docker image with the specified id
      > docker ps                               # lists the running docker containers
      > docker exec -it <container-id> bash     # starts a sh terminal interaction with the running container
      > docker kill <container-id>              # to shut down the running container instance

5) To test the docker image before pushing to the NIST docker registry, run the following command. Notice the paths to
   config.json - the local copy of the file will need the absolute path. This also goes for the sample data set that
   you want to run (in the case below, baseball):

      > docker run -i --entrypoint /bin/bash -v /home/config.json:/home/code/dsbox-ta2/config.json -v
          /home/data/o_185:/baseball/ 2df1c97d392b -c 'ta2_search /home/code/dsbox-ta2/config.json'

      A more machine specific example, for a prior iteration:
      > docker run -i --entrypoint /bin/bash -v
          /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/config/config.json:/home/code/config.json -v
          /Users/daraghhartnett/Projects/D3M/data/o_185:/baseball/ ce95ef7d980c -c 'ta2_search /home/code/config.json'

      A further example that is newer for baseball_185:
      > docker run -i --entrypoint /bin/bash
         -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/config/config-baseball-185.json:/home/code/config.json
         -v /Users/daraghhartnett/Projects/D3M/data/185_baseball:/baseball/
         -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/outputs:/outputs/
          ce95ef7d980c -c 'ta2_search /home/code/config.json'

      A further example that is newer for personae_30:
      > docker run -i --entrypoint /bin/bash
         -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/config/config-personae-30.json:/home/code/config.json
         -v /Users/daraghhartnett/Projects/D3M/data/30_personae:/personae/
         -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/outputs:/outputs/
          ce95ef7d980c -c 'ta2_search /home/code/config.json'

6) To test the the executable is runnable as NIST expects run the following:

      > docker run --entrypoint bash
         -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/config/config-baseball-185.json:/home/code/config.json
         -v /Users/daraghhartnett/Projects/D3M/data/185_baseball:/baseball/
         -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/outputs:/outputs/
          79081536d305 -c '/outputs/executables/CWNPGW90JP.py /home/code/config.json'

7) If the docker run completes with pipelines created then you are ready to upload the image to the NIST Docker Registry.
   If you do not have an account for this let Daragh Hartnett (daragh.hartnett@sri.com) know and he will get you the
   credentials you need. Run the following commands:

      > docker login registry.datadrivendiscovery.org
      > docker push registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot

8) Run the Docker CI pipeline by opening the following in a browser:

      https://gitlab.datadrivendiscovery.org/TA2/SRI_ta2

   - Click on 'Pipelines' in the top menu and then 'Pipelines' in the sub menu.
   - Click on Run Pipeline
   - Click on Create Pipeline
   - Click on the Play button in the setup node.
   - In the logs, you should be able to see the latest image id (63e81a8c1fed for example) embedded in the long
   identifier for the latest docker image being used (Using docker image registry.datadrivendiscovery.org/ta2/sri_ta2:
   latest ID=sha256:**63e81a8c1fed**c2beabb2e8ee3b2cb84b4ed96dc97ebc071d0a2217d984716708...)


Updated run instructions (Grr!) - SEARCH
========================================
They have changed the search command line:

    - 6_70_com_amazon: (Community Detection)
        Search: docker run -i --entrypoint /bin/bash -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/config/search_config.json:/home/code/config.json -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/input/6_70_com_amazon:/inputs/ -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/outputs/6_70_com_amazon:/outputs/ --entrypoint=ta2_search registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:stable /home/code/config.json
        Test: docker run -i --entrypoint /bin/bash -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/config/test_config.json:/home/code/config.json -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/input/6_70_com_amazon:/inputs/ -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/outputs/6_70_com_amazon:/outputs/ registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:stable -c '/outputs/executables/.py /home/code/config.json'

    - 22_handgeometry: (Regression)
        Search: docker run -i --entrypoint /bin/bash -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/config/search_config.json:/home/code/config.json -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/input/22_handgeometry:/inputs/ -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/outputs/22_handgeometry:/outputs/ --entrypoint=ta2_search registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:stable /home/code/config.json
        Test: docker run -i --entrypoint /bin/bash -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/config/test_config.json:/home/code/config.json -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/input/22_handgeometry:/inputs/ -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/outputs/22_handgeometry:/outputs/ registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:stable -c '/outputs/executables/.py /home/code/config.json'

    - 26_radon_seed: (Regression)
        Search: docker run -i --entrypoint /bin/bash -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/config/search_config.json:/home/code/config.json -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/input/26_radon_seed:/inputs/ -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/outputs/26_radon_seed:/outputs/ --entrypoint=ta2_search registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:stable /home/code/config.json
        Test: docker run -i --entrypoint /bin/bash -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/config/test_config.json:/home/code/config.json -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/input/26_radon_seed:/inputs/ -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/outputs/26_radon_seed:/outputs/ registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:stable -c '/outputs/executables/.py /home/code/config.json'

    - 30_personae: (Classification)
        Search: docker run -i --entrypoint /bin/bash -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/config/search_config.json:/home/code/config.json -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/input/30_personae:/inputs/ -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/outputs/30_personae:/outputs/ --entrypoint=ta2_search registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:stable /home/code/config.json
        Test: docker run -i --entrypoint /bin/bash -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/config/test_config.json:/home/code/config.json -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/input/30_personae:/inputs/ -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/outputs/30_personae:/outputs/ registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:stable -c '/outputs/executables/.py /home/code/config.json'

    - 31_urbansound: (Classification)
        Search: docker run -i --entrypoint /bin/bash -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/config/search_config.json:/home/code/config.json -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/input/31_urbansound:/inputs/ -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/outputs/31_urbansound:/outputs/ --entrypoint=ta2_search registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:stable /home/code/config.json
        Test: docker run -i --entrypoint /bin/bash -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/config/test_config.json:/home/code/config.json -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/input/31_urbansound:/inputs/ -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/outputs/31_urbansound:/outputs/ registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:stable -c '/outputs/executables/.py /home/code/config.json'

    - 32_wikiqa: (Classification) (Needs 20 mins to find pipeline)
        Search: docker run -i --entrypoint /bin/bash -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/config/search_config.json:/home/code/config.json -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/input/32_wikiqa:/inputs/ -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/outputs/32_wikiqa:/outputs/ --entrypoint=ta2_search registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:stable /home/code/config.json
        Test: docker run -i --entrypoint /bin/bash -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/config/test_config.json:/home/code/config.json -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/input/32_wikiqa:/inputs/ -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/outputs/32_wikiqa:/outputs/ registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:stable -c '/outputs/executables/.py /home/code/config.json'

    - 38_sick: (Classification)
        Search: docker run -i --entrypoint /bin/bash -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/config/search_config.json:/home/code/config.json -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/input/38_sick:/inputs/ -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/outputs/38_sick:/outputs/ --entrypoint=ta2_search registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:stable /home/code/config.json
        Test: docker run -i --entrypoint /bin/bash -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/config/test_config.json:/home/code/config.json -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/input/38_sick:/inputs/ -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/outputs/38_sick:/outputs/ registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:stable -c '/outputs/executables/.py /home/code/config.json'

    - 49_facebook: (Graph - Matching)
        Search: docker run -i --entrypoint /bin/bash -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/config/search_config.json:/home/code/config.json -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/input/49_facebook:/inputs/ -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/outputs/49_facebook:/outputs/ --entrypoint=ta2_search registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:stable /home/code/config.json
        Test: docker run -i --entrypoint /bin/bash -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/config/test_config.json:/home/code/config.json -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/input/49_facebook:/inputs/ -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/outputs/49_facebook:/outputs/ registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:stable -c '/outputs/executables/.py /home/code/config.json'

    - 59_umls: (Graph - Link Prediction)
        Search: docker run -i --entrypoint /bin/bash -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/config/search_config.json:/home/code/config.json -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/input/59_umls:/inputs/ -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/outputs/59_umls:/outputs/ --entrypoint=ta2_search registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:stable /home/code/config.json
        Test: docker run -i --entrypoint /bin/bash -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/config/test_config.json:/home/code/config.json -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/input/59_umls:/inputs/ -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/outputs/59_umls:/outputs/ registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:stable -c '/outputs/executables/.py /home/code/config.json'

    - 60_jester: (Graph - Collaborative Filtering)
        Search: docker run -i --entrypoint /bin/bash -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/config/search_config.json:/home/code/config.json -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/input/60_jester:/inputs/ -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/outputs/60_jester:/outputs/ --entrypoint=ta2_search registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:stable /home/code/config.json
        Test: docker run -i --entrypoint /bin/bash -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/config/test_config.json:/home/code/config.json -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/input/60_jester:/inputs/ -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/outputs/60_jester:/outputs/ registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:stable -c '/outputs/executables/.py /home/code/config.json'

    - 185_baseball (Classification)
        Search: docker run -i --entrypoint /bin/bash -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/config/search_config.json:/home/code/config.json -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/input/185_baseball:/inputs/ -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/outputs/185_baseball:/outputs/ --entrypoint=ta2_search registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:stable /home/code/config.json
        Test: docker run -i --entrypoint /bin/bash -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/config/test_config.json:/home/code/config.json -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/input/185_baseball:/inputs/ -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/outputs/185_baseball:/outputs/ registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:stable -c '/outputs/executables/.py /home/code/config.json'

    - 196_autoMpg (Regression)
        Search: docker run -i --entrypoint /bin/bash -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/config/search_config.json:/home/code/config.json -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/input/196_autoMpg:/inputs/ -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/outputs/196_autoMpg:/outputs/ --entrypoint=ta2_search registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:stable /home/code/config.json
        Test: docker run -i --entrypoint /bin/bash -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/config/test_config.json:/home/code/config.json -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/input/196_autoMpg:/inputs/ -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/outputs/196_autoMpg:/outputs/ registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:stable -c '/outputs/executables/.py /home/code/config.json'

    - 313_spectrometer: (Classification)
        Search: docker run -i --entrypoint /bin/bash -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/config/search_config.json:/home/code/config.json -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/input/313_spectrometer:/inputs/ -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/outputs/313_spectrometer:/outputs/ --entrypoint=ta2_search registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:stable /home/code/config.json
        Test: docker run -i --entrypoint /bin/bash -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/config/test_config.json:/home/code/config.json -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/input/313_spectrometer:/inputs/ -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/outputs/313_spectrometer:/outputs/ registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:stable -c '/outputs/executables/.py /home/code/config.json'

    - 1491_one_hundred_plants_margin: (Classification)
        Search: docker run -i --entrypoint /bin/bash -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/config/search_config.json:/home/code/config.json -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/input/1491_one_hundred_plants_margin:/inputs/ -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/outputs/1491_one_hundred_plants_margin:/outputs/ --entrypoint=ta2_search registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:stable /home/code/config.json
        Test: docker run -i --entrypoint /bin/bash -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/config/test_config.json:/home/code/config.json -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/input/1491_one_hundred_plants_margin:/inputs/ -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/outputs/1491_one_hundred_plants_margin:/outputs/ registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:stable -c '/outputs/executables/.py /home/code/config.json'

    - 4550_MiceProtein: (Classification)
        Search: docker run -i --entrypoint /bin/bash -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/config/search_config.json:/home/code/config.json -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/input/4550_MiceProtein:/inputs/ -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/outputs/4550_MiceProtein:/outputs/ --entrypoint=ta2_search registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:stable /home/code/config.json
        Test: docker run -i --entrypoint /bin/bash -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/config/test_config.json:/home/code/config.json -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/input/4550_MiceProtein:/inputs/ -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/outputs/4550_MiceProtein:/outputs/ registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:stable -c '/outputs/executables/.py /home/code/config.json'

    - uu1_datasmash: (Classification)
        Search: docker run -i --entrypoint /bin/bash -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/config/search_config.json:/home/code/config.json -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/input/uu1_datasmash:/inputs/ -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/outputs/uu1_datasmash:/outputs/ --entrypoint=ta2_search registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:stable /home/code/config.json
        Test: docker run -i --entrypoint /bin/bash -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/config/test_config.json:/home/code/config.json -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/input/uu1_datasmash:/inputs/ -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/outputs/uu1_datasmash:/outputs/ registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:stable -c '/outputs/executables/.py /home/code/config.json'

    - uu3_world_development_indicators: (Regression)
        Search: docker run -i --entrypoint /bin/bash -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/config/search_config.json:/home/code/config.json -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/input/uu3_world_development_indicators:/inputs/ -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/outputs/uu3_world_development_indicators:/outputs/ --entrypoint=ta2_search registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:stable /home/code/config.json
        Test: docker run -i --entrypoint /bin/bash -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/config/test_config.json:/home/code/config.json -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/input/uu3_world_development_indicators:/inputs/ -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/outputs/uu3_world_development_indicators:/outputs/ registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:stable -c '/outputs/executables/.py /home/code/config.json'

    - uu4_SPECT: (Classification)
        Search: docker run -i --entrypoint /bin/bash -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/config/search_config.json:/home/code/config.json -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/input/uu4_SPECT:/inputs/ -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/outputs/uu4_SPECT:/outputs/ --entrypoint=ta2_search registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:stable /home/code/config.json
        Test: docker run -i --entrypoint /bin/bash -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/config/test_config.json:/home/code/config.json -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/input/uu4_SPECT:/inputs/ -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/outputs/uu4_SPECT:/outputs/ registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:stable -c '/outputs/executables/.py /home/code/config.json'


Run instructions - SERVER
=========================

   These are command line docker run commands to help debug the TA2 server side of the TA2-TA3 interaction - look in the
   ./docker/compose/docker-compose.yml file for corresponding volume mount bindings

    - 185_baseball (Classification)
        Search: docker run -i --entrypoint /bin/bash -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/config/search_config.json:/config/search_config.json -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/input/185_baseball/TRAIN:/inputs/TRAIN --entrypoint=ta2_server registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:stable /config/search_config.json
        Command line Server: docker run --entrypoint bash -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/input/185_baseball/TRAIN:/inputs/TRAIN -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/outputs/185_baseball/:/outputs -v /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/config/search_config.json:/config/search_config.json registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:stable -c 'ta2_server /config/search_config.json'

    - docker-compose up                 # Starts the distil, srita2, elastic search and postgres containers
    - docker exec -it compose_distil_1 ta3_search    # Launches the ta3_search command which goes looking for the ta2 (it was timing out when the 'command' option in the yaml was used)


Installing the Correct Flavor of the D3M libraries:
===================================================

1) Checkout the library version you want in particular:

      git clone d3m
      git checkout <checkin hash>
      pip3.6 install .

2) To force update of pipy

      pip3.6 install pslgraph --upgrade --force

3) To swap a version of a third party libr

      pip3.6 install pandas==0.21.1 --upgrade --force




D3M Discoverability:
====================

1) The d3m api discovery code assumes that compliant primitives are loaded under the python package d3m.primitives.
2) This means that the d3m package needs to be installed as well as the primitives library (such as ds-box-cleaning)


Notes:
======

1) What python libraries are installed:

      pip3.6 list

2) Clean up / delete docker images & hanging objects:

      docker rmi --force $(docker images --filter "dangling=true" -q --no-trunc)
      docker rmi $(docker ps -q -f 'status=exited')
      docker rmi $(docker images -q -f "dangling=true")

3) Clean up docker hash & cache

      docker system prune

   Add -a at the end of that last one if you want to nuke images as well

4) If you try installing using pip and get an error:

      git.exc.InvalidGitRepositoryError: /Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/dsbox/datapreprocessing/profiler

   Just add a -e to the pip command - this will pull in edit mode and request the history from git.

5) How to install sklearn wrap:

    - Clone the code from:
        https://gitlab.datadrivendiscovery.org/jpl/d3m_sklearn_wrap

    - Run the following in a checked out directory:
        pip3.6 install -e ./


Getting the system and adding code to it:
==========================================

1) git clone git@gitlab.com:daraghhartnett/tpot-ta2.git
2) Copy yer stuff into the sri-ta2 directory as ta3-interface (or something)
3) git add ta3-interface/
4) git commit
5) git push


Deployment Notes
================
1) If you make changes to TPOT - do so in the main TPOT repo at

        /Users/daraghhartnett/Projects/D3M/evals/022018/code/tpot/tpot

   and copy the directory to this one:

        cp -r ../tpot/tpot .


Local installation of this Code
===============================

1) Get a copy of this code from the repo:

        git clone git@gitlab.com:daraghhartnett/tpot-ta2.git

   Note that you might need to set up your SSH Keys if you have not done so yet so that you can pull from gitlab.com:

        https://gitlab.com/profile/keys

2) Install the dependencies:

        a) sklearn wrap:

                git clone https://gitlab.datadrivendiscovery.org/jpl/d3m_sklearn_wrap
		cd d3m_sklearn_wrap
                pip install --process-dependency-links -e ./

        b) d3m:

                pip install --process-dependency-links d3m

        c) metadata:

                pip install --process-dependency-links d3m_metadata

        d) primitive interfaces:

                pip install --process-dependency-links primitive_interfaces

        e) custom version of tpot (the tpot.tar.gz is in the root directory of this repo):

                tar -zxvf tpot.tar.gz
                cd tpot
                python setup.py sdist
                pip install dist/TPOT-0.9.3.tar.gz

                NOTE: To get the correct version of tpot installed between the command line and pycharm, make sure the
                same version of pip/python are visible on the command line (source ~/.profile)

        f) dsbox:

                git clone http://github.com/usc-isi-i2/dsbox-profiling.git
                cd dsbox-profiling
                pip install -e ./
                git clone http://github.com/usc-isi-i2/dsbox-cleaning.git
                cd dsbox-cleaning
                pip install -e ./
                pip install theano
                pip install numpy==1.14.0 --upgrade --force

        g) pslgraph:

                pip install --process-dependency-links pslgraph

        h) pathos, a parallel processing library:

                pip install --process-dependency-links pathos


3) Get the data from https://datadrivendiscovery.org/data/seed_datasets_current/ and expand each tar.gz into
   ./data/input so that each seed dataset has its own directory under the input directory. For example:

        mkdir -p data/input
        ./data/input/185_baseball/

4) Run the search process for a dataset, we will use baseball as an example:

        python code/main.py search config/185_baseball_local_config_search.json

5) The above script will output the logs, executable and pickled artifacts into:

        data/outputs/185_baseball/

6) Next run the test process which will use the executable produced by the search phase to make predictions on the
   test data:

        python data/outputs/185_baseball/executables/<executable name>.py config/185_baseball_local_config_test.json

7) This script will produce a set of predictions that can be found in the file:

        data/outputs/185_baseball/results/predictions.csv

8) Next you will want to get the score for the predictions. First determine the metric to use by examining the
   metric field in the data sets problemDoc.json:

        look in: data/input/185_baseball/TRAIN/problem_TRAIN/problemDoc.json

        you will see:
                ...
                "performanceMetrics": [
                    {
                      "metric": "f1Macro"
                    }
                ]
                ...

9) Using the above metric, pass the predictions, targets and metric to the evaluation code:

        python code/evaluate.py data/outputs/185_baseball/results/predictions.csv data/input/185_baseball/SCORE/targets.csv f1Macro

   Which will produce:

        F1Macro: 0.43944895199459094


Updated Image Building Instructions:
====================================

1) To build the docker image that NIST will pick up for testing (stable tag):

    docker build -f docker/core_1.5/Dockerfile -t registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:stable .

2) To build the latest image which we use for internal testing:

    docker build -f docker/core_1.5/Dockerfile -t registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:latest .

3) Add --pull after the docker command on the above commands to make sure you have the latest underylying image
   (see the FROM section in the docker file).


Installing and running with MiniKube:
=====================================

1) Follow install at: https://kubernetes.io/docs/tutorials/stateless-application/hello-minikube/#create-a-deployment

   - Make sure you register kubectl with docker so that when we build the images it will be found by minikube when it is run:

      eval $(minikube docker-env)

2) kubectl does not like _'s in the image name so build it with name = sri-tpot instead of sri_tpot. It also seemed to
   require the version have the tag "v1"

      docker build -f docker/core_1.5/Dockerfile -t registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:latest .
      docker build -f docker/core_1.5/Dockerfile -t sri-tpot:v1 .

3) In separate tabs, mount the required files (Note: this only works for deployments - unfortunately it is not possible
   to use this for pods):

      minikube mount /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/config/search_config.json:/config/search_config.json
      minikube mount /Users/daraghhartnett/Projects/D3M/evals/022018/code/sri-ta2/data/input/185_baseball/TRAIN:/inputs/TRAIN

4) Run the ta2_search command on the running docker image

      kubectl run sri-tpot --image=sri-tpot:v1 --command -- ta2_search
      kubectl run registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot --image=registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:latest --command -- ta2_search

5) To nuke minikube:

      minikube stop;
      minikube delete;
      rm -rf ~/.minikube .kube;
      brew uninstall kubectl;
      brew cask uninstall docker virtualbox minikube;

6) Delete the deployment so we can run another:

      kubectl delete deployment sri-tpot

7) To interactively run with a k8s container:

      https://gc-taylor.com/blog/2016/10/31/fire-up-an-interactive-bash-pod-within-a-kubernetes-cluster

Issue:
------
The above approach got us some of the way - we were able to mount folders and start python but the docker image stopped
almost immediately for unexplained reasons.
Having discussed this with Marion - we need to be deploying in a pod - not a deployment. The issue with this is that the
minikube mount is not available to pods so we need to use a different mount method:

8) Pod mount method:
   Set up local pv/pvc for the /inputs and /outputs and add back the volumes and volume mounts in the yml file
   https://kubernetes.io/docs/tasks/configure-pod-container/configure-persistent-volume-storage/
   See pod-mount.yml for an example from Marion

   Issue with this was permissions - trying to create a directory in kubectl yielded a permission error. Tried
   sudo but to no avail. Googled for solution but nothing looks similar...


Capturing executable name from output:
======================================
grep -E "Created pipeline (.*) \(1," output.txt | grep -Po 'pipeline \K[^ (]*'



Command line runs for NIST Eval:
==================================

313_spectrometer:
docker run -i --entrypoint /bin/bash -v /export/home/jig1/hartnett/Projects/d3m/sri-ta2/config/search_config.json:/home/code/config.json -v /export/home/jig1/hartnett/Projects/d3m/sri-ta2/data/input/313_spectrometer:/inputs/ -v /export/home/jig1/hartnett/Projects/d3m/sri-ta2/data/outputs/313_spectrometer:/outputs/ --entrypoint=ta2_search registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:latest /home/code/config.json
docker run -i --entrypoint /bin/bash -v /export/home/jig1/hartnett/Projects/d3m/sri-ta2/config/test_config.json:/home/code/config.json -v /export/home/jig1/hartnett/Projects/d3m/sri-ta2/data/input/313_spectrometer:/inputs/ -v /export/home/jig1/hartnett/Projects/d3m/sri-ta2/data/outputs/313_spectrometer:/outputs/ registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:latest -c "/outputs/executables/.py /home/code/config.json"
python code/evaluate.py data/outputs/313_spectrometer/results/predictions.csv data/input/313_spectrometer/SCORE/targets.csv f1Macro
15 minute run result:

26_radon_seed
docker run -i --entrypoint /bin/bash -v /export/home/jig1/hartnett/Projects/d3m/sri-ta2/config/search_config.json:/home/code/config.json -v /export/home/jig1/hartnett/Projects/d3m/sri-ta2/data/input/26_radon_seed:/inputs/ -v /export/home/jig1/hartnett/Projects/d3m/sri-ta2/data/outputs/26_radon_seed:/outputs/ --entrypoint=ta2_search registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:latest /home/code/config.json
docker run -i --entrypoint /bin/bash -v /export/home/jig1/hartnett/Projects/d3m/sri-ta2/config/test_config.json:/home/code/config.json -v /export/home/jig1/hartnett/Projects/d3m/sri-ta2/data/input/26_radon_seed:/inputs/ -v /export/home/jig1/hartnett/Projects/d3m/sri-ta2/data/outputs/26_radon_seed:/outputs/ registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:latest -c "/outputs/executables/.py /home/code/config.json"
python code/evaluate.py data/outputs/26_radon_seed/results/predictions.csv data/input/26_radon_seed/SCORE/targets.csv root_mean_squared_error
15 minute run result:

38_sick
docker run -i --entrypoint /bin/bash -v /export/home/jig1/hartnett/Projects/d3m/sri-ta2/config/search_config.json:/home/code/config.json -v /export/home/jig1/hartnett/Projects/d3m/sri-ta2/data/input/38_sick:/inputs/ -v /export/home/jig1/hartnett/Projects/d3m/sri-ta2/data/outputs/38_sick:/outputs/ --entrypoint=ta2_search registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:latest /home/code/config.json
docker run -i --entrypoint /bin/bash -v /export/home/jig1/hartnett/Projects/d3m/sri-ta2/config/test_config.json:/home/code/config.json -v /export/home/jig1/hartnett/Projects/d3m/sri-ta2/data/input/38_sick:/inputs/ -v /export/home/jig1/hartnett/Projects/d3m/sri-ta2/data/outputs/38_sick:/outputs/ registry.datadrivendiscovery.org/j18_ta2eval/sri_tpot:latest -c "/outputs/executables/.py /home/code/config.json"
python code/evaluate.py data/outputs/38_sick/results/predictions.csv data/input/38_sick/SCORE/targets.csv f1Macro
15 minute run result:


Get the SHA of a docker image:
==============================
NIST usually asks for the SHA of a docker image to make sure they get the right one. This is how you get it:

    docker inspect --format='{{index .RepoDigests 0}}' <Image Id>


Pip installable instructions:
=============================
- Autoflow is now pip installable. Before starting, it is recommended that you create a conda environment using the
  following commands:

    > conda create -n <environment name> python=3.6.9
    > conda activate <environment name>

- This step will no longer be needed if we upgrade to Python 3.7 or xgboost 1.1.0. XGBoost, which is needed for some
  D3M libraries needs some special installation:

    git clone --recursive https://github.com/dmlc/xgboost
    cd xgboost; cp make/minimum.mk ./config.mk; make -j4
    cd python-package; python setup.py develop --user

- pip install autoflow-1.0.0-py3-none-any.whl

- This is fixed in the post 20200518 release of d3mcore.
  When you run autoflow you will get an error like this:

    (test-package) AIC-CAS0013240-Bering:test-package daraghhartnett$ autoflow
    Traceback (most recent call last):
      File "/Users/daraghhartnett/miniconda3/envs/test-package/bin/autoflow", line 5, in <module>
        from autoflow.main import main
      File "/Users/daraghhartnett/miniconda3/envs/test-package/lib/python3.6/site-packages/autoflow/main.py", line 15, in <module>
        from .autoflowconfig import AutoflowConfig
      File "/Users/daraghhartnett/miniconda3/envs/test-package/lib/python3.6/site-packages/autoflow/autoflowconfig.py", line 3, in <module>
        from .optimizer import TPOTClassificationOptimizer
      File "/Users/daraghhartnett/miniconda3/envs/test-package/lib/python3.6/site-packages/autoflow/optimizer.py", line 31, in <module>
        from d3m.metadata.pipeline import PlaceholderStep
      File "/Users/daraghhartnett/miniconda3/envs/test-package/lib/python3.6/site-packages/d3m/metadata/pipeline.py", line 18, in <module>
        from d3m import container, deprecate, environment_variables, exceptions, index, utils
      File "/Users/daraghhartnett/miniconda3/envs/test-package/lib/python3.6/site-packages/d3m/index.py", line 21, in <module>
        import pycurl  # type: ignore

  Edit index.py mentioned in the last file in the exception and comment out the 'import pycurl' line

- SKLearn also has a problem with the install - we cant seem to specify a source git clone in the setup.py so you need to run this
  manually:

    pip install -e git+https://gitlab.com/datadrivendiscovery/sklearn-wrap.git@dist#egg=sklearn_wrap



Docker repos:
=============
Repo where we keep this code: https://gitlab.com/daraghhartnett/tpot-ta2
Repo where we keep our TA3 ready image for use by anyone: https://gitlab.com/daraghhartnett/autoflow
Repo where we push images to so the evaluation repos can use the core TA2: https://gitlab.datadrivendiscovery.org/j18_TA2Eval/SRI_TPOT
