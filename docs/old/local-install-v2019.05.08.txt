This file contains details on how to set up your local system with version 2019.05.08 of the core libraries.

1) Create a directory for this new environment version:

    mkdir 20190508
    cd 20190508

2) Clone this repo into it:

    git clone https://gitlab.com/daraghhartnett/tpot-ta2.git
    cd tpot-ta2

3) Run the script to generate the 20190508 environement:

    sh scripts/20190508-env.sh

4) Activate the new environment:

    source activate 20190508

5) Enable that environment in your pycharm project.

6) If you run into a matlab issue:

    ImportError: Python is not installed as a framework. The Mac OS X backend will not be able to function correctly
    if Python is not installed as a framework. See the Python documentation for more information on installing Python
    as a framework on Mac OS X. Please either reinstall Python as a framework, or try one of the other backends.
    If you are using (Ana)Conda please install python.app and replace the use of 'python' with 'pythonw'. See
    'Working with Matplotlib on OSX' in the Matplotlib FAQ for more information.

   when you run then edit the file:

      /Users/daraghhartnett/miniconda3/envs/20190508/lib/python3.6/site-packages/rltk/evaluation/evaluation.py

   And add the following before importing matplotlib.pyplot

      import matplotlib
      matplotlib.use('PS')

   (For background see https://github.com/scikit-optimize/scikit-optimize/issues/637)