# TA2C Client

[![pipeline status](https://gitlab.datadrivendiscovery.org/sunchartify/register-ta2-api/badges/master/pipeline.svg)](https://gitlab.datadrivendiscovery.org/sunchartify/register-ta2-api/commits/master)


### Python client usage


``` python
pip3 install git+ssh://git@gitlab.datadrivendiscovery.org/sunchartify/register-ta2-api.git

...

from ta2c.pyclient import RegistrationHelper


if __name__ == '__main__':
    # Register with coordinator
    RegistrationHelper(ta2c_address, ta2s_port)

    ... your code here ...
```
