from setuptools import setup

setup(
    name='ta2c',
    version='1.0.0',
    packages=['ta2c'],
    install_requires=["grpcio==1.4.0",
                      "grpcio-tools==1.6.3",
    ],
)
