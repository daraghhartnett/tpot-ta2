import os
import socket
import time
import logging

import grpc

from multiprocessing import pool

from ta2c import register_pb2, register_pb2_grpc


class RegistrationHelper():
    """RegistrationHelper class handles sending regular checkins to 
    Coordinator server"""
    def __init__(self, ta2s_uri, worker_port, worker_address=False, worker_name=False):
        self.coordinator_uri = ta2s_uri
        self.worker_address = worker_address
        self.worker_port = worker_port
        self.worker_name = worker_name
        self.self_uri = self._get_self_uri()
        self.logger = logging.getLogger('Registration')
        self.logger.info('Initializing Registration instance')
        self.registration_thread = pool.ThreadPool(processes=1,)
        self.registration_thread.apply_async(self.loop_register)
        
    def _get_self_uri(self):
        if not self.worker_address:
            host = socket.gethostname()
        else:
            host = self.worker_address
        uri = str(host) + ':' + str(self.worker_port)
        return uri

    def _get_pb_message(self):
        message = register_pb2.WorkerInfo(worker_uri=self.self_uri,
                                          name=self.worker_name)
        return message

    def _send_message(self):
        message = self._get_pb_message()
        resp = self.conn.RegisterWorker(message)
        self.logger.info("Coordinator responded to registration request with {}".format(resp))

    def _create_coordinator_connection(self):
        channel = grpc.insecure_channel(self.coordinator_uri)
        self.conn = register_pb2_grpc.WorkerRegistrationStub(channel)
        
    def loop_register(self):
        """Tries to send registration info message to Coordinator every
        three seconds"""
        self.logger.info("Attempting to send initial registration message")
        while True:
            try:
                self.logger.debug("Sending registration message")
                self._send_message()
            except grpc.RpcError as e:
                if e.code().name == "UNAVAILABLE":
                    self.logger.error("Coordinator connection unavailable, attempting to re-try connecting to coordinator")
                    self._create_coordinator_connection()
            except AttributeError:
                self.logger.error("Error sending message, coordinator connection not yet established.")
                self.logger.info("Creating coordinator connection")
                self._create_coordinator_connection()
                self.logger.info("Successfully connected to coordinator at {}".format(self.coordinator_uri))
                self.logger.info("Attempting to re-send registration message")
            except Exception as e:
                self.logger.error("Unhandled exception occured while registering, error = {}".format(e))
                pass
            time.sleep(3)        

